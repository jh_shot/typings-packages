declare module 'element-ui' {
  // Basic
  export const Col: typeof import('./types/col')['Col']
  export const Row: typeof import('./types/row')['Row']
  export const Container: typeof import('./types/container')['Container']
  export const Header: typeof import('./types/header')['Header']
  export const Main: typeof import('./types/main')['Main']
  export const Footer: typeof import('./types/footer')['Footer']
  export const Aside: typeof import('./types/aside')['Aside']
  export const Icon: typeof import('./types/icon')['Icon']
  export const Button: typeof import('./types/button')['Button']
  export const ButtonGroup: typeof import('./types/buttonGroup')['ButtonGroup']
  export const Link: typeof import('./types/link')['Link']

  // Form
  export const Radio: typeof import('./types/radio')['Radio']
  export const RadioButton: typeof import('./types/radioButton')['RadioButton']
  export const RadioGroup: typeof import('./types/radioGroup')['RadioGroup']
  export const Checkbox: typeof import('./types/checkbox')['Checkbox']
  export const CheckboxButton: typeof import('./types/checkboxButton')['CheckboxButton']
  export const CheckboxGroup: typeof import('./types/checkboxGroup')['CheckboxGroup']
  export const Input: typeof import('./types/input')['Input']
  export const InputNumber: typeof import('./types/inputNumber')['InputNumber']
  export const Autocomplete: typeof import('./types/autocomplete')['Autocomplete']
  export const Select: typeof import('./types/select')['Select']
  export const Option: typeof import('./types/option')['Option']
  export const OptionGroup: typeof import('./types/optionGroup')['OptionGroup']
  export const Cascader: typeof import('./types/cascader')['Cascader']
  export const CascaderPanel: typeof import('./types/cascaderPanel')['CascaderPanel']
  export const Switch: typeof import('./types/switch')['Switch']
  export const Slider: typeof import('./types/slider')['Slider']
  export const TimePicker: typeof import('./types/timePicker')['TimePicker']
  export const TimeSelect: typeof import('./types/timeSelect')['TimeSelect']
  export const DatePicker: typeof import('./types/datePicker')['DatePicker']
  export const Upload: typeof import('./types/upload')['Upload']
  export const Rate: typeof import('./types/rate')['Rate']
  export const ColorPicker: typeof import('./types/colorPicker')['ColorPicker']
  export const Transfer: typeof import('./types/transfer')['Transfer']
  export const Form: typeof import('./types/form')['Form']
  export const FormItem: typeof import('./types/formItem')['FormItem']

  // Data
  export const Table: typeof import('./types/table')['Table']
  export const TableColumn: typeof import('./types/tableColumn')['TableColumn']
  export const Tag: typeof import('./types/tag')['Tag']
  export const Progress: typeof import('./types/progress')['Progress']
  export const Tree: typeof import('./types/tree')['Tree']
  export const Pagination: typeof import('./types/pagination')['Pagination']
  export const Badge: typeof import('./types/badge')['Badge']
  export const Avatar: typeof import('./types/avatar')['Avatar']
  export const Skeleton: typeof import('./types/skeleton')['Skeleton']
  export const SkeletonItem: typeof import('./types/skeletonItem')['SkeletonItem']
  export const Empty: typeof import('./types/empty')['Empty']
  export const Descriptions: typeof import('./types/descriptions')['Descriptions']
  export const DescriptionsItem: typeof import('./types/descriptionsItem')['DescriptionsItem']
  export const Result: typeof import('./types/result')['Result']
  export const Statistic: typeof import('./types/statistic')['Statistic']

  // Notice
  export const Alert: typeof import('./types/alert')['Alert']
  export const Loading: typeof import('./types/loading')['Loading']
  export const Message: typeof import('./types/message')['Message']
  export const MessageBox: typeof import('./types/messageBox')['MessageBox']
  export const Notification: typeof import('./types/notification')['Notification']

  // Navigation
  export const Menu: typeof import('./types/menu')['Menu']
  export const MenuItem: typeof import('./types/menuItem')['MenuItem']
  export const MenuItemGroup: typeof import('./types/menuItemGroup')['MenuItemGroup']
  export const Submenu: typeof import('./types/submenu')['Submenu']
  export const Tabs: typeof import('./types/tabs')['Tabs']
  export const TabPane: typeof import('./types/tabPane')['TabPane']
  export const Breadcrumb: typeof import('./types/breadcrumb')['Breadcrumb']
  export const BreadcrumbItem: typeof import('./types/breadcrumbItem')['BreadcrumbItem']
  export const PageHeader: typeof import('./types/pageHeader')['PageHeader']
  export const Dropdown: typeof import('./types/dropdown')['Dropdown']
  export const DropdownItem: typeof import('./types/dropdownItem')['DropdownItem']
  export const DropdownMenu: typeof import('./types/dropdownMenu')['DropdownMenu']
  export const Steps: typeof import('./types/steps')['Steps']
  export const Step: typeof import('./types/step')['Step']

  // Others
  export const Dialog: typeof import('./types/dialog')['Dialog']
  export const Tooltip: typeof import('./types/tooltip')['Tooltip']
  export const Popover: typeof import('./types/popover')['Popover']
  export const Popconfirm: typeof import('./types/popconfirm')['Popconfirm']
  export const Card: typeof import('./types/card')['Card']
  export const Carousel: typeof import('./types/carousel')['Carousel']
  export const CarouselItem: typeof import('./types/carouselItem')['CarouselItem']
  export const Collapse: typeof import('./types/collapse')['Collapse']
  export const CollapseItem: typeof import('./types/collapseItem')['CollapseItem']
  export const Timeline: typeof import('./types/timeline')['Timeline']
  export const TimelineItem: typeof import('./types/timelineItem')['TimelineItem']
  export const Divider: typeof import('./types/divider')['Divider']
  export const Calendar: typeof import('./types/calendar')['Calendar']
  export const Image: typeof import('./types/image')['Image']
  export const Backtop: typeof import('./types/backtop')['Backtop']
  export const Drawer: typeof import('./types/drawer')['Drawer']

  // Extras
  export const CollapseTransition: typeof import('./types/collapseTransition')['CollapseTransition']
  export const Scrollbar: typeof import('./types/scrollbar')['Scrollbar']

  // Install
  const ElementUI: {
    install: (...args: any) => void
  }

  export type ElementUIOptions = {
    size?: import('./types/_common').ElementUIComponentSize
    zIndex?: number
  }

  export default ElementUI
}
