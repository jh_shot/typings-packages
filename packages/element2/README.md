# ElementUI 类型定义

基于 ElementUI 2.15.14 版本文档编写

- 重写包引用 `package.d.ts`
- Vue引用 `vue.d.ts`
- Refs引用 `refs.d.ts`

`tsconfig.json` 示例：

```json
{
  "compilerOptions": {
    "types": [
      "@ttou/element2-typings/package",
      "@ttou/element2-typings/vue",
      "@ttou/element2-typings/refs"
    ]
  }
}
```

## 组件列表

- Basic
  - [x] Row
  - [x] Col
  - [x] Container
  - [x] Header
  - [x] Main
  - [x] Footer
  - [x] Aside
  - [x] Icon
  - [x] Button
  - [x] Link

- Form
  - [x] Radio
  - [x] Checkbox
  - [x] Input
  - [x] InputNumer
  - [x] Select
  - [x] Cascader
  - [x] Switch
  - [x] Slider
  - [x] TimePicker
  - [x] DatePicker
  - [x] DateTimePicker
  - [x] Upload
  - [x] Rate
  - [x] ColorPicker
  - [x] Transfer
  - [x] Form

- Data
  - [x] Table
  - [x] Tag
  - [x] Progress
  - [x] Tree
  - [x] Pagination
  - [x] Badge
  - [x] Avatar
  - [x] Skeleton
  - [x] Empty
  - [x] Descriptions
  - [x] Result
  - [x] Statistic

- Notice
  - [x] Alert
  - [x] Loading
  - [x] Message
  - [x] MessageBox
  - [x] Notification

- Navigation
  - [x] NavMenu
  - [x] Tabs
  - [x] Breadcrumb
  - [x] PageHeader
  - [x] Dropdown
  - [x] Steps

- Others
  - [x] Dialog
  - [x] Tooltip
  - [x] Popover
  - [x] Popconfirm
  - [x] Card
  - [x] Carousel
  - [x] Collapse
  - [x] Timeline
  - [x] Divider
  - [x] Calendar
  - [x] Image
  - [x] Backtop
  - [x] InfiniteScroll
  - [x] Drawer

- Extras
  - [x] CollapseTransition
  - [x] Scrollbar
