import { ElStep } from 'element-ui/types/step'

import { BaseComponent } from './_common'

declare interface StepProps extends Partial<Omit<ElStep, '$slots'>> {
  /**
   * Slots
   */
  scopedSlots?: StepSlots
}

declare type ElStepSlots = ElStep['$slots']

declare interface StepSlots {
  [k: keyof ElStepSlots]: () => any
}

declare interface _Step extends BaseComponent<StepProps, StepSlots> {}

export declare const Step: _Step
