import { ElSlider } from 'element-ui/types/slider'

import { BaseComponent, TemplateEvents } from './_common'

declare interface SliderProps extends Partial<ElSlider> {
  /**
   * Events
   */
  on?: SliderEvents
}

declare interface SliderEvents {
  /**
   * 值改变时触发（使用鼠标拖曳时，只在松开鼠标后触发）
   * @param value 改变后的值
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 数据改变时触发（使用鼠标拖曳时，活动过程实时触发）
   * @param value 改变后的值
   */
  ['input']?: (value) => void | Promise<void>
}

declare type SliderTemplateEvents = TemplateEvents<SliderEvents>

declare interface _Slider
  extends BaseComponent<SliderProps & SliderTemplateEvents> {}

export declare const Slider: _Slider
