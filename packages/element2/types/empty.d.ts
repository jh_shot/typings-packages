import { ELEmptySlots, ElEmpty } from 'element-ui/types/empty'

import { BaseComponent } from './_common'

declare interface EmptyProps extends Partial<Omit<ElEmpty, '$slots'>> {
  /**
   * Slots
   */
  scopedSlots?: EmptySlots
}

declare interface EmptySlots extends ELEmptySlots {}

declare interface _Empty extends BaseComponent<EmptyProps, EmptySlots> {}

export declare const Empty: _Empty
