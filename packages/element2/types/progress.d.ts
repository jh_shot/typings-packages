import { ElProgress } from 'element-ui/types/progress'

import { BaseComponent } from './_common'

declare interface ProgressProps extends Partial<ElProgress> {}

declare interface _Progress extends BaseComponent<ProgressProps> {}

export declare const Progress: _Progress
