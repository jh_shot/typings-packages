import { ElMenuItem } from 'element-ui/types/menu-item'

import { BaseComponent } from './_common'

declare interface MenuItemProps extends Partial<ElMenuItem> {}

declare interface _MenuItem extends BaseComponent<MenuItemProps> {}

export declare const MenuItem: _MenuItem
