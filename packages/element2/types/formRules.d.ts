type RuleType =
  | 'string'
  | 'number'
  | 'boolean'
  | 'method'
  | 'regexp'
  | 'integer'
  | 'float'
  | 'array'
  | 'object'
  | 'enum'
  | 'date'
  | 'url'
  | 'hex'
  | 'email'

interface RuleItem {
  type?: RuleType
  required?: boolean
  pattern?: string
  min?: number
  max?: number
  len?: number
  enum?: Array<string | number | boolean | null | undefined>
  whitespace?: boolean
  fields?: Rules
  options?: ValidateOption
  defaultField?: { type: RuleType }
  transform?: (value: any) => any
  message?: string
  validator?: (
    rule: Rules,
    value: any,
    callback: (error?: string | string[]) => void,
    source: ValidateSource,
    options: ValidateOption
  ) => void
  // ElementUI 附加属性
  trigger?: 'blur' | 'change'
}

interface Rules {
  [field: string]: RuleItem | RuleItem[]
}

interface ValidateSource {
  [field: string]: any
}

interface ValidateOption {
  first?: boolean
  firstFields?: boolean | string[]
}

interface ValidateError {
  message: string
  field: string
}

type ErrorList = ValidateError[]
interface FieldErrorList {
  [field: string]: ValidateError[]
}

export declare const FormRules: Rules
