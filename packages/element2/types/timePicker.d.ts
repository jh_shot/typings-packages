import { ElTimePicker } from 'element-ui/types/time-picker'

import { BaseComponent, TemplateEvents } from './_common'

declare type TimePickerMethods = 'focus'

declare interface TimePickerProps
  extends Partial<Omit<ElTimePicker, TimePickerMethods>> {
  /**
   * Events
   */
  on?: TimePickerEvents
}

declare interface TimePickerEvents {
  /**
   * 用户确认选定的值时触发
   * @param value 组件绑定值
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 当 input 失去焦点时触发
   * @param value 组件实例
   */
  ['blur']?: (value) => void | Promise<void>
  /**
   * 当 input 获得焦点时触发
   * @param value 组件实例
   */
  ['focus']?: (value) => void | Promise<void>
}

declare type TimePickerTemplateEvents = TemplateEvents<TimePickerEvents>

declare interface _TimePicker
  extends BaseComponent<TimePickerProps & TimePickerTemplateEvents> {}

declare interface _TimePickerRef
  extends Pick<ElTimePicker, TimePickerMethods> {}

export declare const TimePicker: _TimePicker

export declare const TimePickerRef: _TimePickerRef
