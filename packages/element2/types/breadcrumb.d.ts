import { ElBreadcrumb } from 'element-ui/types/breadcrumb'

import { BaseComponent } from './_common'

declare interface BreadcrumbProps extends Partial<ElBreadcrumb> {}

declare interface _Breadcrumb extends BaseComponent<BreadcrumbProps> {}

export declare const Breadcrumb: _Breadcrumb
