import { ElTooltip } from 'element-ui/types/tooltip'

import { BaseComponent } from './_common'

declare interface TooltipProps extends Partial<ElTooltip> {}

declare interface _Tooltip extends BaseComponent<TooltipProps> {}

export declare const Tooltip: _Tooltip
