import { ElCalendar } from 'element-ui/types/calendar'

import { BaseComponent } from './_common'

declare interface CalendarProps extends Partial<ElCalendar> {
  /**
   * Slots
   */
  scopedSlots?: CalendarSlots
}

declare interface CalendarSlots {
  /**
   * 自定义日历单元格中显示的内容
   *
   * date（当前单元格的日期）
   *
   * data（包括 type，isSelected，day 属性）
   */
  ['dateCell']?: ({ date, data }) => any
}

declare interface _Calendar
  extends BaseComponent<CalendarProps, CalendarSlots> {}

export declare const Calendar: _Calendar
