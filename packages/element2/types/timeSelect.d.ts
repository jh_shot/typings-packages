import { ElTimeSelect } from 'element-ui/types/time-select'

import { BaseComponent, TemplateEvents } from './_common'

declare type TimeSelectMethods = 'focus'

declare interface TimeSelectProps
  extends Partial<Omit<ElTimeSelect, TimeSelectMethods>> {
  /**
   * Events
   */
  on?: TimeSelectEvents
}

declare interface TimeSelectEvents {
  /**
   * 当 input 获得焦点时触发
   * @param value 组件实例
   */
  ['focus']?: (value) => void | Promise<void>
}

declare type TimeSelectTemplateEvents = TemplateEvents<TimeSelectEvents>

declare interface _TimeSelect
  extends BaseComponent<TimeSelectProps & TimeSelectTemplateEvents> {}

export declare const TimeSelect: _TimeSelect
