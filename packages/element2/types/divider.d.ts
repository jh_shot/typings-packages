import { ContentPosition, ElDivider } from 'element-ui/types/divider'

import { BaseComponent } from './_common'

declare interface DividerProps extends Partial<Omit<ElDivider, 'posiiton'>> {
  /**
   * customize the content on the divider line
   * @description 属性名错误，重写覆盖
   */
  contentPosition?: ContentPosition
}

declare interface _Divider extends BaseComponent<DividerProps> {}

export declare const Divider: _Divider
