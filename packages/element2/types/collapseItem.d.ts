import { ElCollapseItem } from 'element-ui/types/collapse-item'

import { BaseComponent } from './_common'

declare interface CollapseItemProps extends Partial<ElCollapseItem> {}

declare interface _CollapseItem extends BaseComponent<CollapseItemProps> {}

export declare const CollapseItem: _CollapseItem
