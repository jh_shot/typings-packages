import { ElStatistic } from 'element-ui/types/statistic'

import { BaseComponent, TemplateEvents } from './_common'

declare interface StatisticProps extends Partial<ElStatistic> {
  /**
   * Events
   */
  on?: StatisticEvents
  /**
   * Slots
   */
  scopedSlots?: StatisticSlots
}

declare interface StatisticEvents {
  /**
   * 在倒计时的功能中开启
   */
  ['change']?: (value: Date) => void | Promise<void>
  /**
   * 在倒计时 完成后启动
   */
  ['finish']?: (value: boolean) => void | Promise<void>
}

declare type StatisticTemplateEvents = TemplateEvents<StatisticEvents>

declare interface StatisticSlots {
  /**
   * 数值的前缀
   */
  ['prefix']?: () => any
  /**
   * 数值的后缀
   */
  ['suffix']?: () => any
  /**
   * 数值内容
   */
  ['formatter']?: () => any
  /**
   * 数值的标题
   */
  ['title']?: () => any
}

declare interface _Statistic
  extends BaseComponent<
    StatisticProps & StatisticTemplateEvents,
    StatisticSlots
  > {}

declare interface _StatisticRef {
  /**
   * 暂停倒计时
   * @param value
   */
  suspend: (value: boolean) => any
}

export declare const Statistic: _Statistic

export declare const StatisticRef: _StatisticRef
