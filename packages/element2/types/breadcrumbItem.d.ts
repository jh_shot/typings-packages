import { ElBreadcrumbItem } from 'element-ui/types/breadcrumb-item'

import { BaseComponent } from './_common'

declare interface BreadcrumbItemProps extends Partial<ElBreadcrumbItem> {}

declare interface _BreadcrumbItem extends BaseComponent<BreadcrumbItemProps> {}

export declare const BreadcrumbItem: _BreadcrumbItem
