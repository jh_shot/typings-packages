import { ElForm } from 'element-ui/types/form'

import { BaseComponent } from './_common'

declare type FormMethods =
  | 'validate'
  | 'validateField'
  | 'resetFields'
  | 'clearValidate'

declare interface FormProps extends Partial<Omit<ElForm, FormMethods>> {
  on?: {
    /**
     * 任一表单项被校验后触发
     * @param prop 被校验的表单项 prop 值
     * @param valid 校验是否通过
     * @param errorMessage 错误消息（如果存在）
     */
    ['validate']?: (prop, valid: boolean, errorMessage?: string) => any
  }
}

declare interface _Form extends BaseComponent<FormProps> {}

declare interface _FormRef extends Pick<ElForm, FormMethods> {}

export declare const Form: _Form

export declare const FormRef: _FormRef
