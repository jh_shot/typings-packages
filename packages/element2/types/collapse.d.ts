import { ElCollapse } from 'element-ui/types/collapse'

import { BaseComponent } from './_common'

declare interface CollapseProps extends Partial<ElCollapse> {
  on?: {
    /**
     * 当前激活面板改变时触发
     * 如果是手风琴模式，参数 activeNames 类型为string，否则为array
     *
     * @param activeNames 如果是手风琴模式，类型为string，否则为array
     */
    ['change']?: (activeNames: string | string[]) => void | Promise<void>
  }
}

declare interface _Collapse extends BaseComponent<CollapseProps> {}

export declare const Collapse: _Collapse
