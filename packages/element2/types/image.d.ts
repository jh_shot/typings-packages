import { ElImage } from 'element-ui/types/image'

import { BaseComponent, TemplateEvents } from './_common'

declare interface ImageProps extends Partial<Omit<ElImage, '$slots'>> {
  /**
   * Events
   */
  on?: ImageEvents
  /**
   * Slots
   */
  scopedSlots?: ImageSlots
}

declare interface ImageEvents {
  /**
   * 图片加载成功触发
   */
  ['load']?: (e: Event) => void | Promise<void>
  /**
   * 图片加载失败触发
   */
  ['error']?: (e: Error) => void | Promise<void>
}

declare type ImageTemplateEvents = TemplateEvents<ImageEvents>

declare type ElImageSlots = ElImage['$slots']

declare interface ImageSlots {
  [k: keyof ElImageSlots]: () => any
}

declare interface _Image
  extends BaseComponent<ImageProps & ImageTemplateEvents, ImageSlots> {}

export declare const Image: _Image
