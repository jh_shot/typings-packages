import { ElPopover } from 'element-ui/types/popover'

import { BaseComponent, TemplateEvents } from './_common'

declare interface PopoverProps extends Partial<Omit<ElPopover, '$slots'>> {
  /**
   * Events
   */
  on?: PopoverEvents
  /**
   * Slots
   */
  scopedSlots?: PopoverSlots
}

declare interface PopoverEvents {
  /**
   * 显示时触发
   */
  ['show']?: () => void | Promise<void>
  /**
   * 显示动画播放完毕后触发
   */
  ['after-enter']?: () => void | Promise<void>
  /**
   * 隐藏时触发
   */
  ['hide']?: () => void | Promise<void>
  /**
   * 隐藏动画播放完毕后触发
   */
  ['after-leave']?: void | Promise<void>
}

declare type PopoverTemplateEvents = TemplateEvents<PopoverEvents>

declare type ElPopoverSlots = ElPopover['$slots']

declare interface PopoverSlots {
  [k: keyof ElPopoverSlots]: () => any
}

declare interface _Popover
  extends BaseComponent<PopoverProps & PopoverTemplateEvents, PopoverSlots> {}

export declare const Popover: _Popover
