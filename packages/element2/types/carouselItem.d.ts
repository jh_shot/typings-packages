import { ElCarouselItem } from 'element-ui/types/carousel-item'

import { BaseComponent } from './_common'

declare interface CarouselItemProps extends Partial<ElCarouselItem> {}

declare interface _CarouselItem extends BaseComponent<CarouselItemProps> {}

export declare const CarouselItem: _CarouselItem
