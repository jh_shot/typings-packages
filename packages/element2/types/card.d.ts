import { ElCard } from 'element-ui/types/card'

import { BaseComponent } from './_common'

declare interface CardProps extends Partial<ElCard> {}

declare interface _Card extends BaseComponent<CardProps> {}

export declare const Card: _Card
