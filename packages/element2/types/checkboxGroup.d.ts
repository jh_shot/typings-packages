import { ElCheckboxGroup } from 'element-ui/types/checkbox-group'

import { BaseComponent, TemplateEvents } from './_common'

declare interface CheckboxGroupProps extends Partial<ElCheckboxGroup> {
  /**
   * Events
   */
  on?: CheckboxGroupEvents
}

declare interface CheckboxGroupEvents {
  /**
   * 当绑定值变化时触发的事件
   * @param value 更新后的值
   */
  ['change']?: (value) => void | Promise<void>
}

declare type CheckboxGroupTemplateEvents = TemplateEvents<CheckboxGroupEvents>

declare interface _CheckboxGroup
  extends BaseComponent<CheckboxGroupProps & CheckboxGroupTemplateEvents> {}

export declare const CheckboxGroup: _CheckboxGroup
