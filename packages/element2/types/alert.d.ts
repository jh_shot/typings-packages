import { ElAlert } from 'element-ui/types/alert'

import { BaseComponent, TemplateEvents } from './_common'

declare interface AlertProps extends Partial<ElAlert> {
  /**
   * Events
   */
  on?: AlertEvents
  /**
   * Slots
   */
  scopedSlots?: AlertSlots
}

declare interface AlertEvents {
  /**
   * 关闭alert时触发的事件
   */
  ['close']?: () => void | Promise<void>
}

declare type AlertTemplateEvents = TemplateEvents<AlertEvents>

declare interface AlertSlots {
  /**
   * 描述
   */
  ['default']?: () => any
  /**
   * 标题的内容
   */
  ['title']?: () => any
}

declare interface _Alert
  extends BaseComponent<AlertProps & AlertTemplateEvents, AlertSlots> {}

export declare const Alert: _Alert
