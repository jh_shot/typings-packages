import { ElOption } from 'element-ui/types/option'

import { BaseComponent } from './_common'

declare interface OptionProps extends Partial<ElOption> {}

declare interface _Option extends BaseComponent<OptionProps> {}

export declare const Option: _Option
