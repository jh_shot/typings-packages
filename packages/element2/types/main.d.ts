import { ElMain } from 'element-ui/types/main'

import { BaseComponent } from './_common'

declare interface MainProps extends Partial<ElMain> {}

declare interface _Main extends BaseComponent<MainProps> {}

export declare const Main: _Main
