import { ElRate } from 'element-ui/types/rate'

import { BaseComponent, TemplateEvents } from './_common'

declare interface RateProps extends Partial<ElRate> {
  /**
   * Events
   */
  on?: RateEvents
}

declare interface RateEvents {
  /**
   * 分值改变时触发
   * @param value 改变后的分值
   */
  ['change']?: (value) => any
}

declare type RateTemplateEvents = TemplateEvents<RateEvents>

declare interface _Rate extends BaseComponent<RateProps & RateTemplateEvents> {}

export declare const Rate: _Rate
