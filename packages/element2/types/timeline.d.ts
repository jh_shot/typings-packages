import { ElTimeline } from 'element-ui/types/timeline'

import { BaseComponent } from './_common'

declare interface TimelineProps extends Partial<ElTimeline> {}

declare interface _Timeline extends BaseComponent<TimelineProps> {}

export declare const Timeline: _Timeline
