import { ElTimelineItem } from 'element-ui/types/timeline-item'

import { BaseComponent } from './_common'

declare interface TimelineItemProps extends Partial<ElTimelineItem> {
  /**
   * Slots
   */
  scopedSlots?: TimelineItemSlots
}

declare interface TimelineItemSlots {
  /**
   * Timeline-Item 的内容
   */
  ['default']?: () => any
  /**
   * 自定义节点
   */
  ['dot']?: () => any
}

declare interface _TimelineItem
  extends BaseComponent<TimelineItemProps, TimelineItemSlots> {}

export declare const TimelineItem: _TimelineItem
