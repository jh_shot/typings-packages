import { ElTable } from 'element-ui/types/table'

import { BaseComponent, TemplateEvents } from './_common'

declare type TableMethods =
  | 'clearSelection'
  | 'toggleRowSelection'
  | 'toggleAllSelection'
  | 'toggleRowExpansion'
  | 'setCurrentRow'
  | 'clearSort'
  | 'clearFilter'
  | 'doLayout'
  | 'sort'

declare interface TableProps extends Partial<Omit<ElTable, TableMethods>> {
  /**
   * Events
   */
  on?: TableEvents
  /**
   * Slots
   */
  scopedSlots?: TableSlots
}

declare interface TableEvents {
  /**
   * 当用户手动勾选数据行的 Checkbox 时触发的事件
   */
  ['select']?: (selection, row) => void | Promise<void>
  /**
   * 当用户手动勾选全选 Checkbox 时触发的事件
   */
  ['select-all']?: (selection) => void | Promise<void>
  /**
   * 当选择项发生变化时会触发该事件
   */
  ['selection-change']?: (selection) => void | Promise<void>
  /**
   * 当单元格 hover 进入时会触发该事件
   */
  ['cell-mouse-enter']?: (row, column, cell, event) => void | Promise<void>
  /**
   * 当单元格 hover 退出时会触发该事件
   */
  ['cell-mouse-leave']?: (row, column, cell, event) => void | Promise<void>
  /**
   * 当某个单元格被点击时会触发该事件
   */
  ['cell-click']?: (row, column, cell, event) => void | Promise<void>
  /**
   * 当某个单元格被双击击时会触发该事件
   */
  ['cell-dblclick']?: (row, column, cell, event) => void | Promise<void>
  /**
   * 当某一行被点击时会触发该事件
   */
  ['row-click']?: (row, column, event) => void | Promise<void>
  /**
   * 当某一行被鼠标右键点击时会触发该事件
   */
  ['row-contextmenu']?: (row, column, event) => void | Promise<void>
  /**
   * 当某一行被双击时会触发该事件
   */
  ['row-dblclick']?: (row, column, event) => void | Promise<void>
  /**
   * 当某一列的表头被点击时会触发该事件
   */
  ['header-click']?: (column, event) => void | Promise<void>
  /**
   * 当某一列的表头被鼠标右键点击时触发该事件
   */
  ['header-contextmenu']?: (column, event) => void | Promise<void>
  /**
   * 当表格的排序条件发生变化的时候会触发该事件
   */
  ['sort-change']?: ({ column, prop, order }) => void | Promise<void>
  /**
   * 当表格的筛选条件发生变化的时候会触发该事件
   * @param filters 是一个对象，对象的 key 是 column 的 columnKey，对应的 value 为用户选择的筛选条件的数组
   */
  ['filter-change']?: (filters) => void | Promise<void>
  /**
   * 当表格的当前行发生变化的时候会触发该事件
   *
   * 如果要高亮当前行，请打开表格的 highlight-current-row 属性
   */
  ['current-change']?: (currentRow, oldCurrentRow) => void | Promise<void>
  /**
   * 当拖动表头改变了列的宽度的时候会触发该事件
   */
  ['header-dragend']?: (
    newWidth,
    oldWidth,
    column,
    event
  ) => void | Promise<void>
  /**
   * 当用户对某一行展开或者关闭的时候会触发该事件
   *
   * 展开行时，回调的第二个参数为 expandedRows；树形表格时第二参数为 expanded
   */
  ['expand-change']?: (row, expanded) => void | Promise<void>
}

declare type TableTemplateEvents = TemplateEvents<TableEvents>

declare interface TableSlots {
  /**
   * 插入至表格最后一行之后的内容，如果需要对表格的内容进行无限滚动操作，可能需要用到这个 slot
   *
   * 若表格有合计行，该 slot 会位于合计行之上。
   */
  ['append']?: () => any
}

declare interface _Table
  extends BaseComponent<TableProps & TableTemplateEvents, TableSlots> {}

declare interface _TableRef extends Pick<ElTable, TableMethods> {}

export declare const Table: _Table

export declare const TableRef: _TableRef
