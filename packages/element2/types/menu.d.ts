import { ElMenu } from 'element-ui/types/menu'

import { BaseComponent, TemplateEvents } from './_common'

declare type MenuMethods = 'open' | 'close'

declare interface MenuProps extends Partial<Omit<ElMenu, MenuMethods>> {
  /**
   * Events
   */
  on?: MenuEvents
}

declare interface MenuEvents {
  /**
   * 菜单激活回调
   * @param index 选中菜单项的 index
   * @param indexPath 选中菜单项的 index path
   */
  ['select']?: (index, indexPath) => void | Promise<void>
  /**
   * sub-menu 展开的回调
   * @param index 打开的 sub-menu 的 index
   * @param indexPath 打开的 sub-menu 的 index path
   */
  ['open']?: (index, indexPath) => void | Promise<void>
  /**
   * sub-menu 收起的回调
   * @param index 收起的 sub-menu 的 index
   * @param indexPath 收起的 sub-menu 的 index path
   */
  ['close']?: (index, indexPath) => void | Promise<void>
}

declare type MenuTemplateEvents = TemplateEvents<MenuEvents>

declare interface _Menu extends BaseComponent<MenuProps & MenuTemplateEvents> {}

declare interface _MenuRef {
  /**
   * 展开指定的 sub-menu
   * @param index 需要打开的 sub-menu 的 index
   */
  open: (index) => any
  /**
   * 收起指定的 sub-menu
   * @param index 需要收起的 sub-menu 的 index
   */
  close: (index) => any
}

export declare const Menu: _Menu

export declare const MenuRef: _MenuRef
