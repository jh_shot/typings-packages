import {
  ElCascaderPanel,
  CascaderPanelSlots as ElCascaderPanelSlots
} from 'element-ui/types/cascader-panel'

import { BaseComponent, TemplateEvents } from './_common'

declare interface CascaderPanelProps
  extends Partial<Omit<ElCascaderPanel, '$slots'>> {
  /**
   * Events
   */
  on?: CascaderPanelEvents
  /**
   * Slots
   */
  scopedSlots?: CascaderPanelSlots
}

declare interface CascaderPanelEvents {
  /**
   * 当选中节点变化时触发
   * @param value 选中节点的值
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 当展开节点发生变化时触发
   * @param value 各父级选项值组成的数组
   */
  ['expand-change']?: (value) => void | Promise<void>
}

declare type CascaderPanelTemplateEvents = TemplateEvents<CascaderPanelEvents>

declare interface CascaderPanelSlots {
  [k: keyof ElCascaderPanelSlots]: () => any
}

declare interface _CascaderPanel
  extends BaseComponent<
    CascaderPanelProps & CascaderPanelTemplateEvents,
    CascaderPanelSlots
  > {}

declare interface _CascaderPanelRef {
  /**
   * 获取选中的节点数组
   * @param leafOnly 是否只是叶子节点，默认值为 false
   */
  getCheckedNodes: (leafOnly) => any
  /**
   * 清空选中的节点
   */
  clearCheckedNodes: () => any
}

export declare const CascaderPanel: _CascaderPanel

export declare const CascaderPanelRef: _CascaderPanelRef
