import { ElCheckboxButton } from 'element-ui/types/checkbox-button'

import { BaseComponent } from './_common'

declare interface CheckboxButtonProps extends Partial<ElCheckboxButton> {}

declare interface _CheckboxButton extends BaseComponent<CheckboxButtonProps> {}

export declare const CheckboxButton: _CheckboxButton
