import { ElLink } from 'element-ui/types/link'

import { BaseComponent } from './_common'

declare interface LinkProps extends Partial<ElLink> {}

declare interface _Link extends BaseComponent<LinkProps> {}

export declare const Link: _Link
