import { ElInputNumber } from 'element-ui/types/input-number'

import { BaseComponent, TemplateEvents } from './_common'

declare type InputNumberMethods = 'focus'

declare interface InputNumberProps
  extends Partial<Omit<ElInputNumber, InputNumberMethods>> {
  /**
   * Events
   */
  on?: InputNumberEvents
}

declare interface InputNumberEvents {
  /**
   * 绑定值被改变时触发
   */
  ['change']?: (currentValue, oldValue) => void | Promise<void>
  /**
   * 在组件 Input 失去焦点时触发
   */
  ['blur']?: (event: Event) => void | Promise<void>
  /**
   * 在组件 Input 获得焦点时触发
   */
  ['focus']?: (event: Event) => void | Promise<void>
}

declare type InputNumberTemplateEvents = TemplateEvents<InputNumberEvents>

declare interface _InputNumber
  extends BaseComponent<InputNumberProps & InputNumberTemplateEvents> {}

declare interface _InputNumberRef
  extends Pick<ElInputNumber, InputNumberMethods> {
  /**
   * 选中 input 中的文字
   */
  select: () => any
}

export declare const InputNumber: _InputNumber

export declare const InputNumberRef: _InputNumberRef
