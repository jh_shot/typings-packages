import { ElTree, TreeData } from 'element-ui/types/tree'

import { BaseComponent, TemplateEvents } from './_common'

declare type TreeMethods =
  | 'filter'
  | 'updateKeyChildren'
  | 'getCheckedNodes'
  | 'setCheckedNodes'
  | 'getCheckedKeys'
  | 'setCheckedKeys'
  | 'setChecked'
  | 'getHalfCheckedNodes'
  | 'getHalfCheckedKeys'
  | 'getCurrentKey'
  | 'getCurrentNode'
  | 'setCurrentKey'
  | 'setCurrentNode'
  | 'getNode'
  | 'remove'
  | 'append'
  | 'insertBefore'
  | 'insertAfter'

declare type TreeDropType = 'before' | 'after' | 'inner'

declare interface TreeProps
  extends Partial<Omit<ElTree<TreeData, TreeData>, TreeMethods>> {
  /**
   * Events
   */
  on?: TreeEvents
  /**
   * Slots
   */
  scopedSlots?: TreeSlots
}

declare interface TreeEvents {
  /**
   * 节点被点击时的回调
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param node 节点对应的 Node
   * @param instance 节点组件本身
   */
  ['node-click']?: (nodeData, node, instance) => void | Promise<void>
  /**
   * 当某一节点被鼠标右键点击时会触发该事件
   * @param event 事件
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param node 节点对应的 Node
   * @param instance 节点组件本身
   */
  ['node-contextmenu']?: (
    event,
    nodeData,
    node,
    instance
  ) => void | Promise<void>
  /**
   * 节点选中状态发生变化时的回调
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param checked 节点本身是否被选中
   * @param indeterminate 节点的子树中是否有被选中的节点
   */
  ['check-change']?: (
    nodeData,
    checked: boolean,
    indeterminate: boolean
  ) => void | Promise<void>
  /**
   * 当复选框被点击的时候触发
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param treeData 树目前的选中状态对象
   */
  ['check']?: (nodeData, treeData) => void | Promise<void>
  /**
   * 当前选中节点变化时触发的事件
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param node 节点对应的 Node
   */
  ['current-change']?: (nodeData, node) => void | Promise<void>
  /**
   * 节点被展开时触发的事件
   * @param nodeData 传递给 data 属性的数组中该节点所对应的对象
   * @param node 节点对应的 Node
   * @param instance 节点组件本身
   */
  ['node-expand']?: (nodeData, node, instance) => void | Promise<void>
  /**
   * 节点被关闭时触发的事件
   */
  ['node-collapse']?: () => void | Promise<void>
  /**
   * 节点开始拖拽时触发的事件
   * @param draggingNode 被拖拽节点对应的 Node
   * @param oldDropNode 所离开节点对应的 Node
   * @param event 事件
   */
  ['node-drag-start']?: (
    draggingNode,
    oldDropNode,
    event
  ) => void | Promise<void>
  /**
   * 拖拽进入其他节点时触发的事件
   * @param draggingNode 被拖拽节点对应的 Node
   * @param oldDropNode 所离开节点对应的 Node
   * @param event 事件
   */
  ['node-drag-enter']?: (
    draggingNode,
    oldDropNode,
    event
  ) => void | Promise<void>
  /**
   * 拖拽离开某个节点时触发的事件
   * @param draggingNode 被拖拽节点对应的 Node
   * @param oldDropNode 所离开节点对应的 Node
   * @param event 事件
   */
  ['node-drag-leave']?: (
    draggingNode,
    oldDropNode,
    event
  ) => void | Promise<void>
  /**
   * 在拖拽节点时触发的事件（类似浏览器的 mouseover 事件）
   * @param draggingNode 被拖拽节点对应的 Node
   * @param dropNode 当前进入节点对应的 Node
   * @param event 事件
   */
  ['node-drag-over']?: (draggingNode, dropNode, event) => void | Promise<void>
  /**
   * 拖拽结束时（可能未成功）触发的事件
   * @param draggingNode 被拖拽节点对应的 Node
   * @param dropNode 当前进入节点对应的 Node
   * @param dropType 被拖拽节点的放置位置
   * @param event 事件
   */
  ['node-drag-end']?: (
    draggingNode,
    dropNode,
    dropType: TreeDropType,
    event
  ) => void | Promise<void>
  /**
   * 拖拽成功完成时触发的事件
   * @param draggingNode 被拖拽节点对应的 Node
   * @param dropNode 当前进入节点对应的 Node
   * @param dropType 被拖拽节点的放置位置
   * @param event 事件
   */
  ['node-drop']?: (
    draggingNode,
    dropNode,
    dropType: TreeDropType,
    event
  ) => void | Promise<void>
}

declare type TreeTemplateEvents = TemplateEvents<TreeEvents>

declare interface TreeSlots {
  /**
   * 自定义树节点的内容
   */
  ['default']?: ({ node, data }) => any
}

declare interface _Tree extends BaseComponent<TreeProps & TreeTemplateEvents> {}

declare interface _TreeRef
  extends Pick<ElTree<TreeData, TreeData>, TreeMethods> {}

export declare const Tree: _Tree

export declare const TreeRef: _TreeRef
