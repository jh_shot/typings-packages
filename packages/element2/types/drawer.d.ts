import { ElDrawer } from 'element-ui/types/drawer'

import { BaseComponent, TemplateEvents } from './_common'

declare interface DrawerProps extends Partial<Omit<ElDrawer, '$slots'>> {
  /**
   * Events
   */
  on?: ElDrawerEvents
  /**
   * Slots
   */
  scopedSlots?: DrawerSlots
}

declare interface ElDrawerEvents {
  /**
   * Drawer 打开的回调
   */
  ['open']?: () => void | Promise<void>
  /**
   * Drawer 打开动画结束时的回调
   */
  ['opened']?: () => void | Promise<void>
  /**
   * Drawer 关闭的回调
   */
  ['close']?: () => void | Promise<void>
  /**
   * Drawer 关闭动画结束时的回调
   */
  ['closed']?: () => void | Promise<void>
}

declare type ElDrawerTemplateEvents = TemplateEvents<ElDrawerEvents>

type ElDrawerSlots = ElDrawer['$slots']

declare interface DrawerSlots {
  [k: keyof ElDrawerSlots]: () => any
}

declare interface _Drawer
  extends BaseComponent<DrawerProps & ElDrawerTemplateEvents, DrawerSlots> {}

declare interface _DrawerRef {
  /**
   * 用于关闭 Drawer
   *
   * 该方法会调用传入的 `before-close` 方法
   */
  closeDrawer: () => any
}

export declare const Drawer: _Drawer

export declare const DrawerRef: _DrawerRef
