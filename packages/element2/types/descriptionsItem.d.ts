import { ElDescriptionsItem } from 'element-ui/types/descriptions-item'

import { BaseComponent } from './_common'

declare interface DescriptionsItemProps
  extends Partial<Omit<ElDescriptionsItem, '$slots'>> {
  /**
   * Slots
   */
  scopedSlots?: DescriptionsItemSlots
}

type ElDescriptionsItemSlots = ElDescriptionsItem['$slots']

declare interface DescriptionsItemSlots {
  [k: keyof ElDescriptionsItemSlots]: () => any
}

declare interface _DescriptionsItem
  extends BaseComponent<DescriptionsItemProps, DescriptionsItemSlots> {}

export declare const DescriptionsItem: _DescriptionsItem
