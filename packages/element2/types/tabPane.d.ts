import { ElTabPane } from 'element-ui/types/tab-pane'

import { BaseComponent } from './_common'

declare interface TabPaneProps extends Partial<ElTabPane> {}

declare interface _TabPane extends BaseComponent<TabPaneProps> {}

export declare const TabPane: _TabPane
