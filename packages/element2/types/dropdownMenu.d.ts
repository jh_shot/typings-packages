import { ElDropdownMenu } from 'element-ui/types/dropdown-menu'

import { BaseComponent } from './_common'

declare interface DropdownMenuProps extends Partial<ElDropdownMenu> {}

declare interface _DropdownMenu extends BaseComponent<DropdownMenuProps> {}

export declare const DropdownMenu: _DropdownMenu
