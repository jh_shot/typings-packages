import { ElDropdownItem } from 'element-ui/types/dropdown-item'

import { BaseComponent } from './_common'

declare interface DropdownItemProps extends Partial<ElDropdownItem> {}

declare interface _DropdownItem extends BaseComponent<DropdownItemProps> {}

export declare const DropdownItem: _DropdownItem
