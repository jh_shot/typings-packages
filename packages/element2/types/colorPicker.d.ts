import { ElColorPicker } from 'element-ui/types/color-picker'

import { BaseComponent, TemplateEvents } from './_common'

declare interface ColorPickerProps extends Partial<ElColorPicker> {
  /**
   * Events
   */
  on?: ColorPickerEvents
}

declare interface ColorPickerEvents {
  /**
   * 当绑定值变化时触发
   * @param value 当前值
   */
  ['change']?: (value) => any
  /**
   * 面板中当前显示的颜色发生改变时触发
   * @param value 当前显示的颜色值
   */
  ['active-change']?: () => any
}

declare type ColorPickerTemplateEvents = TemplateEvents<ColorPickerEvents>

declare interface _ColorPicker
  extends BaseComponent<ColorPickerProps & ColorPickerTemplateEvents> {}

export declare const ColorPicker: _ColorPicker
