import { ElTableColumn } from 'element-ui/types/table-column'

import { BaseComponent } from './_common'

declare interface TableColumnProps extends Partial<ElTableColumn> {}

declare interface _TableColumn extends BaseComponent<TableColumnProps> {}

export declare const TableColumn: _TableColumn
