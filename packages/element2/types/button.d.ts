import { ElButton } from 'element-ui/types/button'

import { BaseComponent } from './_common'

declare interface ButtonProps extends Partial<ElButton> {}

declare interface _Button extends BaseComponent<ButtonProps> {}

export declare const Button: _Button
