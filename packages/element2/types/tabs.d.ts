import { ElTabs } from 'element-ui/types/tabs'

import { BaseComponent, TemplateEvents } from './_common'

declare interface TabsProps extends Partial<ElTabs> {
  /**
   * Events
   */
  on?: TabsEvents
}

declare interface TabsEvents {
  /**
   * tab 被选中时触发
   * @param tab 被选中的标签 tab 实例
   */
  ['tab-click']?: (tab) => void | Promise<void>
  /**
   * 点击 tab 移除按钮后触发
   * @param name 被删除的标签的 name
   */
  ['tab-remove']?: (name) => void | Promise<void>
  /**
   * 点击 tabs 的新增按钮后触发
   */
  ['tab-add']?: () => void | Promise<void>
  /**
   * 点击 tabs 的新增按钮或 tab 被关闭后触发
   */
  ['edit']?: (targetName, action) => void | Promise<void>
}

declare type TabsTemplateEvents = TemplateEvents<TabsEvents>

declare interface _Tabs extends BaseComponent<TabsProps & TabsTemplateEvents> {}

export declare const Tabs: _Tabs
