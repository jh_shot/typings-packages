import { ElPageHeader } from 'element-ui/types/page-header'

import { BaseComponent, TemplateEvents } from './_common'

declare interface PageHeaderProps extends Partial<ElPageHeader> {
  /**
   * Events
   */
  on?: PageHeaderEvents
  /**
   * Slots
   */
  scopedSlots?: PageHeaderSlots
}

declare interface PageHeaderEvents {
  /**
   * 点击左侧区域触发
   */
  ['back']?: () => void | Promise<void>
}

declare type PageHeaderTemplateEvents = TemplateEvents<PageHeaderEvents>

declare interface PageHeaderSlots {
  /**
   * 标题内容
   */
  ['title']?: () => any
  /**
   * 内容
   */
  ['content']?: () => any
}

declare interface _PageHeader
  extends BaseComponent<
    PageHeaderProps & PageHeaderTemplateEvents,
    PageHeaderSlots
  > {}

export declare const PageHeader: _PageHeader
