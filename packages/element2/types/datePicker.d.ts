import { ElDatePicker } from 'element-ui/types/date-picker'

import { BaseComponent, TemplateEvents } from './_common'

declare type DatePickerMethods = 'focus'

declare interface DatePickerProps
  extends Partial<Omit<ElDatePicker, DatePickerMethods>> {
  /**
   * Events
   */
  on?: DatePickerEvents
  /**
   * Slots
   */
  scopedSlots?: DatePickerSlots
}

declare interface DatePickerEvents {
  /**
   * 用户确认选定的值时触发
   * @param value 组件绑定值。格式与绑定值一致，可受 `value-format` 控制
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 当 input 失去焦点时触发
   * @param value 组件实例
   */
  ['blur']?: (value) => void | Promise<void>
  /**
   * 当 input 获得焦点时触发
   * @param value 组件实例
   */
  ['focus']?: (value) => void | Promise<void>
}

declare type DatePickerTemplateEvents = TemplateEvents<DatePickerEvents>

declare interface DatePickerSlots {
  /**
   * 自定义分隔符
   */
  ['range-separator']?: () => any
}

declare interface _DatePicker
  extends BaseComponent<
    DatePickerProps & DatePickerTemplateEvents,
    DatePickerSlots
  > {}

declare interface _DatePickerRef
  extends Pick<ElDatePicker, DatePickerMethods> {}

export declare const DatePicker: _DatePicker

export declare const DatePickerRef: _DatePickerRef
