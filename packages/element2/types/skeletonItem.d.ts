import { ElSkeletonItem } from 'element-ui/types/skeleton-item'

import { BaseComponent } from './_common'

declare interface SkeletonItemProps extends Partial<ElSkeletonItem> {}

declare interface _SkeletonItem extends BaseComponent<SkeletonItemProps> {}

export declare const SkeletonItem: _SkeletonItem
