import { ElMenuItemGroup } from 'element-ui/types/menu-item-group'

import { BaseComponent } from './_common'

declare interface MenuItemProps extends Partial<ElMenuItemGroup> {}

declare interface _MenuItemGroup extends BaseComponent<MenuItemProps> {}

export declare const MenuItemGroup: _MenuItemGroup
