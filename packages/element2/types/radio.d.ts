import { ElRadio } from 'element-ui/types/radio'

import { BaseComponent, TemplateEvents } from './_common'

declare interface RadioProps extends Partial<ElRadio> {
  /**
   * Events
   */
  on?: RadioEvents
}

declare interface RadioEvents {
  /**
   * 绑定值变化时触发的事件
   * @param value 选中的 Radio label 值
   */
  ['input']?: (value) => void | Promise<void>
}

declare type RadioTemplateEvents = TemplateEvents<RadioEvents>

declare interface _Radio
  extends BaseComponent<RadioProps & RadioTemplateEvents> {}

export declare const Radio: _Radio
