import { ElResult, ElResultSlots } from 'element-ui/types/result'

import { BaseComponent } from './_common'

declare interface ResultProps extends Partial<Omit<ElResult, '$slots'>> {
  /**
   * Slots
   */
  scopedSlots?: ResultSlots
}

declare interface ResultSlots {
  [k: keyof ElResultSlots]: () => any
}

declare interface _Result extends BaseComponent<ResultProps, ResultSlots> {}

export declare const Result: _Result
