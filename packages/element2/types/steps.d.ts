import { ElSteps } from 'element-ui/types/steps'

import { BaseComponent } from './_common'

declare interface StepsProps extends Partial<ElSteps> {}

declare interface _Steps extends BaseComponent<StepsProps> {}

export declare const Steps: _Steps
