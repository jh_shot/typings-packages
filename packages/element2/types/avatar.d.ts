import { ElAvatar } from 'element-ui/types/avatar'

import { BaseComponent, TemplateEvents } from './_common'

declare interface AvatarProps extends Partial<ElAvatar> {
  /**
   * Events
   */
  on?: AvatarTemplateEvents
  /**
   * Slots
   */
  scopedSlots?: AvatarSlots
}

declare interface AvatarEvents {
  /**
   * 图片类头像加载失败的回调
   *
   *  返回 false 会关闭组件默认的 fallback 行为
   */
  ['error']?: (e: Event) => void | Promise<void>
}

declare type AvatarTemplateEvents = TemplateEvents<AvatarEvents>

declare interface AvatarSlots {
  /**
   * 自定义头像展示内容
   */
  ['default']?: () => any
}

declare interface _Avatar
  extends BaseComponent<AvatarProps & AvatarTemplateEvents, AvatarSlots> {}

export declare const Avatar: _Avatar
