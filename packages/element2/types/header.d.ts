import { ElHeader } from 'element-ui/types/header'

import { BaseComponent } from './_common'

declare interface HeaderProps extends Partial<ElHeader> {}

declare interface _Header extends BaseComponent<HeaderProps> {}

export declare const Header: _Header
