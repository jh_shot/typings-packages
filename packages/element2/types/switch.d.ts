import { ElSwitch } from 'element-ui/types/switch'

import { BaseComponent } from './_common'

declare interface SwitchProps extends Partial<ElSwitch> {
  on?: {
    /**
     * switch 状态发生变化时的回调函数
     * @param value 新状态的值
     */
    ['change']?: (value) => void | Promise<void>
  }
}

declare interface _Switch extends BaseComponent<SwitchProps> {}

declare interface _SwitchRef {
  /**
   * 使 Switch 获取焦点
   */
  focus: () => any
}

export declare const Switch: _Switch

export declare const SwitchRef: _SwitchRef
