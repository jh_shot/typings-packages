import { ElButtonGroup } from 'element-ui/types/button-group'

import { BaseComponent } from './_common'

declare interface ButtonGroupProps extends Partial<ElButtonGroup> {}

declare interface _ButtonGroup extends BaseComponent<ButtonGroupProps> {}

export declare const ButtonGroup: _ButtonGroup
