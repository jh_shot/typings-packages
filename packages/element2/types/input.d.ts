import { ElInput } from 'element-ui/types/input'

import { BaseComponent, TemplateEvents } from './_common'

declare type InputMethods = 'focus' | 'blur' | 'select'

declare interface InputProps
  extends Partial<Omit<ElInput, InputMethods | 'type'>> {
  type?: ElInput['type'] | 'password'

  /**
   * Events
   */
  on?: InputEvents
  /**
   * Slots
   */
  scopedSlots?: InputSlots
}

declare interface InputEvents {
  /**
   * 在 Input 失去焦点时触发
   */
  ['blur']?: (event: Event) => void | Promise<void>
  /**
   * 在 Input 获得焦点时触发
   */
  ['focus']?: (event: Event) => void | Promise<void>
  /**
   * 仅在输入框失去焦点或用户按下回车时触发
   */
  ['change']?: (value: string | number) => void | Promise<void>
  /**
   * 在 Input 值改变时触发
   */
  ['input']?: (value: string | number) => void | Promise<void>
  /**
   * 在点击由 clearable 属性生成的清空按钮时触发
   */
  ['clear']?: () => void | Promise<void>
}

declare type InputTemplateEvents = TemplateEvents<InputEvents>

declare interface InputSlots {
  /**
   * 输入框头部内容，只对 type="text" 有效
   */
  ['prefix']?: () => any
  /**
   * 输入框尾部内容，只对 type="text" 有效
   */
  ['suffix']?: () => any
  /**
   * 输入框前置内容，只对 type="text" 有效
   */
  ['prepend']?: () => any
  /**
   * 输入框后置内容，只对 type="text" 有效
   */
  ['append']?: () => any
}

declare interface _Input
  extends BaseComponent<InputProps & InputTemplateEvents, InputSlots> {}

declare interface _InputRef extends Pick<ElInput, InputMethods> {}

export declare const Input: _Input

export declare const InputRef: _InputRef
