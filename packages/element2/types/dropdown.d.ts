import { ElDropdown } from 'element-ui/types/dropdown'

import { BaseComponent, TemplateEvents } from './_common'

declare interface DropdownProps extends Partial<ElDropdown> {
  /**
   * Events
   */
  on?: DropdownEvents
  /**
   * Slots
   */
  scopedSlots?: DropdownSlots
}

declare interface DropdownEvents {
  /**
   * `split-button` 为 true 时，点击左侧按钮的回调
   */
  ['click']?: () => void | Promise<void>
  /**
   * 点击菜单项触发的事件回调
   * @param value dropdown-item 的指令
   */
  ['command']?: (value) => void | Promise<void>
  /**
   * 下拉框出现/隐藏时触发
   * @param value 出现则为 true，隐藏则为 false
   */
  ['visible-change']?: (value: boolean) => void | Promise<void>
}

declare type DropdownTemplateEvents = TemplateEvents<DropdownEvents>

declare interface DropdownSlots {
  /**
   * 触发下拉列表显示的元素
   *
   * 必须是一个元素或者或者组件
   */
  ['default']?: () => any
  /**
   * 下拉列表
   *
   * 通常是 `<el-dropdown-menu>` 组件
   */
  ['dropdown']?: () => any
}

declare interface _Dropdown
  extends BaseComponent<
    DropdownProps & DropdownTemplateEvents,
    DropdownSlots
  > {}

export declare const Dropdown: _Dropdown
