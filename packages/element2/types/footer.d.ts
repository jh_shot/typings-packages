import { ElFooter } from 'element-ui/types/footer'

import { BaseComponent } from './_common'

declare interface FooterProps extends Partial<ElFooter> {}

declare interface _Footer extends BaseComponent<FooterProps> {}

export declare const Footer: _Footer
