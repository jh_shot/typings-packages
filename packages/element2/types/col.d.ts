import { ElCol } from 'element-ui/types/col'

import { BaseComponent } from './_common'

declare interface ColProps extends Partial<ElCol> {}

declare interface _Col extends BaseComponent<ColProps> {}

export declare const Col: _Col
