import { ElUpload } from 'element-ui/types/upload'

import { BaseComponent } from './_common'

declare type UploadMethods = 'clearFiles' | 'abort' | 'submit'

declare interface UploadProps extends Partial<Omit<ElUpload, UploadMethods>> {
  /**
   * Slots
   */
  scopedSlots?: UploadSlots
}

declare interface UploadSlots {
  /**
   * 触发文件选择框的内容
   */
  ['trigger']?: () => any
  /**
   * 提示说明文字
   */
  ['tip']?: () => any
}

declare interface _Upload extends BaseComponent<UploadProps, UploadSlots> {}

declare interface _UploadRef extends Pick<ElUpload, UploadMethods> {}

export declare const Upload: _Upload

export declare const UploadRef: _UploadRef
