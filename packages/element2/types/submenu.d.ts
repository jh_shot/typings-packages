import { ElSubmenu } from 'element-ui/types/submenu'

import { BaseComponent } from './_common'

declare interface SubmenuProps extends Partial<ElSubmenu> {}

declare interface _Submenu extends BaseComponent<SubmenuProps> {}

export declare const Submenu: _Submenu
