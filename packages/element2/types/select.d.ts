import { ElSelect } from 'element-ui/types/select'

import { BaseComponent, TemplateEvents } from './_common'

declare type SelectMethods = 'focus' | 'blur'

declare interface SelectProps extends Partial<Omit<ElSelect, SelectMethods>> {
  /**
   * Events
   */
  on?: SelectEvents
  /**
   * Slots
   */
  scopedSlots?: SelectSlots
}

declare interface SelectEvents {
  /**
   * 选中值发生变化时触发
   * @param value 目前的选中值
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 下拉框出现/隐藏时触发
   * @param value 出现则为 `true`，隐藏则为 `false`
   */
  ['visible-change']?: (value) => void | Promise<void>
  /**
   * 多选模式下移除tag时触发
   * @param value 移除的tag值
   */
  ['remove-tag']?: (value) => void | Promise<void>
  /**
   * 可清空的单选模式下用户点击清空按钮时触发
   */
  ['clear']?: () => void | Promise<void>
  /**
   * 当 input 失去焦点时触发
   */
  ['blur']?: (event: Event) => void | Promise<void>
  /**
   * 当 input 获得焦点时触发
   */
  ['focus']?: (event: Event) => void | Promise<void>
}

declare type SelectTemplateEvents = TemplateEvents<SelectEvents>

declare interface SelectSlots {
  /**
   * Option 组件列表
   */
  ['default']?: () => any
  /**
   * Select 组件头部内容
   */
  ['prefix']?: () => any
  /**
   * 无选项时的列表
   */
  ['empty']?: () => any
}

declare interface _Select
  extends BaseComponent<SelectProps & SelectTemplateEvents, SelectSlots> {}

declare interface _SelectRef extends Pick<ElSelect, SelectMethods> {}

export declare const Select: _Select

export declare const SelectRef: _SelectRef
