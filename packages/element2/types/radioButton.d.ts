import { ElRadioButton } from 'element-ui/types/radio-button'

import { BaseComponent } from './_common'

declare interface RadioButtonProps extends Partial<ElRadioButton> {}

declare interface _RadioButton extends BaseComponent<RadioButtonProps> {}

export declare const RadioButton: _RadioButton
