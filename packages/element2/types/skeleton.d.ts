import { ElSkeleton } from 'element-ui/types/skeleton'

import { BaseComponent } from './_common'

declare interface SkeletonProps
  extends Partial<Omit<ElSkeleton, '$slots' | 'rows'>> {
  /**
   * numbers of the row, only useful when no template slot were given
   * @default 4
   */
  rows?: number
}

type ElSkeletonSlots = ElSkeleton['$slots']

declare interface SkeletonSlots {
  [k: keyof ElSkeletonSlots]: () => any
}

declare interface _Skeleton
  extends BaseComponent<SkeletonProps, ElSkeletonSlots> {}

export declare const Skeleton: _Skeleton
