import { ElAside } from 'element-ui/types/aside'

import { BaseComponent } from './_common'

declare interface AsideProps extends Partial<ElAside> {}

declare interface _Aside extends BaseComponent<AsideProps> {}

export declare const Aside: _Aside
