import { ElIcon } from 'element-ui/types/icon'

import { BaseComponent } from './_common'

declare interface IconProps extends Partial<ElIcon> {}

declare interface _Icon extends BaseComponent<IconProps> {}

export declare const Icon: _Icon
