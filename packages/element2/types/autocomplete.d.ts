import { ElAutocomplete } from 'element-ui/types/autocomplete'

import { BaseComponent, TemplateEvents } from './_common'

declare type AutocompleteMethods = 'focus'

declare interface AutocompleteProps
  extends Partial<Omit<ElAutocomplete, AutocompleteMethods>> {
  /**
   * Events
   */
  on?: AutocompleteEvents
  /**
   * Slots
   */
  scopedSlots?: AutocompleteSlots
}

declare interface AutocompleteEvents {
  /**
   * 点击选中建议项时触发
   * @param item 选中建议项
   */
  ['select']?: (item) => void | Promise<void>
  /**
   * 在 Input 值改变时触发
   */
  ['change']?: (value: string | number) => void | Promise<void>
}

declare type AutocompleteTemplateEvents = TemplateEvents<AutocompleteEvents>

declare interface AutocompleteSlots {
  /**
   * 输入框头部内容
   */
  ['prefix']?: (...args: any) => any
  /**
   * 输入框尾部内容
   */
  ['suffix']?: (...args: any) => any
  /**
   * 输入框前置内容
   */
  ['prepend']?: (...args: any) => any
  /**
   * 输入框后置内容
   */
  ['append']?: (...args: any) => any
}

declare interface _Autocomplete
  extends BaseComponent<
    AutocompleteProps & AutocompleteTemplateEvents,
    AutocompleteSlots
  > {}

declare interface _AutocompleteRef
  extends Pick<ElAutocomplete, AutocompleteMethods> {}

export declare const Autocomplete: _Autocomplete

export declare const AutocompleteRef: _AutocompleteRef
