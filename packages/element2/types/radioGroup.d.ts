import { ElementUIComponentSize } from 'element-ui/types/component'
import { ElRadioGroup } from 'element-ui/types/radio-group'

import { BaseComponent, TemplateEvents } from './_common'

declare interface RadioGroupProps extends Partial<Omit<ElRadioGroup, 'size'>> {
  /**
   * The size of radio buttons
   * @description 单选框组的类型有问题，使用全局的尺寸类型代替
   */
  size?: ElementUIComponentSize
  /**
   * Events
   */
  on?: RadioGroupEvents
}

declare interface RadioGroupEvents {
  /**
   * 绑定值变化时触发的事件
   * @param value 选中的 Radio label 值
   */
  ['input']?: (value) => void | Promise<void>
}

declare type RadioGroupTemplateEvents = TemplateEvents<RadioGroupEvents>

declare interface _RadioGroup
  extends BaseComponent<RadioGroupProps & RadioGroupTemplateEvents> {}

export declare const RadioGroup: _RadioGroup
