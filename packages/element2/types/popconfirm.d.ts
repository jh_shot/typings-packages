import { ElPopconfirm } from 'element-ui/types/popconfirm'

import { BaseComponent, TemplateEvents } from './_common'

declare interface PopconfirmProps extends Partial<ElPopconfirm> {
  /**
   * Events
   */
  on?: PopconfirmEvents
  /**
   * Slots
   */
  scopedSlots?: PopconfirmSlots
}

declare interface PopconfirmEvents {
  /**
   * 点击确认按钮时触发
   */
  ['confirm']?: () => void | Promise<void>
  /**
   * 点击取消按钮时触发
   */
  ['cancel']?: () => void | Promise<void>
}

declare type PopconfirmTemplateEvents = TemplateEvents<PopconfirmEvents>

declare interface PopconfirmSlots {
  /**
   * 触发 Popconfirm 显示的 HTML 元素
   */
  ['reference']?: () => any
}

declare interface _Popconfirm
  extends BaseComponent<
    PopconfirmProps & PopconfirmTemplateEvents,
    PopconfirmSlots
  > {}

export declare const Popconfirm: _Popconfirm
