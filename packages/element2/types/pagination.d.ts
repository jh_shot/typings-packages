import { ElPagination } from 'element-ui/types/pagination'

import { BaseComponent } from './_common'

declare interface PaginationProps extends Partial<ElPagination> {
  on?: {
    /**
     * pageSize 改变时会触发
     * @param value 每页条数
     */
    ['size-change']?: (value) => void | Promise<void>
    /**
     * currentPage 改变时会触发
     * @param value 当前页
     */
    ['current-change']?: (value) => void | Promise<void>
    /**
     * 用户点击上一页按钮改变当前页后触发
     * @param value 当前页
     */
    ['prev-click']?: (value) => void | Promise<void>
    /**
     * 用户点击下一页按钮改变当前页后触发
     * @param value 当前页
     */
    ['next-click']?: (value) => void | Promise<void>
  }
  /**
   * Slots
   */
  scopedSlots?: PaginationSlots
}

declare interface PaginationSlots {
  /**
   * 自定义内容
   */
  ['default']?: () => any
}

declare interface _Pagination
  extends BaseComponent<PaginationProps, PaginationSlots> {}

export declare const Pagination: _Pagination
