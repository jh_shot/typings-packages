import { ElFormItem } from 'element-ui/types/form-item'

import { BaseComponent } from './_common'

declare type FormItemMethods = 'resetField' | 'clearValidate'

declare interface FormItemProps
  extends Partial<Omit<ElFormItem, FormItemMethods>> {
  /**
   * Slots
   */
  scopedSlots?: FormItemSlots
}

declare interface FormItemSlots {
  /**
   * Form Item 的内容
   */
  ['default']?: () => any
  /**
   * 标签文本的内容
   */
  ['label']?: () => any
  /**
   * 自定义表单校验信息的显示方式
   */
  ['error']?: ({ error }) => any
}

declare interface _FormItem
  extends BaseComponent<FormItemProps, FormItemSlots> {}

declare interface _FormItemRef extends Pick<ElFormItem, FormItemMethods> {}

export declare const FormItem: _FormItem

export declare const FormItemRef: _FormItemRef
