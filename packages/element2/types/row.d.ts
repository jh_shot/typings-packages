import { ElRow } from 'element-ui/types/row'

import { BaseComponent } from './_common'

declare interface RowProps extends Partial<ElRow> {}

declare interface _Row extends BaseComponent<RowProps> {}

export declare const Row: _Row
