import { ElDescriptions } from 'element-ui/types/descriptions'

import { BaseComponent } from './_common'

declare interface DescriptionsProps
  extends Partial<Omit<ElDescriptions, '$slots'>> {
  /**
   * Slots
   */
  scopedSlots?: DescriptionsSlots
}

type ElDescriptionsSlots = ElDescriptions['$slots']

declare interface DescriptionsSlots {
  [k: keyof ElDescriptionsSlots]: () => any
}

declare interface _Descriptions
  extends BaseComponent<DescriptionsProps, ElDescriptionsSlots> {}

export declare const Descriptions: _Descriptions
