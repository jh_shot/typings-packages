import { ElTransfer, TransferPanelPosition } from 'element-ui/types/transfer'

import { BaseComponent, TemplateEvents } from './_common'

declare type TransferMethods = 'clearQuery'

declare interface TransferProps
  extends Partial<Omit<ElTransfer, TransferMethods>> {
  /**
   * Events
   */
  on?: TransferEvents
  /**
   * Slots
   */
  scopedSlots?: TransferSlots
}

declare interface TransferEvents {
  /**
   * 右侧列表元素变化时触发
   * @param value 当前值
   * @param position 数据移动的方向（'left' / 'right'）
   * @param moveArr 发生移动的数据 key 数组
   */
  ['change']?: (
    value,
    position: TransferPanelPosition,
    moveArr: any[]
  ) => void | Promise<void>
  /**
   * 左侧列表元素被用户选中 / 取消选中时触发
   * @param arr 当前被选中的元素的 key 数组
   * @param changeArr 选中状态发生变化的元素的 key 数组
   */
  ['left-check-change']?: (arr: any[], changeArr: any[]) => void | Promise<void>
  /**
   * 右侧列表元素被用户选中 / 取消选中时触发
   * @param arr 当前被选中的元素的 key 数组
   * @param changeArr 选中状态发生变化的元素的 key 数组
   */
  ['right-check-change']?: (
    arr: any[],
    changeArr: any[]
  ) => void | Promise<void>
}

declare type TransferTemplateEvents = TemplateEvents<TransferEvents>

declare interface TransferSlots {
  /**
   * 自定义数据项的内容
   */
  ['default']?: ({ option }) => any
  /**
   * 左侧列表底部的内容
   */
  ['left-footer']?: () => any
  /**
   * 右侧列表底部的内容
   */
  ['right-footer']?: () => any
}

declare interface _Transfer
  extends BaseComponent<
    TransferProps & TransferTemplateEvents,
    TransferSlots
  > {}

declare interface _TransferRef extends Pick<ElTransfer, TransferMethods> {}

export declare const Transfer: _Transfer

export declare const TransferRef: _TransferRef
