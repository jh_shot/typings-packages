import { ElDialog } from 'element-ui/types/dialog'

import { BaseComponent, TemplateEvents } from './_common'

declare interface DialogProps extends Partial<Omit<ElDialog, '$slots'>> {
  /**
   * 是否显示 Dialog，支持 .sync 修饰符
   * @default false
   */
  visible: boolean
  /**
   * Dialog 自身是否插入至 body 元素上。嵌套的 Dialog 必须指定该属性并赋值为 true
   * @default false
   */
  appendToBody?: boolean
  /**
   * Events
   */
  on?: ElDialogEvents
  /**
   * Slots
   */
  scopedSlots?: DialogSlots
}

declare interface ElDialogEvents {
  /**
   * Dialog 打开的回调
   */
  ['open']?: () => void | Promise<void>
  /**
   * Dialog 打开动画结束时的回调
   */
  ['opened']?: () => void | Promise<void>
  /**
   * Dialog 关闭的回调
   */
  ['close']?: () => void | Promise<void>
  /**
   * Dialog 关闭动画结束时的回调
   */
  ['closed']?: () => void | Promise<void>
}

declare type ElDialogTemplateEvents = TemplateEvents<ElDialogEvents>

declare type ElDialogSlots = ElDialog['$slots']

declare interface DialogSlots {
  [k: keyof ElDialogSlots]: () => any
}

declare interface _Dialog
  extends BaseComponent<DialogProps & ElDialogTemplateEvents, DialogSlots> {}

export declare const Dialog: _Dialog
