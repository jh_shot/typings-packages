import { ElTag } from 'element-ui/types/tag'

import { BaseComponent, TemplateEvents } from './_common'

declare interface TagProps extends Partial<ElTag> {
  /**
   * Events
   */
  on?: TagEvents
}

declare interface TagEvents {
  /**
   * 点击 Tag 时触发的事件
   */
  ['click']?: () => void | Promise<void>
  /**
   * 关闭 Tag 时触发的事件
   */
  ['close']?: () => void | Promise<void>
}

declare type TagTemplateEvents = TemplateEvents<TagEvents>

declare interface _Tag extends BaseComponent<TagProps & TagTemplateEvents> {}

export declare const Tag: _Tag
