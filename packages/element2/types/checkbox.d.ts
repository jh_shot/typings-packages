import { ElCheckbox } from 'element-ui/types/checkbox'

import { BaseComponent, TemplateEvents } from './_common'

declare interface CheckboxProps extends Partial<Omit<ElCheckbox, 'value'>> {
  value?: ElCheckbox['value'] | boolean
  /**
   * Events
   */
  on?: CheckboxEvents
}

declare interface CheckboxEvents {
  /**
   * 当绑定值变化时触发的事件
   * @param value 更新后的值
   */
  ['change']?: (value) => void | Promise<void>
}

declare type CheckboxTemplateEvents = TemplateEvents<CheckboxEvents>

declare interface _Checkbox
  extends BaseComponent<CheckboxProps & CheckboxTemplateEvents> {}

export declare const Checkbox: _Checkbox
