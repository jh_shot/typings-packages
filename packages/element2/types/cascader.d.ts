import {
  ElCascader,
  CascaderSlots as ElCascaderSlots
} from 'element-ui/types/cascader'

import { BaseComponent, TemplateEvents } from './_common'

declare interface CascaderProps extends Partial<Omit<ElCascader, '$slots'>> {
  /**
   * Events
   */
  on?: CascaderEvents
  /**
   * Slots
   */
  scopedSlots?: CascaderSlots
}

declare interface CascaderEvents {
  /**
   * 当选中节点变化时触发
   * @param value 选中节点的值
   */
  ['change']?: (value) => void | Promise<void>
  /**
   * 当展开节点发生变化时触发
   * @param value 各父级选项值组成的数组
   */
  ['expand-change']?: (value) => void | Promise<void>
  /**
   * 当失去焦点时触发
   */
  ['blur']?: (event: Event) => void | Promise<void>
  /**
   * 当获得焦点时触发
   */
  ['focus']?: (event: Event) => void | Promise<void>
  /**
   * 下拉框出现/隐藏时触发
   * @param value 出现则为 true，隐藏则为 false
   */
  ['visible-change']?: (value) => void | Promise<void>
  /**
   * 在多选模式下，移除Tag时触发
   * @param value 移除的Tag对应的节点的值
   */
  ['remove-tag']?: (value) => void | Promise<void>
}

declare type CascaderTemplateEvents = TemplateEvents<CascaderEvents>

declare interface CascaderSlots {
  [k: keyof ElCascaderSlots]: () => any
}

declare interface _Cascader
  extends BaseComponent<
    CascaderProps & CascaderTemplateEvents,
    CascaderSlots
  > {}

declare interface _CascaderRef {
  /**
   * 获取选中的节点
   * @param leafOnly 是否只是叶子节点，默认值为 false
   */
  getCheckedNodes: (leafOnly) => any
}

export declare const Cascader: _Cascader

export declare const CascaderRef: _CascaderRef
