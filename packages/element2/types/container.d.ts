import { ElContainer } from 'element-ui/types/container'

import { BaseComponent } from './_common'

declare interface ContainerProps extends Partial<ElContainer> {}

declare interface _Container extends BaseComponent<ContainerProps> {}

export declare const Container: _Container
