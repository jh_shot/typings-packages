import { ElBadge } from 'element-ui/types/badge'

import { BaseComponent } from './_common'

declare interface BadgeProps extends Partial<ElBadge> {}

declare interface _Badge extends BaseComponent<BadgeProps> {}

export declare const Badge: _Badge
