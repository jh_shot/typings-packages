import { ElOptionGroup } from 'element-ui/types/option-group'

import { BaseComponent } from './_common'

declare interface OptionGroupProps extends Partial<ElOptionGroup> {}

declare interface _OptionGroup extends BaseComponent<OptionGroupProps> {}

export declare const OptionGroup: _OptionGroup
