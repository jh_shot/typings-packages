import { ElementUIComponent } from 'element-ui/types/component'

import { BaseComponent } from './_common'

declare class ElScrollbar extends ElementUIComponent {
  /**
   * 容器的样式名
   */
  wrapClass: string
  /**
   * 展示视图样式名
   */
  viewClass: string
  /**
   * 容器的样式
   */
  wrapStyle: string
  /**
   * 展示视图样式
   */
  viewStyle: string
  /**
   * 是否使用原生滚动
   */
  native: boolean
  /**
   * 容器大小是否是不可变的
   */
  noresize: boolean
  /**
   * 渲染容器的标签
   */
  tag: string
}

declare interface ScrollbarProps extends Partial<ElScrollbar> {}

declare interface _Scrollbar extends BaseComponent<ScrollbarProps> {}

declare interface _ScrollbarRef {
  /**
   * 更新滚动条
   */
  update: () => void
}

export declare const Scrollbar: _Scrollbar
export declare const ScrollbarRef: _ScrollbarRef
