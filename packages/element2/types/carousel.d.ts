import { ElCarousel } from 'element-ui/types/carousel'

import { BaseComponent, TemplateEvents } from './_common'

declare type CarouselMethods = 'setActiveItem' | 'prev' | 'next'

declare interface CarouselProps
  extends Partial<Omit<ElCarousel, CarouselMethods>> {
  /**
   * Events
   */
  on?: CarouselEvents
}

declare interface CarouselEvents {
  /**
   * 幻灯片切换时触发
   * @param currentIndex 目前激活的幻灯片的索引
   * @param prevIndex 原幻灯片的索引
   */
  ['change']?: (currentIndex, prevIndex) => void | Promise<void>
}

declare type CarouselTemplateEvents = TemplateEvents<CarouselEvents>

declare interface _Carousel
  extends BaseComponent<CarouselProps & CarouselTemplateEvents> {}

declare interface _CarouselRef extends Pick<ElCarousel, CarouselMethods> {}

export declare const Carousel: _Carousel

export declare const CarouselRef: _CarouselRef
