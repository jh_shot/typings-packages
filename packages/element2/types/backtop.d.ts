import { ElBacktop } from 'element-ui/types/backtop'

import { BaseComponent, TemplateEvents } from './_common'

declare interface BacktopProps extends Partial<ElBacktop> {
  /**
   * Events
   */
  on?: BacktopEvents
}

declare interface BacktopEvents {
  /**
   * 点击按钮触发的事件
   */
  ['click']?: (e: Event) => void | Promise<void>
}

declare type BacktopTemplateEvents = TemplateEvents<BacktopEvents>

declare interface _Backtop
  extends BaseComponent<BacktopProps & BacktopTemplateEvents> {}

export declare const Backtop: _Backtop
