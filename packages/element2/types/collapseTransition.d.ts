import { BaseComponent } from './_common'

declare interface _CollapseTransition extends BaseComponent<any> {}

export declare const CollapseTransition: _CollapseTransition
