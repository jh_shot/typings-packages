declare type ElAutocompleteRef =
  typeof import('./types/autocomplete')['AutocompleteRef']
declare type ElInputRef = typeof import('./types/input')['InputRef']
declare type ElInputNumberRef =
  typeof import('./types/inputNumber')['InputNumberRef']
declare type ElSelectRef = typeof import('./types/select')['SelectRef']
declare type ElCascaderRef = typeof import('./types/cascader')['CascaderRef']
declare type ElCascaderPanelRef =
  typeof import('./types/cascaderPanel')['CascaderPanelRef']
declare type ElSwitchRef = typeof import('./types/switch')['SwitchRef']
declare type ElTimePickerRef =
  typeof import('./types/timePicker')['TimePickerRef']
declare type ElDatePickerRef =
  typeof import('./types/datePicker')['DatePickerRef']
declare type ElUploadRef = typeof import('./types/upload')['UploadRef']
declare type ElTransferRef = typeof import('./types/transfer')['TransferRef']
declare type ElFormRef = typeof import('./types/form')['FormRef']
declare type ElFormItemRef = typeof import('./types/formItem')['FormItemRef']
declare type ElTableRef = typeof import('./types/table')['TableRef']
declare type ElTreeRef = typeof import('./types/tree')['TreeRef']
declare type ElStatisticRef = typeof import('./types/statistic')['StatisticRef']
declare type ElMenuRef = typeof import('./types/menu')['MenuRef']
declare type ElCarouselRef = typeof import('./types/carousel')['CarouselRef']
declare type ElDrawerRef = typeof import('./types/drawer')['DrawerRef']

// 一些附加的类型
declare type ElFormRules = typeof import('./types/formRules')['FormRules']
declare type ElScrollbarRef = typeof import('./types/scrollbar')['ScrollbarRef']
