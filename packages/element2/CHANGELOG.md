# @ttou/element2-typings

## 2.2.6

### Patch Changes

- 代码格式化

## 2.2.5

### Patch Changes

- 修复类型

## 2.2.4

### Patch Changes

- 修复弹窗属性缺失

## 2.2.3

### Patch Changes

- 修复分割线属性名错误

## 2.2.2

### Patch Changes

- 修复类型

## 2.2.1

### Patch Changes

- 增加滚动条引用类型

## 2.2.0

### Minor Changes

- 优化默认导出类型

## 2.1.2

### Patch Changes

- 修复 input 类型

## 2.1.1

### Patch Changes

- 查漏补缺

## 2.1.0

### Minor Changes

- 只支持 vue 2.7

## 2.0.8

### Patch Changes

- 修复 Skeleton 类型

## 2.0.7

### Patch Changes

- 使用全局尺寸类型代替单选框组尺寸类型

## 2.0.6

### Patch Changes

- 更新配置和依赖
