declare module 'vue' {
  export interface GlobalComponents {
    // Basic
    ['el-col']: typeof import('./types/col')['Col']
    ['el-row']: typeof import('./types/row')['Row']
    ['el-container']: typeof import('./types/container')['Container']
    ['el-header']: typeof import('./types/header')['Header']
    ['el-main']: typeof import('./types/main')['Main']
    ['el-footer']: typeof import('./types/footer')['Footer']
    ['el-aside']: typeof import('./types/aside')['Aside']
    ['el-icon']: typeof import('./types/icon')['Icon']
    ['el-button']: typeof import('./types/button')['Button']
    ['el-button-group']: typeof import('./types/buttonGroup')['ButtonGroup']
    ['el-link']: typeof import('./types/link')['Link']

    // Form
    ['el-radio']: typeof import('./types/radio')['Radio']
    ['el-radio-button']: typeof import('./types/radioButton')['RadioButton']
    ['el-radio-group']: typeof import('./types/radioGroup')['RadioGroup']
    ['el-checkbox']: typeof import('./types/checkbox')['Checkbox']
    ['el-checkbox-button']: typeof import('./types/checkboxButton')['CheckboxButton']
    ['el-checkbox-group']: typeof import('./types/checkboxGroup')['CheckboxGroup']
    ['el-input']: typeof import('./types/input')['Input']
    ['el-input-number']: typeof import('./types/inputNumber')['InputNumber']
    ['el-autocomplete']: typeof import('./types/autocomplete')['Autocomplete']
    ['el-select']: typeof import('./types/select')['Select']
    ['el-option']: typeof import('./types/option')['Option']
    ['el-option-group']: typeof import('./types/optionGroup')
    ['el-cascader']: typeof import('./types/cascader')['Cascader']
    ['el-cascader-panel']: typeof import('./types/cascaderPanel')['CascaderPanel']
    ['el-switch']: typeof import('./types/switch')['Switch']
    ['el-slider']: typeof import('./types/slider')['Slider']
    ['el-time-picker']: typeof import('./types/timePicker')['TimePicker']
    ['el-time-select']: typeof import('./types/timeSelect')['TimeSelect']
    ['el-date-picker']: typeof import('./types/datePicker')['DatePicker']
    ['el-upload']: typeof import('./types/upload')['Upload']
    ['el-rate']: typeof import('./types/rate')['Rate']
    ['el-color-picker']: typeof import('./types/colorPicker')['ColorPicker']
    ['el-transfer']: typeof import('./types/transfer')['Transfer']
    ['el-form']: typeof import('./types/form')['Form']
    ['el-form-item']: typeof import('./types/formItem')['FormItem']

    // Data
    ['el-table']: typeof import('./types/table')['Table']
    ['el-table-column']: typeof import('./types/tableColumn')['TableColumn']
    ['el-tag']: typeof import('./types/tag')['Tag']
    ['el-progress']: typeof import('./types/progress')['Progress']
    ['el-tree']: typeof import('./types/tree')['Tree']
    ['el-pagination']: typeof import('./types/pagination')['Pagination']
    ['el-badge']: typeof import('./types/badge')['Badge']
    ['el-avatar']: typeof import('./types/avatar')['Avatar']
    ['el-skeleton']: typeof import('./types/skeleton')['Skeleton']
    ['el-skeleton-item']: typeof import('./types/skeletonItem')['SkeletonItem']
    ['el-empty']: typeof import('./types/empty')['Empty']
    ['el-descriptions']: typeof import('./types/descriptions')['Descriptions']
    ['el-descriptions-item']: typeof import('./types/descriptionsItem')['DescriptionsItem']
    ['el-result']: typeof import('./types/result')['Result']
    ['el-statistic']: typeof import('./types/statistic')['Statistic']

    // Notice
    ['el-alert']: typeof import('./types/alert')['Alert']

    // Navigation
    ['el-menu']: typeof import('./types/menu')['Menu']
    ['el-menu-item']: typeof import('./types/menuItem')['MenuItem']
    ['el-menu-item-group']: typeof import('./types/menuItemGroup')['MenuItemGroup']
    ['el-submenu']: typeof import('./types/submenu')['Submenu']
    ['el-tabs']: typeof import('./types/tabs')['Tabs']
    ['el-tab-pane']: typeof import('./types/tabPane')['TabPane']
    ['el-breadcrumb']: typeof import('./types/breadcrumb')['Breadcrumb']
    ['el-breadcrumb-item']: typeof import('./types/breadcrumbItem')['BreadcrumbItem']
    ['el-page-header']: typeof import('./types/pageHeader')['PageHeader']
    ['el-dropdown']: typeof import('./types/dropdown')['Dropdown']
    ['el-dropdown-item']: typeof import('./types/dropdownItem')['DropdownItem']
    ['el-dropdown-menu']: typeof import('./types/dropdownMenu')['DropdownMenu']
    ['el-steps']: typeof import('./types/steps')['Steps']
    ['el-step']: typeof import('./types/step')['Step']

    // Others
    ['el-dialog']: typeof import('./types/dialog')['Dialog']
    ['el-tooltip']: typeof import('./types/tooltip')['Tooltip']
    ['el-popover']: typeof import('./types/popover')['Popover']
    ['el-popconfirm']: typeof import('./types/popconfirm')['Popconfirm']
    ['el-card']: typeof import('./types/card')['Card']
    ['el-carousel']: typeof import('./types/carousel')['Carousel']
    ['el-carousel-item']: typeof import('./types/carouselItem')['CarouselItem']
    ['el-collapse']: typeof import('./types/collapse')['Collapse']
    ['el-collapse-item']: typeof import('./types/collapseItem')['CollapseItem']
    ['el-timeline']: typeof import('./types/timeline')['Timeline']
    ['el-timeline-item']: typeof import('./types/timelineItem')['TimelineItem']
    ['el-divider']: typeof import('./types/divider')['Divider']
    ['el-calendar']: typeof import('./types/calendar')['Calendar']
    ['el-image']: typeof import('./types/image')['Image']
    ['el-backtop']: typeof import('./types/backtop')['Backtop']
    ['el-drawer']: typeof import('./types/drawer')['Drawer']

    // Extras
    ['el-collapse-transition']: typeof import('./types/collapseTransition')['CollapseTransition']
    ['el-scrollbar']: typeof import('./types/scrollbar')['Scrollbar']
  }

  export interface ComponentCustomProperties {
    ['$loading']: typeof import('./types/loading')['Loading']
    ['$message']: typeof import('./types/message')['Message']
    ['$msgbox']: typeof import('./types/messageBox')['MessageBox']
    ['$alert']: typeof import('./types/messageBox')['MessageBox']['alert']
    ['$confirm']: typeof import('./types/messageBox')['MessageBox']['confirm']
    ['$prompt']: typeof import('./types/messageBox')['MessageBox']['prompt']
    ['$notify']: typeof import('./types/notification')['Notification']
  }
}

export type {}
