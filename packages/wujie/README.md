# 无界类型定义

基于 Wujie 1.0.18 版本编写

- Vue组件 引用 `vue.d.ts`
- JSX组件引用 `jsx.d.ts`
- 全局变量引用 `global.d.ts`

`tsconfig.json` 示例：

```json
{
  "compilerOptions": {
    "types": [
      "@ttou/wujie-typings/global",
      "@ttou/wujie-typings/jsx"
    ]
  }
}
```
