# @ttou/vant2-typings

## 2.2.1

### Patch Changes

- 代码格式化

## 2.2.0

### Minor Changes

- 同步 2.13.2

## 2.1.2

### Patch Changes

- 查漏补缺

## 2.1.1

### Patch Changes

- 修复函数式组件调用

## 2.1.0

### Minor Changes

- 只支持 vue 2.7

## 2.0.2

### Patch Changes

- 更新依赖和配置
