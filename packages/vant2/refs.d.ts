declare type VanCalendarRef = typeof import('./types/calendar')['CalendarRef']
declare type VanCheckboxRef = typeof import('./types/checkbox')['CheckboxRef']
declare type VanCheckboxGroupRef =
  typeof import('./types/checkboxGroup')['CheckboxGroupRef']
declare type VanDatetimePickerRef =
  typeof import('./types/datetimePicker')['DatetimePickerRef']
declare type VanFieldRef = typeof import('./types/field')['FieldRef']
declare type VanFormRef = typeof import('./types/form')['FormRef']
declare type VanPickerRef = typeof import('./types/picker')['PickerRef']
declare type VanUploaderRef = typeof import('./types/uploader')['UploaderRef']
declare type VanIndexBarRef = typeof import('./types/indexBar')['IndexBarRef']
declare type VanTabsRef = typeof import('./types/tabs')['TabsRef']
declare type VanCollapseItemRef =
  typeof import('./types/collapseItem')['CollapseItemRef']
declare type VanNoticeBarRef =
  typeof import('./types/noticeBar')['NoticeBarRef']
declare type VanSwipeRef = typeof import('./types/swipe')['SwipeRef']
declare type VanListRef = typeof import('./types/list')['ListRef']
declare type VanCountDown = typeof import('./types/countDown')['CountDownRef']
declare type VanDropdownItemRef =
  typeof import('./types/dropdownItem')['DropdownItemRef']
declare type VanSwipeCellRef =
  typeof import('./types/swipeCell')['SwipeCellRef']
declare type VanSkuRef = typeof import('./types/sku')['SkuRef']
declare type VanAreaRef = typeof import('./types/area')['AreaRef']
declare type VanAddressEditRef =
  typeof import('./types/addressEdit')['AddressEditRef']
