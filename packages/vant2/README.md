# Vant 类型定义

基于 Vant 2.12.54 版本文档编写

- 重写包引用 `package.d.ts`
- Vue引用 `vue.d.ts`
- Refs引用 `refs.d.ts`

`tsconfig.json` 示例：

```json
{
  "compilerOptions": {
    "types": [
      "@ttou/vant2-typings/package",
      "@ttou/vant2-typings/vue",
      "@ttou/vant2-typings/refs"
    ]
  }
}
```

## 组件列表

- 基础组件
  - [x] Button
  - [x] Cell
  - [x] Icon
  - [x] Image
  - [x] Layout
  - [x] Popup
  - [x] Toast
- 表单组件
  - [x] Calendar
  - [x] Cascader
  - [x] Checkbox
  - [x] DatetimePicker
  - [x] Field
  - [x] Form
  - [x] NumberKeyboard
  - [x] PasswordInput
  - [x] Picker
  - [x] Radio
  - [x] Rate
  - [x] Search
  - [x] Slider
  - [x] Stepper
  - [x] Switch
  - [x] Uploader
- 反馈组件
  - [x] ActionSheet
  - [x] Dialog
  - [x] DropdownMenu
  - [x] Loading
  - [x] Notify
  - [x] Overlay
  - [x] PullRefresh
  - [x] ShareSheet
  - [x] SwipeCell
- 展示组件
  - [x] Badge
  - [x] Circle
  - [x] Collapse
  - [x] CountDown
  - [x] Divider
  - [x] Empty
  - [x] ImagePreview
  - [x] Lazyload
  - [x] List
  - [x] NoticeBar
  - [x] Popover
  - [x] Progress
  - [x] Skeleton
  - [x] Steps
  - [x] Sticky
  - [x] Swipe
  - [x] Tag
- 导航组件
  - [x] Grid
  - [x] IndexBar
  - [x] NavBar
  - [x] Pagination
  - [x] Sidebar
  - [x] Tab
  - [x] Tabbar
  - [x] TreeSelect
- 业务组件
  - [x] AddressEdit
  - [x] AddressList
  - [x] Area
  - [x] Card
  - [x] ContactCard
  - [x] ContactEdit
  - [x] ContactList
  - [x] Coupon
  - [x] GoodsAction
  - [x] SubmitBar
  - [x] Sku
