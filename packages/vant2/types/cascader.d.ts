import { BaseComponent, TemplateEvents } from './_common'

declare interface CascaderProps {
  /**
   * 顶部标题
   */
  title?: string
  /**
   * 选中项的值
   */
  value?: string | number
  /**
   * 可选项数据源
   * @default []
   */
  options?: any[]
  /**
   * 未选中时的提示文案
   * @default "请选择"
   */
  placeholder?: string
  /**
   * 选中状态的高亮颜色
   * @default "#ee0a24"
   */
  activeColor?: string
  /**
   * 是否显示关闭图标
   * @default true
   */
  closeable?: boolean
  /**
   * 是否展示标题栏
   * @default true
   */
  showHeader?: boolean
  /**
   * 自定义 `options` 结构中的字段
   * @default { text: 'text', value: 'value', children: 'children' }
   */
  fieldNames?: {
    text?: string
    value?: string
    children?: string
  }
  /**
   * Events
   */
  on?: CascaderEvents
  /**
   * Slots
   */
  scopedSlots?: CascaderSlots
}

declare interface CascaderEvents {
  /**
   * 选中项变化时触发
   */
  ['change']?: (params: { value; selectedOptions; tabIndex }) => any
  /**
   * 全部选项选择完成后触发
   */
  ['finish']?: (params: { value; selectedOptions; tabIndex }) => any
  /**
   * 点击关闭图标时触发
   */
  ['close']?: () => any
}

declare type CascaderTemplateEvents = TemplateEvents<CascaderEvents>

declare interface CascaderSlots {
  /**
   * 自定义顶部标题
   */
  ['title']?: () => any
  /**
   * 自定义选项文字
   */
  ['option']?: (params: {
    option: Record<string, any>
    selected: boolean
  }) => any
}

declare interface _Cascader
  extends BaseComponent<
    CascaderProps & CascaderTemplateEvents,
    CascaderSlots
  > {}

export declare const Cascader: _Cascader
