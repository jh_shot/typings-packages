export interface AllowedComponentProps {
  class?: unknown
  style?: unknown
}

export interface VNodeProps {
  key?: string | number | symbol
  ref?: unknown
}

export type TemplateEvents<T> = {
  // @ts-ignore
  [P in keyof T as `on${Capitalize<P>}`]: T[P]
}

export interface BaseComponent<T, K = any> {
  new (): {
    $props: AllowedComponentProps & VNodeProps & T
    $slots: K
  }
  install: (vue: any) => void
}
