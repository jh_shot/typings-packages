import { BaseComponent, TemplateEvents } from './_common'

declare interface TimeData {
  /**
   * 剩余天数
   */
  days: number
  /**
   * 剩余小时
   */
  hours: number
  /**
   * 剩余分钟
   */
  minutes: number
  /**
   * 剩余秒数
   */
  seconds: number
  /**
   * 剩余毫秒
   */
  milliseconds: number
}

declare interface CountDownProps {
  /**
   * 倒计时时长，单位毫秒
   * @default 0
   */
  time?: string | number
  /**
   * 时间格式
   * @default "HH:mm:ss"
   */
  format?: string
  /**
   * 是否自动开始倒计时
   * @default true
   */
  autoStart?: boolean
  /**
   * 是否开启毫秒级渲染
   * @default false
   */
  millisecond?: boolean
  /**
   * Events
   */
  on?: CountDownEvents
  /**
   * Slots
   */
  scopedSlots?: CountDownSlots
}

declare interface CountDownEvents {
  /**
   * 倒计时结束时触发
   */
  ['finish']?: () => any
  /**
   * 倒计时变化时触发
   */
  ['change']?: (timeData: TimeData) => any
}

declare type CountDownTemplateEvents = TemplateEvents<CountDownEvents>

declare interface CountDownSlots {
  /**
   * 自定义内容
   */
  ['default']?: (timeData: TimeData) => any
}

declare interface _CountDown
  extends BaseComponent<
    CountDownProps & CountDownTemplateEvents,
    CountDownSlots
  > {}

declare interface _CountDownRef {
  /**
   * 开始倒计时
   */
  ['start']?: () => any
  /**
   * 暂停倒计时
   */
  ['pause']?: () => any
  /**
   * 重设倒计时，若 `auto-start` 为 `true`，重设后会自动开始倒计时
   */
  ['reset']?: () => any
}

export declare const CountDown: _CountDown

export declare const CountDownRef: _CountDownRef
