import { BaseComponent, TemplateEvents } from './_common'

declare interface StickyProps {
  /**
   * 吸顶时与顶部的距离，支持 `px` `vw` `vh` `rem` 单位，默认 `px`
   * @default 0
   */
  offsetTop?: string | number
  /**
   * 吸顶时的 z-index
   * @default 99
   */
  zIndex?: string | number
  /**
   * 容器对应的 HTML 节点
   */
  container?: any
  /**
   * Events
   */
  on?: StickyEvents
}

declare interface StickyEvents {
  /**
   * 当吸顶状态改变时触发
   */
  ['change']?: (isFixed: boolean) => any
  /**
   * 滚动时触发
   */
  ['scroll']?: (params: { scrollTop: number; isFixed: boolean }) => any
}

declare type StickyTemplateEvents = TemplateEvents<StickyEvents>

declare interface _Sticky
  extends BaseComponent<StickyProps & StickyTemplateEvents> {}

export declare const Sticky: _Sticky
