import { BaseComponent, TemplateEvents } from './_common'

declare interface Option {
  /**
   * 分享渠道名称
   */
  name: string
  /**
   * 分享选项描述
   */
  description?: string
  /**
   * 图标，可选值为 `wechat` `weibo` `qq` `link` `qrcode` `poster` `weapp-qrcode` `wechat-moments`，支持传入图片 URL
   */
  icon?: string
  /**
   * 分享选项类名
   */
  className?: string
}

declare interface ShareSheetProps {
  /**
   * 分享选项
   * @default []
   */
  options?: Option[]
  /**
   * 顶部标题
   */
  title?: string
  /**
   * 取消按钮文字，传入空字符串可以隐藏按钮
   * @default "取消"
   */
  cancelText?: string
  /**
   * 标题下方的辅助描述文字
   */
  description?: string
  /**
   * 动画时长，单位秒
   * @default 0.3
   */
  duration?: string | number
  /**
   * 是否显示遮罩层
   * @default true
   */
  overlay?: boolean
  /**
   * 是否锁定背景滚动
   * @default true
   */
  lockScroll?: boolean
  /**
   * 是否在显示弹层时才渲染内容
   * @default true
   */
  lazyRender?: boolean
  /**
   * 是否在页面回退时自动关闭
   * @default true
   */
  closeOnPopstate?: boolean
  /**
   * 是否在点击遮罩层后关闭
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * Events
   */
  on?: ShareSheetEvents
  /**
   * Slots
   */
  scopedSlots?: ShareSheetSlots
}

declare interface ShareSheetEvents {
  /**
   * 点击分享选项时触发
   */
  ['select']?: (option: Option, index: number) => any
  /**
   * 点击取消按钮时触发
   */
  ['cancel']?: () => any
  /**
   * 点击遮罩层时触发
   */
  ['click-overlay']?: () => any
}

declare type ShareSheetTemplateEvents = TemplateEvents<ShareSheetEvents>

declare interface ShareSheetSlots {
  /**
   * 自定义顶部标题
   */
  ['title']?: () => any
  /**
   * 自定义描述文字
   */
  ['description']?: () => any
}

declare interface _ShareSheet
  extends BaseComponent<
    ShareSheetProps & ShareSheetTemplateEvents,
    ShareSheetSlots
  > {}

export declare const ShareSheet: _ShareSheet
