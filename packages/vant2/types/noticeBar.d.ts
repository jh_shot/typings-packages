import { BaseComponent, TemplateEvents } from './_common'

declare interface NoticeBarProps {
  /**
   * 通知栏模式
   * @default ""
   */
  mode?: 'closeable' | 'link'
  /**
   * 通知文本内容
   * @default ""
   */
  text?: string
  /**
   * 通知文本颜色
   * @default "#f60"
   */
  color?: string
  /**
   * 滚动条背景
   * @default "#fff7cc"
   */
  background?: string
  /**
   * 左侧图标名称或图片链接
   */
  leftIcon?: string
  /**
   * 动画延迟时间 (s)
   * @default 1
   */
  delay?: string | number
  /**
   * 滚动速率 (px/s)
   * @default 60
   */
  speed?: string | number
  /**
   * 是否开启滚动播放，内容长度溢出时默认开启
   */
  scrollable?: boolean
  /**
   * 是否开启文本换行，只在禁用滚动时生效
   * @default false
   */
  wrapable?: boolean
  /**
   * Events
   */
  on?: NoticeBarEvents
  /**
   * Slots
   */
  scopedSlots?: NoticeBarSlots
}

declare interface NoticeBarEvents {
  /**
   * 点击通知栏时触发
   */
  ['click']?: (event: Event) => any
  /**
   * 关闭通知栏时触发
   */
  ['close']?: (event: Event) => any
  /**
   * 每当滚动栏重新开始滚动时触发
   */
  ['replay']?: () => any
}

declare type NoticeBarTemplateEvents = TemplateEvents<NoticeBarEvents>

declare interface NoticeBarSlots {
  /**
   * 通知文本内容
   */
  ['default']?: () => any
  /**
   * 自定义左侧图标
   */
  ['left-icon']?: () => any
  /**
   * 自定义右侧图标
   */
  ['right-icon']?: () => any
}

declare interface _NoticeBar
  extends BaseComponent<
    NoticeBarProps & NoticeBarTemplateEvents,
    NoticeBarSlots
  > {}

declare interface _NoticeBarRef {
  /**
   * 重置通知栏到初始状态
   */
  reset: () => any
}

export declare const NoticeBar: _NoticeBar

export declare const NoticeBarRef: _NoticeBarRef
