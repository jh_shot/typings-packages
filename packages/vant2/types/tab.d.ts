import { BaseComponent } from './_common'

declare interface TabProps {
  /**
   * 标题
   */
  title?: string
  /**
   * 是否禁用标签
   * @default false
   */
  disabled?: boolean
  /**
   * 是否在标题右上角显示小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 标签名称，作为匹配的标识符
   * @default 标签的索引值
   */
  name?: string | number
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * 自定义标题样式
   */
  titleStyle?: any
  /**
   * 自定义标题类名
   */
  titleClass?: any
  /**
   * Slots
   */
  scopedSlots?: TabSlots
}

declare interface TabSlots {
  /**
   * 标签页内容
   */
  ['default']?: () => any
  /**
   * 自定义标题
   */
  ['title']?: () => any
}

declare interface _Tab extends BaseComponent<TabProps, TabSlots> {}

export declare const Tab: _Tab
