import {
  DialogAction,
  DialogOptions,
  Dialog as _Dialog
} from 'vant/types/dialog'

import { BaseComponent } from './_common'

declare type DialogComponentProps = DialogOptions

declare interface _DialogComponent
  extends BaseComponent<DialogComponentProps> {}

export declare const Dialog: Omit<_Dialog, 'Component'> & {
  (options: DialogOptions): Promise<DialogAction>
  Component: _DialogComponent
}
