import { BaseComponent } from './_common'

declare interface DropdownMenuProps {
  /**
   * 菜单标题和选项的选中态颜色
   * @default "#ee0a24"
   */
  activeColor?: string
  /**
   * 菜单展开方向
   * @default "down"
   */
  direction?: 'up' | 'down'
  /**
   * 菜单栏 z-index 层级
   * @default 10
   */
  zIndex?: string | number
  /**
   * 动画时长，单位秒
   * @default 0.2
   */
  duration?: string | number
  /**
   * 是否显示遮罩层
   * @default true
   */
  overlay?: boolean
  /**
   * 是否在点击遮罩层后关闭菜单
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 是否在点击外部元素后关闭菜单
   * @default true
   */
  closeOnClickOutside?: boolean
}

declare interface _DropdownMenu extends BaseComponent<DropdownMenuProps> {}

export declare const DropdownMenu: _DropdownMenu
