import { BaseComponent, TemplateEvents } from './_common'

declare interface CardProps {
  /**
   * 左侧图片 URL
   */
  thumb?: string
  /**
   * 标题
   */
  title?: string
  /**
   * 描述
   */
  desc?: string
  /**
   * 图片角标
   */
  tag?: string
  /**
   * 商品数量
   */
  num?: string | number
  /**
   * 商品价格
   */
  price?: string | number
  /**
   * 商品划线原价
   */
  originPrice?: string | number
  /**
   * 内容是否垂直居中
   * @default false
   */
  centered?: boolean
  /**
   * 货币符号
   * @default "¥"
   */
  currency?: string
  /**
   * 点击左侧图片后跳转的链接地址
   */
  thumbLink?: string
  /**
   * 是否开启图片懒加载，须配合 Lazyload 组件使用
   * @default false
   */
  lazyLoad?: boolean
  /**
   * Events
   */
  on?: CardEvents
  /**
   * Slots
   */
  scopedSlots?: CardSlots
}

declare interface CardEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
  /**
   * 点击自定义图片时触发
   */
  ['click-thumb']?: (event: Event) => any
}

declare type CardTemplateEvents = TemplateEvents<CardEvents>

declare interface CardSlots {
  /**
   * 自定义标题
   */
  ['title']?: () => any
  /**
   * 自定义描述
   */
  ['desc']?: () => any
  /**
   * 自定义数量
   */
  ['num']?: () => any
  /**
   * 自定义价格
   */
  ['price']?: () => any
  /**
   * 自定义商品原价
   */
  ['origin-price']?: () => any
  /**
   * 自定义价格上方区域
   */
  ['price-top']?: () => any
  /**
   * 自定义价格下方区域
   */
  ['bottom']?: () => any
  /**
   * 自定义图片
   */
  ['thumb']?: () => any
  /**
   * 自定义图片角标
   */
  ['tag']?: () => any
  /**
   * 自定义描述下方标签区域
   */
  ['tags']?: () => any
  /**
   * 自定义右下角内容
   */
  ['footer']?: () => any
}

declare interface _Card
  extends BaseComponent<CardProps & CardTemplateEvents, CardSlots> {}

export declare const Card: _Card
