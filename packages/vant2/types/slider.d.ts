import { BaseComponent, TemplateEvents } from './_common'

declare interface SliderProps {
  /**
   * 当前进度百分比
   * @default 0
   */
  value?: number | number[]
  /**
   * 最大值
   * @default 100
   */
  max?: string | number
  /**
   * 最小值
   * @default 0
   */
  min?: string | number
  /**
   * 步长
   * @default 1
   */
  step?: string | number
  /**
   * 进度条高度，默认单位为`px`
   * @default "2px"
   */
  barHeight?: string | number
  /**
   * 滑块按钮大小，默认单位为`px`
   * @default "24px"
   */
  buttonSize?: string | number
  /**
   * 进度条激活态颜色
   * @default "#1989fa"
   */
  activeColor?: string
  /**
   * 进度条非激活态颜色
   * @default "#e5e5e5"
   */
  inactiveColor?: string
  /**
   * 是否开启双滑块模式
   * @default false
   */
  range?: boolean
  /**
   * 是否禁用滑块
   * @default false
   */
  disabled?: boolean
  /**
   * 是否垂直展示
   * @default false
   */
  vertical?: boolean
  /**
   * Events
   */
  on?: SliderEvents
  /**
   * Slots
   */
  scopedSlots?: SliderSlots
}

declare interface SliderEvents {
  /**
   * 进度变化时实时触发
   */
  ['input']?: (value) => any
  /**
   * 进度变化且结束拖动后触发
   */
  ['change']?: (value) => any
  /**
   * 开始拖动时触发
   */
  ['drag-start']?: () => any
  /**
   * 结束拖动时触发
   */
  ['drag-end']?: () => any
}

declare type SliderTemplateEvents = TemplateEvents<SliderEvents>

declare interface SliderSlots {
  /**
   * 自定义滑动按钮
   */
  ['button']?: () => any
  /**
   * 自定义左侧滑块按钮（双滑块模式下）
   */
  ['left-button']?: (params: { value: number }) => any
  /**
   * 自定义右侧滑块按钮（双滑块模式下）
   */
  ['right-button']?: (params: { value: number }) => any
}

declare interface _Slider
  extends BaseComponent<SliderProps & SliderTemplateEvents, SliderSlots> {}

export declare const Slider: _Slider
