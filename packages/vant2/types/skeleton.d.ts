import { BaseComponent } from './_common'

declare interface SkeletonProps {
  /**
   * 段落占位图行数
   * @default 0
   */
  row?: string | number
  /**
   * 段落占位图宽度，可传数组来设置每一行的宽度
   * @default "100%"
   */
  rowWidth?: string | number | (string | number)[]
  /**
   * 是否显示标题占位图
   * @default false
   */
  title?: boolean
  /**
   * 是否显示头像占位图
   * @default false
   */
  avatar?: boolean
  /**
   * 是否显示骨架屏，传 `false` 时会展示子组件内容
   * @default true
   */
  loading?: boolean
  /**
   * 是否开启动画
   * @default true
   */
  animate?: boolean
  /**
   * 是否将标题和段落显示为圆角风格
   * @default false
   */
  round?: boolean
  /**
   * 标题占位图宽度
   * @default "40%"
   */
  titleWidth?: string | number
  /**
   * 头像占位图大小
   * @default "32px"
   */
  avatarSize?: string | number
  /**
   * 头像占位图形状
   * @default "round"
   */
  avatarShape?: 'square' | 'round'
}

declare interface _Skeleton extends BaseComponent<SkeletonProps> {}

export declare const Skeleton: _Skeleton
