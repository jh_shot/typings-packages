import { BaseComponent } from './_common'

declare interface CollapseItemProps {
  /**
   * 唯一标识符，默认为索引值
   */
  name?: string | number
  /**
   * 标题栏左侧图标名称或图片链接
   */
  icon?: string
  /**
   * 标题栏大小
   */
  size?: 'large'
  /**
   * 标题栏左侧内容
   */
  title?: string | number
  /**
   * 标题栏右侧内容
   */
  value?: string | number
  /**
   * 标题栏描述信息
   */
  label?: string | number
  /**
   * 是否显示内边框
   * @default true
   */
  border?: boolean
  /**
   * 是否展示标题栏右侧箭头并开启点击反馈
   * @default true
   */
  isLink?: boolean
  /**
   * 是否禁用面板
   * @default false
   */
  disabled?: boolean
  /**
   * 是否在首次展开时才渲染面板内容
   * @default true
   */
  lazyRender?: boolean
  /**
   * 左侧标题额外类名
   */
  titleClass?: any
  /**
   * 右侧内容额外类名
   */
  valueClass?: any
  /**
   * 描述信息额外类名
   */
  labelClass?: any
  /**
   * Slots
   */
  scopedSlots?: CollapseItemSlots
}

declare interface CollapseItemSlots {
  /**
   * 面板内容
   */
  ['default']?: () => any
  /**
   * 自定义显示内容
   */
  ['value']?: () => any
  /**
   * 自定义 `icon`
   */
  ['icon']?: () => any
  /**
   * 自定义 `title`
   */
  ['title']?: () => any
  /**
   * 自定义右侧按钮，默认是 `arrow`
   */
  ['right-icon']?: () => any
}

declare interface _CollapseItem
  extends BaseComponent<CollapseItemProps, CollapseItemSlots> {}

declare interface _CollapseItemRef {
  /**
   * 切换面试展开状态，传 `true` 为展开，`false` 为收起，不传参为切换
   */
  toggle: (expand?: boolean) => any
}

export declare const CollapseItem: _CollapseItem

export declare const CollapseItemRef: _CollapseItemRef
