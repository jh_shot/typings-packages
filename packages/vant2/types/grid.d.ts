import { BaseComponent } from './_common'

declare interface GridProps {
  /**
   * 列数
   * @default 4
   */
  columnNum?: string | number
  /**
   * 图标大小，默认单位为`px`
   * @default "28px"
   */
  iconSize?: string | number
  /**
   * 格子之间的间距，默认单位为`px`
   * @default 0
   */
  gutter?: string | number
  /**
   * 是否显示边框
   * @default false
   */
  border?: boolean
  /**
   * 是否将格子内容居中显示
   * @default true
   */
  center?: boolean
  /**
   * 是否将格子固定为正方形
   * @default false
   */
  square?: boolean
  /**
   * 是否开启格子点击反馈
   * @default false
   */
  clickable?: boolean
  /**
   * 格子内容排列的方向
   * @default "vertical"
   */
  direction?: 'horizontal' | 'vertical'
}

declare interface _Grid extends BaseComponent<GridProps> {}

export declare const Grid: _Grid
