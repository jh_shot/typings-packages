import { BaseComponent, TemplateEvents } from './_common'

export declare type ButtonType =
  | 'default'
  | 'primary'
  | 'info'
  | 'warning'
  | 'danger'

declare interface ButtonProps {
  /**
   * 类型
   * @default "default"
   */
  type?: ButtonType
  /**
   * 尺寸
   * @default "normal"
   */
  size?: 'normal' | 'large' | 'small' | 'mini'
  /**
   * 按钮文字
   */
  text?: string
  /**
   * 按钮颜色，支持传入 `linear-gradient` 渐变色
   */
  color?: string
  /**
   * 左侧图标名称或图片链接
   */
  icon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 图标展示位置
   * @default "left"
   */
  iconPosition?: 'left' | 'right'
  /**
   * 按钮根节点的 HTML 标签
   * @default "button"
   */
  tag?: string
  /**
   * 原生 button 标签的 type 属性
   */
  nativeType?: string
  /**
   * 是否为块级元素
   * @default false
   */
  block?: boolean
  /**
   * 是否为朴素按钮
   * @default false
   */
  plain?: boolean
  /**
   * 是否为方形按钮
   * @default false
   */
  square?: boolean
  /**
   * 是否为圆形按钮
   * @default false
   */
  round?: boolean
  /**
   * 是否禁用按钮
   * @default false
   */
  disabled?: boolean
  /**
   * 是否使用 0.5px 边框
   * @default false
   */
  hairline?: boolean
  /**
   * 是否显示为加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 加载状态提示文字
   */
  loadingText?: string
  /**
   * 加载图标类型
   * @default "circular"
   */
  loadingType?: 'spinner' | 'circular'
  /**
   * 加载图标大小
   * @default "20px"
   */
  loadingSize?: string
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Events
   */
  on?: ButtonEvents
  /**
   * Slots
   */
  scopedSlots?: ButtonSlots
}

declare interface ButtonEvents {
  /**
   * 点击按钮，且按钮状态不为加载或禁用时触发
   */
  ['click']?: (e: Event) => any
  /**
   * 开始触摸按钮时触发
   */
  ['touchstart']?: (e: TouchEvent) => any
}

declare type ButtonTemplateEvents = TemplateEvents<ButtonEvents>

declare interface ButtonSlots {
  /**
   * 按钮内容
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['icon']?: () => any
  /**
   * 自定义加载图标
   */
  ['loading']?: () => any
}

declare interface _Button
  extends BaseComponent<ButtonProps & ButtonTemplateEvents, ButtonSlots> {}

export declare const Button: _Button
