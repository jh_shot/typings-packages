import { BaseComponent, TemplateEvents } from './_common'

declare interface NavBarProps {
  /**
   * 标题
   * @default ""
   */
  title?: string
  /**
   * 左侧文案
   * @default ""
   */
  leftText?: string
  /**
   * 右侧文案
   * @default ""
   */
  rightText?: string
  /**
   * 是否显示左侧箭头
   * @default false
   */
  leftArrow?: boolean
  /**
   * 是否显示下边框
   * @default true
   */
  border?: boolean
  /**
   * 是否固定在顶部
   * @default false
   */
  fixed?: boolean
  /**
   * 固定在顶部时，是否在标签位置生成一个等高的占位元素
   * @default false
   */
  placeholder?: boolean
  /**
   * 导航栏 z-index
   * @default 1
   */
  zIndex?: string | number
  /**
   * 是否开启顶部安全区适配
   * @default false
   */
  safeAreaInsetTop?: boolean
  /**
   * Events
   */
  on?: NavBarEvents
  /**
   * Slots
   */
  scopedSlots?: NavBarSlots
}

declare interface NavBarEvents {
  /**
   * 点击左侧按钮时触发
   */
  ['click-left']?: () => any
  /**
   * 点击右侧按钮时触发
   */
  ['click-right']?: () => any
}

declare type NavBarTemplateEvents = TemplateEvents<NavBarEvents>

declare interface NavBarSlots {
  /**
   * 自定义标题
   */
  ['title']?: () => any
  /**
   * 自定义左侧区域内容
   */
  ['left']?: () => any
  /**
   * 自定义右侧区域内容
   */
  ['right']?: () => any
}

declare interface _NavBar
  extends BaseComponent<NavBarProps & NavBarTemplateEvents, NavBarSlots> {}

export declare const NavBar: _NavBar
