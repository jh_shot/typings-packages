import { BaseComponent, TemplateEvents } from './_common'

declare interface NumberKeyboardProps {
  /**
   * 当前输入值
   */
  value?: string
  /**
   * 是否显示键盘
   */
  show?: boolean
  /**
   * 键盘标题
   */
  title?: string
  /**
   * 样式风格
   */
  theme?: 'default' | 'custom'
  /**
   * 输入值最大长度
   */
  maxlength?: string | number
  /**
   * 是否开启过场动画
   * @default true
   */
  transition?: boolean
  /**
   * 键盘 z-index 层级
   * @default 100
   */
  zIndex?: string | number
  /**
   * 底部额外按键的内容
   */
  extraKey?: string | string[]
  /**
   * 关闭按钮文字，空则不展示
   */
  closeButtonText?: string
  /**
   * 删除按钮文字，空则展示删除图标
   */
  deleteButtonText?: string
  /**
   * 是否将关闭按钮设置为加载中状态，仅在 `theme="custom"` 时有效
   * @default false
   */
  closeButtonLoading?: boolean
  /**
   * 是否展示删除图标
   * @default true
   */
  showDeleteKey?: boolean
  /**
   * 点击外部时是否收起键盘
   * @default true
   */
  hideOnClickOutside?: boolean
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * 是否将通过随机顺序展示按键
   * @default false
   */
  randomKeyOrder?: boolean
  /**
   * Events
   */
  on?: NumberKeyboardEvents
  /**
   * Slots
   */
  scopedSlots?: NumberKeyboardSlots
}

declare interface NumberKeyboardEvents {
  /**
   * 点击按键时触发
   */
  ['input']?: (key) => any
  /**
   * 点击删除键时触发
   */
  ['delete']?: () => any
  /**
   * 点击关闭按钮时触发
   */
  ['close']?: () => any
  /**
   * 点击关闭按钮或非键盘区域时触发
   */
  ['blur']?: () => any
  /**
   * 键盘完全弹出时触发
   */
  ['show']?: () => any
  /**
   * 键盘完全收起时触发
   */
  ['hide']?: () => any
}

declare type NumberKeyboardTemplateEvents = TemplateEvents<NumberKeyboardEvents>

declare interface NumberKeyboardSlots {
  /**
   * 自定义删除按键内容
   */
  ['delete']?: () => any
  /**
   * 自定义左下角按键内容
   */
  ['extra-key']?: () => any
  /**
   * 自定义标题栏左侧内容
   */
  ['title-left']?: () => any
}

declare interface _NumberKeyboard
  extends BaseComponent<
    NumberKeyboardProps & NumberKeyboardTemplateEvents,
    NumberKeyboardSlots
  > {}

export declare const NumberKeyboard: _NumberKeyboard
