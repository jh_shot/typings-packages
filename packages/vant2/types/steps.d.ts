import { BaseComponent, TemplateEvents } from './_common'

declare interface StepsProps {
  /**
   * 当前步骤
   * @default 0
   */
  active?: string | number
  /**
   * 显示方向
   * @default "horizontal"
   */
  direction?: 'vertical' | 'horizontal'
  /**
   * 激活状态颜色
   * @default "#07c160"
   */
  activeColor?: string
  /**
   * 未激活状态颜色
   * @default "#969799"
   */
  inactiveColor?: string
  /**
   * 激活状态底部图标
   * @default "checked"
   */
  activeIcon?: string
  /**
   * 未激活状态底部图标
   */
  inactiveIcon?: string
  /**
   * 已完成步骤对应的底部图标，优先级高于 `inactive-icon`
   */
  finishIcon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 是否进行居中对齐，仅在竖向展示时有效
   * @default false
   */
  center?: boolean
  /**
   * Events
   */
  on?: StepsEvents
}

declare interface StepsEvents {
  /**
   * 点击步骤的标题或图标时触发
   */
  ['click-step']?: (index: number) => any
}

declare type StepsTemplateEvents = TemplateEvents<StepsEvents>

declare interface _Steps
  extends BaseComponent<StepsProps & StepsTemplateEvents> {}

export declare const Steps: _Steps
