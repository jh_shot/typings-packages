import { BaseComponent, TemplateEvents } from './_common'

declare interface ContactCardProps {
  /**
   * 卡片类型
   * @default "add"
   */
  type?: 'add' | 'edit'
  /**
   * 联系人姓名
   */
  name?: string
  /**
   * 联系人手机号
   */
  tel?: string
  /**
   * 添加时的文案提示
   * @default "添加联系人"
   */
  addText?: string
  /**
   * Events
   */
  on?: ContactCardEvents
}

declare interface ContactCardEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type ContactCardTemplateEvents = TemplateEvents<ContactCardEvents>

declare interface _ContactCard
  extends BaseComponent<ContactCardProps & ContactCardTemplateEvents> {}

export declare const ContactCard: _ContactCard
