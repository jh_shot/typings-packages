import { BaseComponent, TemplateEvents } from './_common'

declare interface RadioProps {
  /**
   * 标识符
   */
  name?: any
  /**
   * 形状
   */
  shape?: 'square' | 'round'
  /**
   * 是否为禁用状态
   * @default false
   */
  disabled?: boolean
  /**
   * 是否禁用文本内容点击
   * @default false
   */
  labelDisabled?: boolean
  /**
   * 文本位置
   * @default "right"
   */
  labelPosition?: 'left' | 'right'
  /**
   * 图标大小，默认单位为`px`
   * @default "20px"
   */
  iconSize?: string | number
  /**
   * 选中状态颜色
   * @default "#1989fa"
   */
  checkedColor?: string
  /**
   * Events
   */
  on?: RadioEvents
  /**
   * Slots
   */
  scopedSlots?: RadioSlots
}

declare interface RadioEvents {
  /**
   * 点击单选框时触发
   */
  ['click']?: (event: Event) => any
}

declare type RadioTemplateEvents = TemplateEvents<RadioEvents>

declare interface RadioSlots {
  /**
   * 自定义文本
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['icon']?: (checked: boolean) => any
}

declare interface _Radio
  extends BaseComponent<RadioProps & RadioTemplateEvents, RadioSlots> {}

export declare const Radio: _Radio
