import { BaseComponent, TemplateEvents } from './_common'

declare interface ColProps {
  /**
   * 列元素宽度
   */
  span?: string | number
  /**
   * 列元素偏移距离
   */
  offset?: string | number
  /**
   * 自定义元素标签
   * @default "div"
   */
  tag?: string
  /**
   * Events
   */
  on?: ColEvents
}

declare interface ColEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type ColTemplateEvents = TemplateEvents<ColEvents>

declare interface _Col extends BaseComponent<ColProps & ColTemplateEvents> {}

export declare const Col: _Col
