import { BaseComponent } from './_common'

declare interface ProgressProps {
  /**
   * 进度百分比
   * @default 0
   */
  percentage?: string | number
  /**
   * 进度条粗细，默认单位为`px`
   * @default "4px"
   */
  strokeWidth?: string | number
  /**
   * 进度条颜色
   * @default "#1989fa"
   */
  color?: string
  /**
   * 轨道颜色
   * @default "#e5e5e5"
   */
  trackColor?: string
  /**
   * 进度文字内容
   * @default 百分比
   */
  pivotText?: string
  /**
   * 进度文字背景色
   * @default 同进度条颜色
   */
  pivotColor?: string
  /**
   * 进度文字颜色
   * @default "white"
   */
  textColor?: string
  /**
   * 是否置灰
   * @default false
   */
  inactive?: boolean
  /**
   * 是否显示进度文字
   * @default true
   */
  showPivot?: boolean
}

declare interface _Progress extends BaseComponent<ProgressProps> {}

declare interface _ProgressRef {
  /**
   * 外层元素大小变化后，可以调用此方法来触发重绘
   */
  resize: () => any
}

export declare const Progress: _Progress

export declare const ProgressRef: _ProgressRef
