import { BaseComponent, TemplateEvents } from './_common'

declare interface DatetimePickerProps {
  /**
   * 时间类型
   * @default "datetime"
   */
  type?: 'datetime' | 'date' | 'time' | 'year-month' | 'month-day' | 'datehour'
  /**
   * 顶部栏标题
   * @default ""
   */
  title?: string
  /**
   * 确认按钮文字
   * @default "确认"
   */
  confirmButtonText?: string
  /**
   * 取消按钮文字
   * @default "取消"
   */
  cancelButtonText?: string
  /**
   * 是否显示顶部栏
   * @default true
   */
  showToolbar?: boolean
  /**
   * 是否显示加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 是否为只读状态，只读状态下无法切换选项
   * @default false
   */
  readonly?: boolean
  /**
   * 选项过滤函数
   */
  filter?: (type: DatetimePickerProps['type'], vals: any) => any
  /**
   * 选项格式化函数
   */
  formatter?: (type: DatetimePickerProps['type'], val: any) => any
  /**
   * 自定义列排序数组
   */
  columnsOrder?: ('year' | 'month' | 'day' | 'hour' | 'minute')[]
  /**
   * 选项高度，支持 `px` `vw` `vh` `rem` 单位，默认 `px`
   * @default 44
   */
  itemHeight?: string | number
  /**
   * 可见的选项个数
   * @default 6
   */
  visibleItemCount?: string | number
  /**
   * 快速滑动时惯性滚动的时长，单位`ms`
   * @default 1000
   */
  swipeDuration?: string | number
  /**
   * 可选的最小时间，精确到分钟
   * @description 当时间选择器类型为 date 或 datetime 时，支持
   * @default 十年前
   */
  minDate?: Date
  /**
   * 可选的最大时间，精确到分钟
   * @description 当时间选择器类型为 date 或 datetime 时，支持
   * @default 十年后
   */
  maxDate?: Date
  /**
   * 可选的最小小时
   * @description 当时间选择器类型为 time 时，支持
   * @default 0
   */
  minHour?: string | number
  /**
   * 可选的最大小时
   * @description 当时间选择器类型为 time 时，支持
   * @default 23
   */
  maxHour?: string | number
  /**
   * 可选的最小分钟
   * @description 当时间选择器类型为 time 时，支持
   * @default 0
   */
  minMinute?: string | number
  /**
   * 可选的最大分钟
   * @description 当时间选择器类型为 time 时，支持
   * @default 59
   */
  maxMinute?: string | number
  /**
   * Events
   */
  on?: DatetimePickerEvents
  /**
   * Slots
   */
  scopedSlots?: DatetimePickerSlots
}

declare interface DatetimePickerEvents {
  /**
   * 当值变化时触发的事件
   */
  ['change']?: (picker: any) => any
  /**
   * 点击完成按钮时触发的事件
   */
  ['confirm']?: (value: any) => any
  /**
   * 点击取消按钮时触发的事件
   */
  ['cancel']?: () => any
}

declare type DatetimePickerTemplateEvents = TemplateEvents<DatetimePickerEvents>

declare interface DatetimePickerSlots {
  /**
   * 自定义整个顶部栏的内容
   */
  ['default']?: () => any
  /**
   * 自定义标题内容
   */
  ['title']?: () => any
  /**
   * 自定义确认按钮内容
   */
  ['confirm']?: () => any
  /**
   * 自定义取消按钮内容
   */
  ['cancel']?: () => any
  /**
   * 自定义选项内容
   */
  ['option']?: (option: string | Record<string, any>) => any
  /**
   * 自定义选项上方内容
   */
  ['columns-top']?: () => any
  /**
   * 自定义选项下方内容
   */
  ['columns-bottom']?: () => any
}

declare interface _DatetimePicker
  extends BaseComponent<
    DatetimePickerProps & DatetimePickerTemplateEvents,
    DatetimePickerSlots
  > {}

declare interface _DatetimePickerRef {
  /**
   * 获取 Picker 实例，用于调用 Picker 的实例方法
   */
  getPicker: () => any
}

export declare const DatetimePicker: _DatetimePicker

export declare const DatetimePickerRef: _DatetimePickerRef
