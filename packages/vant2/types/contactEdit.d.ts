import { BaseComponent, TemplateEvents } from './_common'

declare interface Contact {
  /**
   * 联系人姓名
   */
  name?: string
  /**
   * 联系人手机号
   */
  tel?: string | number
}

declare interface ContactEditProps {
  /**
   * 联系人信息
   * @default {}
   */
  contactInfo?: Contact
  /**
   * 是否为编辑联系人
   * @default false
   */
  isEdit?: boolean
  /**
   * 是否显示保存按钮加载动画
   * @default false
   */
  isSaving?: boolean
  /**
   * 是否显示删除按钮加载动画
   * @default false
   */
  isDeleting?: boolean
  /**
   * 手机号格式校验函数
   */
  telValidator?: (tel: string) => boolean
  /**
   * 是否显示默认联系人栏
   * @default false
   */
  showSetDefault?: boolean
  /**
   * 默认联系人栏文案
   */
  setDefaultLabel?: string
  /**
   * Events
   */
  on?: ContactEvents
}

declare interface ContactEvents {
  /**
   * 点击保存按钮时触发
   */
  ['save']?: (content) => any
  /**
   * 点击删除按钮时触发
   */
  ['delete']?: (content) => any
}

declare type ContactTemplateEvents = TemplateEvents<ContactEvents>

declare interface _ContactEdit
  extends BaseComponent<ContactEditProps & ContactTemplateEvents> {}

export declare const ContactEdit: _ContactEdit
