import { BaseComponent, TemplateEvents } from './_common'

declare interface GridItemProps {
  /**
   * 文字
   */
  text?: string
  /**
   * 图标名称或图片链接
   */
  icon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 是否显示图标右上角小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Events
   */
  on?: GridItemEvents
  /**
   * Slots
   */
  scopedSlots?: GridItemSlots
}

declare interface GridItemEvents {
  /**
   * 点击格子时触发
   */
  ['click']?: (event: Event) => any
}

declare type GridItemTemplateEvents = TemplateEvents<GridItemEvents>

declare interface GridItemSlots {
  /**
   * 自定义宫格的所有内容
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['icon']?: () => any
  /**
   * 自定义文字
   */
  ['text']?: () => any
}

declare interface _GridItem
  extends BaseComponent<
    GridItemProps & GridItemTemplateEvents,
    GridItemSlots
  > {}

export declare const GridItem: _GridItem
