import { BaseComponent, TemplateEvents } from './_common'

declare interface TabsProps {
  /**
   * 绑定当前选中标签的标识符
   * @default 0
   */
  value?: string | number
  /**
   * 样式风格类型
   * @default "line"
   */
  type?: 'card' | 'line'
  /**
   * 标签主题色
   * @default "#ee0a24"
   */
  color?: string
  /**
   * 标签栏背景色
   * @default "white"
   */
  background?: string
  /**
   * 动画时间，单位秒
   * @default 0.3
   */
  duration?: string | number
  /**
   * 底部条宽度，默认单位 `px`
   * @default "40px"
   */
  lineWidth?: string | number
  /**
   * 底部条高度，默认单位 `px`
   * @default "3px"
   */
  lineHeight?: string | number
  /**
   * 是否开启切换标签内容时的转场动画
   * @default false
   */
  animated?: boolean
  /**
   * 是否显示标签栏外边框，仅在 `type="line"` 时有效
   * @default false
   */
  border?: boolean
  /**
   * 是否省略过长的标题文字
   * @default false
   */
  ellipsis?: boolean
  /**
   * 是否使用粘性定位布局
   * @default false
   */
  sticky?: boolean
  /**
   * 是否开启手势滑动切换
   * @default false
   */
  swipeable?: boolean
  /**
   * 是否开启延迟渲染（首次切换到标签时才触发内容渲染）
   * @default true
   */
  lazyRender?: boolean
  /**
   * 是否开启滚动导航
   * @default false
   */
  scrollspy?: boolean
  /**
   * 粘性定位布局下与顶部的最小距离，支持 `px` `vw` `vh` `rem` 单位，默认 `px`
   * @default 0
   */
  offsetTop?: string | number
  /**
   * 滚动阈值，标签数量超过阈值且总宽度超过标签栏宽度时开始横向滚动
   * @default 5
   */
  swipeThreshold?: string | number
  /**
   * 标题选中态颜色
   */
  titleActiveColor?: string | number
  /**
   * 标题默认态颜色
   */
  titleInactiveColor?: string | number
  /**
   * 切换标签前的回调函数，返回 `false` 可阻止切换，支持返回 Promise
   */
  beforeChange?: (name) => boolean | Promise<boolean>
  /**
   * Events
   */
  on?: TabsEvents
  /**
   * Slots
   */
  scopedSlots?: TabsSlots
}

declare interface TabsEvents {
  /**
   * 点击标签时触发
   */
  ['click']?: (name, title) => any
  /**
   * 当前激活的标签改变时触发
   */
  ['change']?: (name, title) => any
  /**
   * 点击被禁用的标签时触发
   */
  ['disabled']?: (name, title) => any
  /**
   * 标签内容首次渲染时触发（仅在开启延迟渲染后触发）
   */
  ['rendered']?: (name, title) => any
  /**
   * 滚动时触发，仅在 sticky 模式下生效
   */
  ['scroll']?: (params: { scrollTop; isFixed }) => any
}

declare type TabsTemplateEvents = TemplateEvents<TabsEvents>

declare interface TabsSlots {
  /**
   * 标题左侧内容
   */
  ['nav-left']?: () => any
  /**
   * 标题右侧内容
   */
  ['nav-right']?: () => any
}

declare interface _Tabs
  extends BaseComponent<TabsProps & TabsTemplateEvents, TabsSlots> {}

declare interface _TabsRef {
  /**
   * 外层元素大小或组件显示状态变化时，可以调用此方法来触发重绘
   */
  resize: () => any
  /**
   * 滚动到指定的标签页，在滚动导航模式下可用
   */
  scrollTo: (name) => any
}

export declare const Tabs: _Tabs

export declare const TabsRef: _TabsRef
