import { BaseComponent } from './_common'

declare interface StepProps {
  /**
   * Slots
   */
  scopedSlots?: StepSlots
}

declare interface StepSlots {
  /**
   * 自定义激活状态图标
   */
  ['active-icon']?: () => any
  /**
   * 自定义未激活状态图标
   */
  ['inactive-icon']?: () => any
  /**
   * 自定义已完成步骤对应的底部图标，优先级高于 `inactive-icon`
   */
  ['finish-icon']?: () => any
}

declare interface _Step extends BaseComponent<StepProps, StepSlots> {}

export declare const Step: _Step
