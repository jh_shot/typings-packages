import { BaseComponent, TemplateEvents } from './_common'

declare interface SidebarProps {
  /**
   * 当前导航项的索引
   * @default 0
   */
  value?: string | number
  /**
   * Events
   */
  on?: SidebarEvents
}

declare interface SidebarEvents {
  /**
   * 切换导航项时触发
   */
  ['change']?: (index: number) => any
}

declare type SidebarTemplateEvents = TemplateEvents<SidebarEvents>

declare interface _Sidebar
  extends BaseComponent<SidebarProps & SidebarTemplateEvents> {}

export declare const Sidebar: _Sidebar
