import { BaseComponent, TemplateEvents } from './_common'

declare interface CollapseProps {
  /**
   * 当前展开面板的 name
   * @description 手风琴模式：number | string
   * @description 非手风琴模式：(number | string)[]
   */
  value?: string | number | (string | number)[]
  /**
   * 是否开启手风琴模式
   * @default false
   */
  accordion?: boolean
  /**
   * 是否显示外边框
   * @default true
   */
  border?: boolean
  /**
   * Events
   */
  on?: CollapseEvents
}

declare interface CollapseEvents {
  /**
   * 切换面板时触发
   */
  ['change']?: (activeNames) => any
}

declare type CollapseTemplateEvents = TemplateEvents<CollapseEvents>

declare interface _Collapse
  extends BaseComponent<CollapseProps & CollapseTemplateEvents> {}

export declare const Collapse: _Collapse
