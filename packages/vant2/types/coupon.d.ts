export declare interface Coupon {
  /**
   * 优惠券 id
   */
  id?: string
  /**
   * 优惠券名称
   */
  name?: string
  /**
   * 满减条件
   */
  condition?: string
  /**
   * 卡有效开始时间 (时间戳, 单位毫秒)
   */
  startAt?: number
  /**
   * 卡失效日期 (时间戳, 单位毫秒)
   */
  endAt?: number
  /**
   * 描述信息，优惠券可用时展示
   */
  description?: string
  /**
   * 不可用原因，优惠券不可用时展示
   */
  reason?: string
  /**
   * 折扣券优惠金额，单位分
   */
  value?: number
  /**
   * 折扣券优惠金额文案
   */
  valueDesc?: string
  /**
   * 单位文案
   */
  unitDesc?: string
  /**
   * 自定义有效时间文案
   */
  customValidPeriod?: string
}
