import { BaseComponent, TemplateEvents } from './_common'
import { Coupon } from './coupon'

declare interface CouponListProps {
  /**
   * 当前输入的兑换码
   */
  value?: string
  /**
   * 当前选中优惠券的索引
   * @default -1
   */
  chosenCoupon?: number
  /**
   * 可用优惠券列表
   * @default []
   */
  coupons?: Coupon[]
  /**
   * 不可用优惠券列表
   * @default []
   */
  disabledCoupons?: Coupon[]
  /**
   * 可用优惠券列表标题
   * @default "可使用优惠券"
   */
  enabledTitle?: string
  /**
   * 不可用优惠券列表标题
   * @default "不可使用优惠券"
   */
  disabledTitle?: string
  /**
   * 兑换按钮文字
   * @default "兑换"
   */
  exchangeButtonText?: string
  /**
   * 是否显示兑换按钮加载动画
   * @default false
   */
  exchangeButtonLoading?: boolean
  /**
   * 是否禁用兑换按钮
   * @default false
   */
  exchangeButtonDisabled?: boolean
  /**
   * 兑换码最小长度
   * @default 1
   */
  exchangeMinLength?: number
  /**
   * 滚动至特定优惠券位置
   */
  displayedCouponIndex?: number
  /**
   * 是否显示列表底部按钮
   * @default true
   */
  showCloseButton?: boolean
  /**
   * 列表底部按钮文字
   * @default "不使用优惠"
   */
  closeButtonText?: string
  /**
   * 输入框文字提示
   * @default "请输入优惠码"
   */
  inputPlaceholder?: string
  /**
   * 是否展示兑换栏
   * @default true
   */
  showExchangeBar?: boolean
  /**
   * 货币符号
   * @default "¥"
   */
  currency?: string
  /**
   * 列表为空时的占位图
   * @default "https://img01.yzcdn.cn/vant/coupon-empty.png"
   */
  emptyImage?: string
  /**
   * 是否展示可用 / 不可用数量
   * @default true
   */
  showCount?: boolean
  /**
   * Events
   */
  on?: CouponListEvents
  /**
   * Slots
   */
  scopedSlots?: CouponListSlots
}

declare interface CouponListEvents {
  /**
   * 优惠券切换回调
   */
  ['change']?: (index: number) => any
  /**
   * 兑换优惠券回调
   */
  ['exchange']?: (code) => any
}

declare type CouponListTemplateEvents = TemplateEvents<CouponListEvents>

declare interface CouponListSlots {
  /**
   * 优惠券列表底部
   */
  ['list-footer']?: () => any
  /**
   * 不可用优惠券列表底部
   */
  ['disabled-list-footer']?: () => any
}

declare interface _CouponList
  extends BaseComponent<
    CouponListProps & CouponListTemplateEvents,
    CouponListSlots
  > {}

export declare const CouponList: _CouponList
