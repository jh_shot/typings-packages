import { BaseComponent, TemplateEvents } from './_common'

declare interface PaginationProps {
  /**
   * 当前页码
   */
  value?: number
  /**
   * 显示模式
   * @default "multi"
   */
  mode?: 'simple' | 'multi'
  /**
   * 上一页按钮文字
   * @default "上一页"
   */
  preText?: string
  /**
   * 下一页按钮文字
   * @default "下一页"
   */
  nextText?: string
  /**
   * 总页数
   * @default 根据页数计算
   */
  pageCount?: string | number
  /**
   * 总记录数
   * @default 0
   */
  totalItems?: string | number
  /**
   * 每页记录数
   * @default 10
   */
  itemsPrePage?: string | number
  /**
   * 显示的页码个数
   * @default 5
   */
  showPageSize?: string | number
  /**
   * 是否显示省略号
   * @default false
   */
  forceEllipses?: boolean
  /**
   * Events
   */
  on?: PaginationEvents
  /**
   * Slots
   */
  scopedSlots?: PaginationSlots
}

declare interface PaginationEvents {
  /**
   * 页码改变时触发
   */
  ['change']?: () => any
}

declare type PaginationTemplateEvents = TemplateEvents<PaginationEvents>

declare interface PaginationSlots {
  /**
   * 自定义页码
   */
  ['page']?: (params: { number: number; text: string; active: boolean }) => any
  /**
   * 自定义上一页按钮文字
   */
  ['pre-text']?: () => any
  /**
   * 自定义下一页按钮文字
   */
  ['next-text']?: () => any
}

declare interface _Pagination
  extends BaseComponent<
    PaginationProps & PaginationTemplateEvents,
    PaginationSlots
  > {}

export declare const Pagination: _Pagination
