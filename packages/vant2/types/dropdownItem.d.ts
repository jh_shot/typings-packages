import { BaseComponent, TemplateEvents } from './_common'

declare interface Option {
  /**
   * 文字
   */
  text: string
  /**
   * 标识符
   */
  value: string | number
  /**
   * 左侧图标名称或图片链接
   */
  icon?: string
}

declare interface DropdownItemProps {
  /**
   * 当前选中项对应的 value，可以通过`v-model`双向绑定
   */
  value?: string | number
  /**
   * 菜单项标题
   * @default 当前选中项文字
   */
  title?: string
  /**
   * 选项数组
   * @default []
   */
  options?: Option[]
  /**
   * 是否禁用菜单
   * @default false
   */
  disabled?: boolean
  /**
   * 是否在首次展开时才渲染菜单内容
   * @default true
   */
  lazyRender?: boolean
  /**
   * 标题额外类名
   */
  titleClass?: string
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * Events
   */
  on?: DropdownItemEvents
  /**
   * Slots
   */
  scopedSlots?: DropdownItemSlots
}

declare interface DropdownItemEvents {
  /**
   * 点击选项导致 value 变化时触发
   */
  ['change']?: (value) => any
  /**
   * 打开菜单栏时触发
   */
  ['open']?: () => any
  /**
   * 关闭菜单栏时触发
   */
  ['close']?: () => any
  /**
   * 打开菜单栏且动画结束后触发
   */
  ['opened']?: () => any
  /**
   * 关闭菜单栏且动画结束后触发
   */
  ['closed']?: () => any
}

declare type DropdownItemTemplateEvents = TemplateEvents<DropdownItemEvents>

declare interface DropdownItemSlots {
  /**
   * 菜单内容
   */
  ['default']?: () => any
  /**
   * 自定义菜单项标题
   */
  ['title']?: () => any
}

declare interface _DropdownItem
  extends BaseComponent<
    DropdownItemProps & DropdownItemTemplateEvents,
    DropdownItemSlots
  > {}

declare interface _DropdownItemRef {
  /**
   * 切换菜单展示状态，传 `true` 为显示，`false` 为隐藏，不传参为取反
   */
  toggle: (show?: boolean) => any
}

export declare const DropdownItem: _DropdownItem

export declare const DropdownItemRef: _DropdownItemRef
