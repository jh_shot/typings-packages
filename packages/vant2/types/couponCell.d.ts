import { BaseComponent } from './_common'
import { Coupon } from './coupon'

declare interface CouponCellProps {
  /**
   * 单元格标题
   * @default "优惠券"
   */
  title?: string
  /**
   * 当前选中优惠券的索引
   * @default -1
   */
  chosenCoupon?: string | number
  /**
   * 可用优惠券列表
   */
  coupons?: Coupon[]
  /**
   * 能否切换优惠券
   * @default true
   */
  editable?: boolean
  /**
   * 是否显示内边框
   * @default true
   */
  border?: boolean
  /**
   * 货币符号
   * @default "¥"
   */
  currency?: string
}

declare interface _CouponCell extends BaseComponent<CouponCellProps> {}

export declare const CouponCell: _CouponCell
