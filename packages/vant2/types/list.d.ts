import { BaseComponent, TemplateEvents } from './_common'

declare interface ListProps {
  /**
   * 是否处于加载状态，加载过程中不触发`load`事件
   * @default false
   */
  value?: boolean
  /**
   * 是否已加载完成，加载完成后不再触发`load`事件
   * @default false
   */
  finished?: boolean
  /**
   * 是否加载失败，加载失败后点击错误提示可以重新触发`load`事件，必须使用`sync`修饰符
   * @default false
   */
  error?: boolean
  /**
   * 滚动条与底部距离小于 offset 时触发`load`事件
   * @default 300
   */
  offset?: string | number
  /**
   * 加载过程中的提示文案
   * @default "加载中..."
   */
  loadingText?: string
  /**
   * 加载完成后的提示文案
   */
  finishedText?: string
  /**
   * 加载失败后的提示文案
   */
  errorText?: string
  /**
   * 是否在初始化时立即执行滚动位置检查
   * @default true
   */
  immediateCheck?: boolean
  /**
   * 滚动触发加载的方向
   * @default "down"
   */
  direction?: 'up' | 'down'
  /**
   * Events
   */
  on?: ListEvents
  /**
   * Slots
   */
  scopedSlots?: ListSlots
}

declare interface ListEvents {
  /**
   * 滚动条与底部距离小于 offset 时触发
   */
  ['load']?: () => any
}

declare type ListTemplateEvents = TemplateEvents<ListEvents>

declare interface ListSlots {
  /**
   * 列表内容
   */
  ['default']?: () => any
  /**
   * 自定义底部加载中提示
   */
  ['loading']?: () => any
  /**
   * 自定义加载完成后的提示文案
   */
  ['finished']?: () => any
  /**
   * 自定义加载失败后的提示文案
   */
  ['error']?: () => any
}

declare interface _List
  extends BaseComponent<ListProps & ListTemplateEvents, ListSlots> {}

declare interface _ListRef {
  /**
   * 检查当前的滚动位置，若已滚动至底部，则会触发 load 事件
   */
  check: () => any
}

export declare const List: _List

export declare const ListRef: _ListRef
