import { BaseComponent, TemplateEvents } from './_common'

declare interface SwitchProps {
  /**
   * 开关选中状态
   * @default false
   */
  value?: any
  /**
   * 是否为加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 是否为禁用状态
   * @default false
   */
  disabled?: boolean
  /**
   * 开关尺寸，默认单位为`px`
   * @default "30px"
   */
  size?: string | number
  /**
   * 打开时的背景色
   * @default "#1989fa"
   */
  activeColor?: string
  /**
   * 关闭时的背景色
   * @default "white"
   */
  inactiveColor?: string
  /**
   * 打开时对应的值
   * @default true
   */
  activeValue?: any
  /**
   * 关闭时对应的值
   * @default false
   */
  inactiveValue?: any
  /**
   * Events
   */
  on?: SwitchEvents
}

declare interface SwitchEvents {
  /**
   * 开关状态切换时触发
   */
  ['change']?: (value: any) => any
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type SwitchTemplateEvents = TemplateEvents<SwitchEvents>

declare interface _Switch
  extends BaseComponent<SwitchProps & SwitchTemplateEvents> {}

export declare const Switch: _Switch
