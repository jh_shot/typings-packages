import { BaseComponent, TemplateEvents } from './_common'

export declare type ImageFit =
  | 'contain'
  | 'cover'
  | 'fill'
  | 'none'
  | 'scale-down'

declare interface ImageProps {
  /**
   * 图片链接
   */
  src?: string
  /**
   * 图片填充模式
   * @default "fill"
   */
  fit?: ImageFit
  /**
   * 替代文本
   */
  alt?: string
  /**
   * 宽度，默认单位为`px`
   */
  width?: string | number
  /**
   * 高度，默认单位为`px`
   */
  height?: string | number
  /**
   * 圆角大小，默认单位为`px`
   * @default 0
   */
  radius?: string | number
  /**
   * 是否显示为圆形
   * @default false
   */
  round?: boolean
  /**
   * 是否开启图片懒加载，须配合 Lazyload 组件使用
   * @default false
   */
  lazyLoad?: boolean
  /**
   * 是否展示图片加载失败提示
   * @default true
   */
  showError?: boolean
  /**
   * 是否展示图片加载中提示
   * @default true
   */
  showLoading?: boolean
  /**
   * 失败时提示的图标名称或图片链接
   * @default "photo-fail"
   */
  errorIcon?: string
  /**
   * 加载时提示的图标名称或图片链接
   * @default "photo"
   */
  loadingIcon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * Events
   */
  on?: ImageEvents
  /**
   * Slots
   */
  scopedSlots?: ImageSlots
}

declare interface ImageEvents {
  /**
   * 点击图片时触发
   */
  ['click']?: (event: Event) => any
  /**
   * 图片加载完毕时触发
   */
  ['load']?: () => any
  /**
   * 图片加载失败时触发
   */
  ['error']?: () => any
}

declare type ImageTemplateEvents = TemplateEvents<ImageEvents>

declare interface ImageSlots {
  /**
   * 自定义图片下方的内容
   */
  ['default']?: () => any
  /**
   * 自定义加载中的提示内容
   */
  ['loading']?: () => any
  /**
   * 自定义加载失败时的提示内容
   */
  ['error']?: () => any
}

declare interface _Image
  extends BaseComponent<ImageProps & ImageTemplateEvents, ImageSlots> {}

export declare const Image: _Image
