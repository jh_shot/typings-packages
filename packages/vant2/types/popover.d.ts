import { BaseComponent, TemplateEvents } from './_common'

declare interface Action {
  /**
   * 选项文字
   */
  text: string
  /**
   * 文字左侧的图标，支持传入图标名称或图片链接
   */
  icon?: string
  /**
   * 是否为禁用状态
   */
  disabled?: boolean
  /**
   * 为对应选项添加额外的类名
   */
  className?: any
}

declare interface PopoverProps {
  /**
   * 是否展示气泡弹出层
   * @default false
   */
  value?: boolean
  /**
   * 选项列表
   * @default []
   */
  actions?: Action[]
  /**
   * 弹出位置
   * @default "bottom"
   */
  placement?:
    | 'top'
    | 'top-start'
    | 'top-end'
    | 'left'
    | 'left-start'
    | 'left-end'
    | 'right'
    | 'right-start'
    | 'right-end'
    | 'bottom'
    | 'bottom-start'
    | 'bottom-end'
  /**
   * 主题风格
   * @default "light"
   */
  theme?: 'light' | 'dark'
  /**
   * 触发方式
   */
  trigger?: 'click'
  /**
   * 出现位置的偏移量
   * @default [0, 8]
   */
  offset?: number[]
  /**
   * 是否显示遮罩层
   * @default true
   */
  overlay?: boolean
  /**
   * 是否在点击选项后关闭
   * @default true
   */
  closeOnClickAction?: boolean
  /**
   * 是否在点击外部元素后关闭菜单
   * @default true
   */
  closeOnClickOutside?: boolean
  /**
   * 指定挂载的节点
   * @default "body"
   */
  getContainer?: string | (() => any)
  /**
   * Events
   */
  on?: PopoverEvents
  /**
   * Slots
   */
  scopedSlots?: PopoverSlots
}

declare interface PopoverEvents {
  /**
   * 点击选项时触发
   */
  ['select']?: (action: Action, index: number) => any
  /**
   * 打开菜单时触发
   */
  ['open']?: () => any
  /**
   * 关闭菜单时触发
   */
  ['close']?: () => any
  /**
   * 打开菜单且动画结束后触发
   */
  ['opened']?: () => any
  /**
   * 关闭菜单且动画结束后触发
   */
  ['closed']?: () => any
}

declare type PopoverTemplateEvents = TemplateEvents<PopoverEvents>

declare interface PopoverSlots {
  /**
   * 自定义菜单内容
   */
  ['default']?: () => any
  /**
   * 触发 Popover 显示的元素内容
   */
  ['reference']?: () => any
}

declare interface _Popover
  extends BaseComponent<PopoverProps & PopoverTemplateEvents, PopoverSlots> {}

export declare const Popover: _Popover
