import { BaseComponent, TemplateEvents } from './_common'

declare interface RadioGroupProps {
  /**
   * 当前选中项的标识符
   */
  value?: any
  /**
   * 是否禁用所有单选框
   * @default false
   */
  disabled?: boolean
  /**
   * 排列方向
   * @default "vertical"
   */
  direction?: 'vertical' | 'horizontal'
  /**
   * 所有单选框的图标大小，默认单位为`px`
   * @default "20px"
   */
  iconSize?: string | number
  /**
   * 所有单选框的选中状态颜色
   * @default "#1989fa"
   */
  checkedColor?: string
  /**
   * Events
   */
  on?: RadioGroupEvents
}

declare interface RadioGroupEvents {
  /**
   * 当绑定值变化时触发的事件
   */
  ['change']?: (name: string) => any
}

declare type RadioGroupTemplateEvents = TemplateEvents<RadioGroupEvents>

declare interface _RadioGroup
  extends BaseComponent<RadioGroupProps & RadioGroupTemplateEvents> {}

export declare const RadioGroup: _RadioGroup
