import { BaseComponent, TemplateEvents } from './_common'

declare interface IndexBarProps {
  /**
   * 索引字符列表
   * @default A-Z
   */
  indexList?: string[] | number[]
  /**
   * z-index 层级
   * @default 1
   */
  zIndex?: string | number
  /**
   * 是否开启锚点自动吸顶
   * @default true
   */
  sticky?: boolean
  /**
   * 锚点自动吸顶时与顶部的距离
   * @default 0
   */
  stickyOffsetTop?: number
  /**
   * 索引字符高亮颜色
   * @default "#ee0a24"
   */
  highlightColor?: string
  /**
   * Events
   */
  on?: IndexBarEvents
}

declare interface IndexBarEvents {
  /**
   * 点击索引栏的字符时触发
   */
  ['select']?: (index: string | number) => any
  /**
   * 当前高亮的索引字符变化时触发
   */
  ['change']?: (index: string | number) => any
}

declare type IndexBarTemplateEvents = TemplateEvents<IndexBarEvents>

declare interface _IndexBar
  extends BaseComponent<IndexBarProps & IndexBarTemplateEvents> {}

declare interface _IndexBarRef {
  /**
   * 滚动到指定锚点
   */
  scrollTo: (index: number | string) => any
}

export declare const IndexBar: _IndexBar

export declare const IndexBarRef: _IndexBarRef
