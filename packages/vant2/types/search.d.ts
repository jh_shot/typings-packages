import { BaseComponent } from './_common'

declare interface SearchProps {
  /**
   * 搜索框左侧文本
   */
  label?: string
  /**
   * 搜索框形状
   * @default "square"
   */
  shape?: 'round' | 'square'
  /**
   * 搜索框外部背景色
   * @default "#f2f2f2"
   */
  background?: string
  /**
   * 输入的最大字符数
   */
  maxlength?: string | number
  /**
   * 占位提示文字
   */
  placeholder?: string
  /**
   * 是否启用清除图标，点击清除图标后会清空输入框
   */
  clearable?: boolean
  /**
   * 显示清除图标的时机
   */
  clearTrigger?: 'always' | 'focus'
  /**
   * 是否自动聚焦，iOS 系统不支持该属性
   * @default false
   */
  autofocus?: boolean
  /**
   * 是否在搜索框右侧显示取消按钮
   */
  showAction?: boolean
  /**
   * 取消按钮文字
   * @default "取消"
   */
  actionText?: string
  /**
   * 是否禁用输入框
   * @default false
   */
  disabled?: boolean
  /**
   * 是否将输入框设为只读
   * @default false
   */
  readonly?: boolean
  /**
   * 是否将输入内容标红
   * @default false
   */
  error?: boolean
  /**
   * 输入框内容对齐方式
   * @default "left"
   */
  inputAlign?: 'left' | 'center' | 'right'
  /**
   * 输入框左侧图标名称或图片链接
   * @default "search"
   */
  leftIcon?: string
  /**
   * 输入框右侧图标名称或图片链接
   */
  rightIcon?: string
  on?: {
    /**
     * 确定搜索时触发
     */
    ['search']?: (value: string) => any
    /**
     * 输入框内容变化时触发
     */
    ['input']?: (value: string) => any
    /**
     * 输入框获得焦点时触发
     */
    ['focus']?: (event: Event) => any
    /**
     * 输入框失去焦点时触发
     */
    ['blur']?: (event: Event) => any
    /**
     * 点击清除按钮后触发
     */
    ['clear']?: (event: Event) => any
    /**
     * 点击取消按钮时触发
     */
    ['cancel']?: () => any
  }
  /**
   * Slots
   */
  scopedSlots?: SearchSlots
}

declare interface SearchSlots {
  /**
   * 自定义左侧内容（搜索框外）
   */
  ['left']?: () => any
  /**
   * 自定义右侧内容（搜索框外），设置`show-action`属性后展示
   */
  ['action']?: () => any
  /**
   * 自定义左侧文本（搜索框内）
   */
  ['label']?: () => any
  /**
   * 自定义左侧图标（搜索框内）
   */
  ['left-icon']?: () => any
  /**
   * 自定义右侧图标（搜索框内）
   */
  ['right-icon']?: () => any
}

declare interface _Search extends BaseComponent<SearchProps, SearchSlots> {}

export declare const Search: _Search
