import { BaseComponent, TemplateEvents } from './_common'

declare interface Column {
  /**
   * 列中对应的备选值
   */
  values: string[]
  /**
   * 初始选中项的索引，默认为 0
   */
  defaultIndex: number
  /**
   * 为对应列添加额外的类名
   */
  className: any
  /**
   * 级联选项
   */
  children: Column[]
}

declare interface PickerProps {
  /**
   * 对象数组，配置每一列显示的数据
   * @default []
   */
  columns?: Column[]
  /**
   * 顶部栏标题
   */
  title?: string
  /**
   * 确认按钮文字
   * @default "确认"
   */
  confirmButtonText?: string
  /**
   * 取消按钮文字
   * @default "取消"
   */
  cancelButtonText?: string
  /**
   * 选项对象中，选项文字对应的键名
   * @default "text"
   */
  valueKey?: string
  /**
   * 顶部栏位置
   * @default "top"
   */
  toolbarPosition?: 'top' | 'bottom'
  /**
   * 是否显示加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 是否为只读状态，只读状态下无法切换选项
   * @default false
   */
  readonly?: boolean
  /**
   * 是否显示顶部栏
   * @default false
   */
  showToolbar?: boolean
  /**
   * 是否允许选项内容中渲染 HTML
   * @default true
   */
  allowHtml?: boolean
  /**
   * 单列选择时，默认选中项的索引
   * @default 0
   */
  defaultIndex?: string | number
  /**
   * 选项高度，支持 `px` `vw` `vh` `rem` 单位，默认 `px`
   * @default 44
   */
  itemHeight?: string | number
  /**
   * 可见的选项个数
   * @default 6
   */
  visibleItemCount?: string | number
  /**
   * 快速滑动时惯性滚动的时长，单位 `ms`
   * @default 1000
   */
  swipeDuration?: string | number
  /**
   * Events
   */
  on?: PickerEvents
  /**
   * Slots
   */
  scopedSlots?: PickerSlots
}

declare interface PickerEvents {
  /**
   * 点击完成按钮时触发
   * @param args 单列：选中值，选中值对应的索引
   * @param args 多列：所有列选中值，所有列选中值对应的索引
   */
  ['confirm']?: (...args: any[]) => any
  /**
   * 点击取消按钮时触发
   * @param args 单列：选中值，选中值对应的索引
   * @param args 多列：所有列选中值，所有列选中值对应的索引
   */
  ['cancel']?: (...args: any[]) => any
  /**
   * 选项改变时触发
   * @param args 单列：Picker 实例，选中值，选中值对应的索引
   * @param args 多列：Picker 实例，所有列选中值，当前列对应的索引
   */
  ['change']?: (...args: any[]) => any
}

declare type PickerTemplateEvents = TemplateEvents<PickerEvents>

declare interface PickerSlots {
  /**
   * 自定义整个顶部栏的内容
   */
  ['default']?: () => any
  /**
   * 自定义标题内容
   */
  ['title']?: () => any
  /**
   * 自定义确认按钮内容
   */
  ['confirm']?: () => any
  /**
   * 自定义取消按钮内容
   */
  ['cancel']?: () => any
  /**
   * 自定义选项内容
   */
  ['option']?: (option: string | Record<string, any>) => any
  /**
   * 自定义选项上方内容
   */
  ['columns-top']?: () => any
  /**
   * 自定义选项下方内容
   */
  ['columns-bottom']?: () => any
}

declare interface _Picker
  extends BaseComponent<PickerProps & PickerTemplateEvents, PickerSlots> {}

declare interface _PickerRef {
  /**
   * 获取所有列选中的值
   */
  getValues: () => any
  /**
   * 设置所有列选中的值
   */
  setValues: (values) => any
  /**
   * 获取所有列选中值对应的索引
   */
  getIndexes: () => any
  /**
   * 设置所有列选中值对应的索引
   */
  setIndexes: (indexes) => any
  /**
   * 获取对应列选中的值
   */
  getColumnValue: (columnIndex) => any
  /**
   * 设置对应列选中的值
   */
  setColumnValue: (columnIndex, value) => any
  /**
   * 获取对应列选中项的索引
   */
  getColumnIndex: (columnIndex) => any
  /**
   * 设置对应列选中项的索引
   */
  setColumnIndex: (columnIndex, optionIndex) => any
  /**
   * 获取对应列中所有选项
   */
  getColumnValues: (columnIndex) => any
  /**
   * 设置对应列中所有选项
   */
  setColumnValues: (columnIndex, values) => any
  /**
   * 停止惯性滚动并触发 confirm 事件
   */
  confirm: () => any
}

export declare const Picker: _Picker

export declare const PickerRef: _PickerRef
