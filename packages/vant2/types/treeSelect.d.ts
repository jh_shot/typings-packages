import { BaseComponent, TemplateEvents } from './_common'

declare interface Item {
  /**
   * 导航名称
   */
  text: string
  /**
   *  导航名称右上角徽标
   */
  badge?: number
  /**
   * 是否在导航名称右上角显示小红点
   */
  dot?: boolean
  /**
   * 导航节点额外类名
   */
  className?: string
  /**
   * 该导航下所有的可选项
   */
  children?: Array<{
    /**
     * 名称
     */
    text: string
    /**
     * 作为匹配选中状态的标识符
     */
    id: number
    /**
     * 禁用选项
     */
    disabled?: boolean
  }>
}

declare interface TreeSelectProps {
  /**
   * 分类显示所需的数据
   */
  items?: Item[]
  /**
   * 高度，默认单位为`px`
   * @default 300
   */
  height?: string | number
  /**
   * 左侧选中项的索引
   * @default 0
   */
  mainActiveIndex?: string | number
  /**
   * 右侧选中项的 id，支持传入数组
   * @default 0
   */
  activeId?: string | number | (string | number)[]
  /**
   * 右侧项最大选中个数
   * @default "Infinity"
   */
  max?: string | number
  /**
   * 自定义右侧栏选中状态的图标
   * @default "success"
   */
  selectedIcon?: string
  /**
   * Events
   */
  on?: TreeSelectEvents
  /**
   * Slots
   */
  scopedSlots?: TreeSelectSlots
}

declare interface TreeSelectEvents {
  /**
   * 点击左侧导航时触发
   */
  ['click-nav']?: (index: number) => any
  /**
   * 点击右侧选择项时触发
   */
  ['click-item']?: (data) => any
}

declare type TreeSelectTemplateEvents = TemplateEvents<TreeSelectEvents>

declare interface TreeSelectSlots {
  /**
   * 自定义右侧区域内容
   */
  ['content']?: () => any
}

declare interface _TreeSelect
  extends BaseComponent<
    TreeSelectProps & TreeSelectTemplateEvents,
    TreeSelectSlots
  > {}

export declare const TreeSelect: _TreeSelect
