import { BaseComponent, TemplateEvents } from './_common'

declare interface RowProps {
  /**
   * 布局方式
   */
  type?: 'flex'
  /**
   * 列元素之间的间距（单位为 px）
   */
  gutter?: string | number
  /**
   * 自定义元素标签
   * @default "div"
   */
  tag?: string
  /**
   * Flex 主轴对齐方式
   */
  justify?: 'start' | 'end' | 'center' | 'space-around' | 'space-between'
  /**
   * Flex 交叉轴对齐方式
   */
  align?: 'top' | 'center' | 'bottom'
  /**
   * Events
   */
  on?: RowEvents
}

declare interface RowEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type RowTemplateEvents = TemplateEvents<RowEvents>

declare interface _Row extends BaseComponent<RowProps & RowTemplateEvents> {}

export declare const Row: _Row
