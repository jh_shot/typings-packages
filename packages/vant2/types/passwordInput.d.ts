import { BaseComponent, TemplateEvents } from './_common'

declare interface PasswordInputProps {
  /**
   * 密码值
   * @default ""
   */
  value?: string
  /**
   * 输入框下方文字提示
   */
  info?: string
  /**
   * 输入框下方错误提示
   */
  errorInfo?: string
  /**
   * 密码最大长度
   * @default 6
   */
  length?: string | number
  /**
   * 输入框格子之间的间距，如 `20px` `2em`，默认单位为`px`
   * @default 0
   */
  gutter?: string | number
  /**
   * 是否隐藏密码内容
   * @default true
   */
  mask?: boolean
  /**
   * 是否已聚焦，聚焦时会显示光标
   * @default false
   */
  focused?: boolean
  /**
   * Events
   */
  on?: PasswordInputEvents
}

declare interface PasswordInputEvents {
  /**
   * 输入框聚焦时触发
   */
  ['focus']?: () => any
}

declare type PasswordInputTemplateEvents = TemplateEvents<PasswordInputEvents>

declare interface _PasswordInput
  extends BaseComponent<PasswordInputProps & PasswordInputTemplateEvents> {}

export declare const PasswordInput: _PasswordInput
