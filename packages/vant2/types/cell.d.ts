import { BaseComponent, TemplateEvents } from './_common'

declare interface CellProps {
  /**
   * 左侧标题
   */
  title?: string | number
  /**
   * 右侧内容
   */
  value?: string | number
  /**
   * 标题下方的描述信息
   */
  label?: string
  /**
   * 单元格大小
   */
  size?: string
  /**
   * 左侧图标名称或图片链接
   */
  icon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "icon"
   */
  iconPrefix?: string
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否显示内边框
   * @default true
   */
  border?: boolean
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * 是否开启点击反馈
   * @default false
   */
  clickable?: boolean
  /**
   * 是否展示右侧箭头并开启点击反馈
   * @default false
   */
  isLink?: boolean
  /**
   * 是否显示表单必填星号
   * @default false
   */
  required?: boolean
  /**
   * 是否使内容垂直居中
   * @default false
   */
  center?: boolean
  /**
   * 箭头方向
   * @default "right"
   */
  arrowDirection?: 'left' | 'right' | 'up' | 'down'
  /**
   * 左侧标题额外样式
   */
  titleStyle?: unknown
  /**
   * 左侧标题额外类名
   */
  titleClass?: any
  /**
   * 右侧内容额外类名
   */
  valueClass?: any
  /**
   * 描述信息额外类名
   */
  labelClass?: any
  /**
   * Events
   */
  on?: CellEvents
  /**
   * Slots
   */
  scopedSlots?: CellSlots
}

declare interface CellEvents {
  /**
   * 点击单元格时触发
   */
  ['click']?: (event: Event) => any
}

declare type CellTemplateEvents = TemplateEvents<CellEvents>

declare interface CellSlots {
  /**
   * 自定义右侧 value 的内容
   */
  ['default']?: () => any
  /**
   * 自定义左侧 title 的内容
   */
  ['title']?: () => any
  /**
   * 自定义标题下方 label 的内容
   */
  ['label']?: () => any
  /**
   * 自定义左侧图标
   */
  ['icon']?: () => any
  /**
   * 自定义右侧按钮，默认为`arrow`
   */
  ['right-icon']?: () => any
  /**
   * 自定义单元格最右侧的额外内容
   */
  ['extra']?: () => any
}

declare interface _Cell
  extends BaseComponent<CellProps & CellTemplateEvents, CellSlots> {}

export declare const Cell: _Cell
