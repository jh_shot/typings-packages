import { BaseComponent } from './_common'

declare interface CircleProps {
  /**
   * 当前进度
   */
  value?: number
  /**
   * 目标进度
   * @default 100
   */
  rate?: string | number
  /**
   * 圆环直径，默认单位为 `px`
   * @default "100px"
   */
  size?: string | number
  /**
   * 进度条颜色，传入对象格式可以定义渐变色
   * @default "#1989fa"
   */
  color?: string | Record<string, any>
  /**
   * 轨道颜色
   * @default "white"
   */
  layerColor?: string
  /**
   * 填充颜色
   * @default "none"
   */
  fill?: string
  /**
   * 动画速度（单位为 rate/s）
   * @default 0
   */
  speed?: string | number
  /**
   * 文字
   */
  text?: string
  /**
   * 进度条宽度
   * @default 40
   */
  strokeWidth?: string | number
  /**
   * 进度条端点的形状
   * @default "round"
   */
  strokeLinecap?: 'square' | 'butt' | 'round'
  /**
   * 是否顺时针增加
   * @default true
   */
  clockwise?: boolean
  /**
   * Slots
   */
  scopedSlots?: CircleSlots
}

declare interface CircleSlots {
  /**
   * 自定义文字内容
   */
  ['default']?: () => any
}

declare interface _Circle extends BaseComponent<CircleProps, CircleSlots> {}

export declare const Circle: _Circle
