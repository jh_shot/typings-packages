import { BaseComponent, TemplateEvents } from './_common'

interface Action {
  /**
   * 标题
   */
  name?: string
  /**
   * 二级标题
   */
  subname?: string
  /**
   * 选项文字颜色
   */
  color?: string
  /**
   * 为对应列添加额外的 class
   */
  className?: string
  /**
   * 是否为加载状态
   */
  loading?: boolean
  /**
   * 是否为禁用状态
   */
  disabled?: boolean
}

declare interface ActionSheetProps {
  /**
   * 面板选项列表
   * @default []
   */
  actions?: Action[]
  /**
   * 顶部标题
   */
  title?: string
  /**
   * 取消按钮文字
   */
  cancelText?: string
  /**
   * 选项上方的描述信息
   */
  description?: string
  /**
   * 是否显示关闭图标
   * @default true
   */
  closeable?: boolean
  /**
   * 关闭图标名称或图片链接
   * @default "cross"
   */
  closeIcon?: string
  /**
   * 动画时长，单位秒
   * @default 0.3
   */
  duration?: string | number
  /**
   * 是否显示圆角
   * @default true
   */
  round?: boolean
  /**
   * 是否显示遮罩层
   * @default true
   */
  overlay?: boolean
  /**
   * 是否锁定背景滚动
   * @default true
   */
  lockScroll?: boolean
  /**
   * 是否在显示弹层时才渲染节点
   * @default true
   */
  lazyRender?: boolean
  /**
   * 是否在页面回退时自动关闭
   * @default false
   */
  closeOnPopstate?: boolean
  /**
   * 是否在点击选项后关闭
   * @default false
   */
  closeOnClickAction?: boolean
  /**
   * 是否在点击遮罩层后关闭
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * Events
   */
  on?: ActionSheetEvents
  /**
   * Slots
   */
  scopedSlots?: ActionSheetSlots
}

declare interface ActionSheetEvents {
  /**
   * 点击选项时触发，禁用或加载状态下不会触发
   */
  ['select']?: (action: Action, index: number) => any
  /**
   * 点击取消按钮时触发
   */
  ['cancel']?: () => any
  /**
   * 打开面板时触发
   */
  ['open']?: () => any
  /**
   * 关闭面板时触发
   */
  ['close']?: () => any
  /**
   * 打开面板且动画结束后触发
   */
  ['opened']?: () => any
  /**
   * 关闭面板且动画结束后触发
   */
  ['closed']?: () => any
  /**
   * 点击遮罩层时触发
   */
  ['click-overlay']?: () => any
}

declare type ActionSheetTemplateEvents = TemplateEvents<ActionSheetEvents>

declare interface ActionSheetSlots {
  /**
   * 自定义面板的展示内容
   */
  ['default']?: () => any
  /**
   * 自定义描述文案
   */
  ['description']?: () => any
}

declare interface _ActionSheet
  extends BaseComponent<
    ActionSheetProps & ActionSheetTemplateEvents,
    ActionSheetSlots
  > {}

export declare const ActionSheet: _ActionSheet
