import { BaseComponent, TemplateEvents } from './_common'
import { ImageFit } from './image'

declare interface UploaderProps {
  /**
   * 已上传的文件列表
   */
  fileList?: any[]
  /**
   * 允许上传的文件类型
   * @default "image/*"
   */
  accept?: string
  /**
   * 标识符，可以在回调函数的第二项参数中获取
   */
  name?: string | number
  /**
   * 预览图和上传区域的尺寸，默认单位为 `px`
   * @default "80px"
   */
  previewSize?: string | number
  /**
   * 是否在上传完成后展示预览图
   * @default true
   */
  previewImage?: boolean
  /**
   * 是否在点击预览图后展示全屏图片预览
   * @default true
   */
  previewFullImage?: boolean
  /**
   * 全屏图片预览的配置项
   */
  previewOptions?: Record<string, any>
  /**
   * 是否开启图片多选，部分安卓机型不支持
   * @default false
   */
  multiple?: boolean
  /**
   * 是否禁用文件上传
   * @default false
   */
  disabled?: boolean
  /**
   * 是否将上传区域设置为只读状态
   * @default false
   */
  readonly?: boolean
  /**
   * 是否展示删除按钮
   * @default true
   */
  deletable?: boolean
  /**
   * 是否展示上传区域
   * @default true
   */
  showUpload?: boolean
  /**
   * 是否开启图片懒加载，须配合 Lazyload 组件使用
   * @default false
   */
  lazyLoad?: boolean
  /**
   * 图片选取模式
   */
  capture?: 'camera'
  /**
   * 文件读取完成后的回调函数
   */
  afterRead?: (file, detail) => any
  /**
   * 文件读取前的回调函数，返回 `false` 可终止文件读取，支持返回 `Promise`
   */
  beforeRead?: (file, detail) => any
  /**
   * 文件删除前的回调函数，返回 `false` 可终止文件读取，支持返回 `Promise`
   */
  beforeDelete?: (file, detail) => any
  /**
   * 文件大小限制，单位为 `byte`
   */
  maxSize?: string | number | ((file: File) => boolean)
  /**
   * 文件上传数量限制
   */
  maxCount?: string | number
  /**
   * 文件读取结果类型
   * @default "dataUrl"
   */
  resultType?: 'file' | 'text' | 'dataUrl'
  /**
   * 上传区域文字提示
   */
  uploadText?: string
  /**
   * 预览图裁剪模式
   * @default "cover"
   */
  imageFit?: ImageFit
  /**
   * 上传区域图标名称或图片链接
   * @default "photograph"
   */
  uploadIcon?: string
  /**
   * Events
   */
  on?: UploaderEvents
  /**
   * Slots
   */
  scopedSlots?: UploaderSlots
}

declare interface UploaderEvents {
  /**
   * 文件大小超过限制时触发
   */
  ['oversize']?: (file, detail) => any
  /**
   * 点击上传区域时触发
   */
  ['click-upload']?: (event: MouseEvent) => any
  /**
   * 点击预览图时触发
   */
  ['click-preview']?: (file, detail) => any
  /**
   * 关闭全屏图片预览时触发
   */
  ['close-preview']?: () => any
  /**
   * 删除文件预览时触发
   */
  ['delete']?: (file, detail) => any
}

declare type UploaderTemplateEvents = TemplateEvents<UploaderEvents>

declare interface UploaderSlots {
  /**
   * 自定义上传区域
   */
  ['default']?: () => any
  /**
   * 自定义覆盖在预览区域上方的内容
   */
  ['preview-cover']?: (item) => any
}

declare interface _Uploader
  extends BaseComponent<
    UploaderProps & UploaderTemplateEvents,
    UploaderSlots
  > {}

declare interface _UploaderRef {
  /**
   * 关闭全屏的图片预览
   */
  closeImagePreview: () => any
  /**
   * 主动调起文件选择，由于浏览器安全限制，只有在用户触发操作的上下文中调用才有效
   */
  chooseFile: () => any
}

export declare const Uploader: _Uploader

export declare const UploaderRef: _UploaderRef
