import { BaseComponent, TemplateEvents } from './_common'

declare interface OverlayProps {
  /**
   * 是否展示遮罩层
   * @default false
   */
  show?: boolean
  /**
   * z-index 层级
   * @default 1
   */
  zIndex?: string | number
  /**
   * 动画时长，单位秒
   * @default 0.3
   */
  duration?: string | number
  /**
   * 自定义类名
   */
  className?: string
  /**
   * 自定义样式
   */
  customStyle?: any
  /**
   * 是否锁定背景滚动，锁定时蒙层里的内容也将无法滚动
   * @default true
   */
  lockScroll?: boolean
  /**
   * Events
   */
  on?: OverlayEvents
  /**
   * Slots
   */
  scopedSlots?: OverlaySlots
}

declare interface OverlayEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type OverlayTemplateEvents = TemplateEvents<OverlayEvents>

declare interface OverlaySlots {
  /**
   * 默认插槽，用于在遮罩层上方嵌入内容
   */
  ['default']?: () => any
}

declare interface _Overlay
  extends BaseComponent<OverlayProps & OverlayTemplateEvents, OverlaySlots> {}

export declare const Overlay: _Overlay
