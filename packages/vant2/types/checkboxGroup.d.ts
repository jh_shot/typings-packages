import { BaseComponent, TemplateEvents } from './_common'

declare interface CheckboxGroupProps {
  /**
   * 所有选中项的标识符
   */
  value?: any[]
  /**
   * 是否禁用所有复选框
   * @default false
   */
  disabled?: boolean
  /**
   * 最大可选数，`0`为无限制
   * @default 0
   */
  max?: string | number
  /**
   * 排列方向
   * @default "vertical"
   */
  direction?: 'horizontal' | 'vertical'
  /**
   * 所有复选框的图标大小，默认单位为 `px`
   * @default "20px"
   */
  iconSize?: string | number
  /**
   * 所有复选框的选中状态颜色
   * @default "#1989fa"
   */
  checkedColor?: string
  /**
   * Events
   */
  on?: CheckboxGroupEvents
}

declare interface CheckboxGroupEvents {
  /**
   * 当绑定值变化时触发的事件
   */
  ['change']?: (names: any[]) => any
}

declare type CheckboxGroupTemplateEvents = TemplateEvents<CheckboxGroupEvents>

declare interface _CheckboxGroup
  extends BaseComponent<CheckboxGroupProps & CheckboxGroupTemplateEvents> {}

declare interface _CheckboxGroupRef {
  /**
   * 切换所有复选框，传 `true` 为选中，`false` 为取消选中，不传参为取反
   */
  toggleAll: (options?: boolean | Record<string, any>) => any
}

export declare const CheckboxGroup: _CheckboxGroup

export declare const CheckboxGroupRef: _CheckboxGroupRef
