import { BaseComponent, TemplateEvents } from './_common'

declare interface PullRefreshProps {
  /**
   * 是否处于加载中状态
   */
  value?: boolean
  /**
   * 下拉过程提示文案
   * @default "下拉即可刷新..."
   */
  pullingText?: string
  /**
   * 释放过程提示文案
   * @default "释放即可刷新..."
   */
  loosingText?: string
  /**
   * 加载过程提示文案
   * @default "加载中..."
   */
  loadingText?: string
  /**
   * 刷新成功提示文案
   */
  successText?: string
  /**
   * 刷新成功提示展示时长(ms)
   * @default 500
   */
  successDuration?: string | number
  /**
   * 动画时长
   * @default 300
   */
  animationDuration?: string | number
  /**
   * 顶部内容高度
   * @default 50
   */
  headHeight?: string | number
  /**
   * 触发下拉刷新的距离
   * @default 与 head-height 一致
   */
  pullDistance?: string | number
  /**
   * 是否禁用下拉刷新
   * @default false
   */
  disabled?: boolean
  /**
   * Events
   */
  on?: PullRefreshEvents
  /**
   * Slots
   */
  scopedSlots?: PullRefreshSlots
}

declare interface PullRefreshEvents {
  /**
   * 下拉刷新时触发
   */
  ['refresh']?: () => any
}

declare type PullRefreshTemplateEvents = TemplateEvents<PullRefreshEvents>

declare interface PullRefreshSlots {
  /**
   * 自定义内容
   */
  ['default']?: () => any
  /**
   * 非下拉状态时顶部内容
   */
  ['normal']?: () => any
  /**
   * 下拉过程中顶部内容
   */
  ['pulling']?: (params: { distance }) => any
  /**
   * 释放过程中顶部内容
   */
  ['loosing']?: (params: { distance }) => any
  /**
   * 加载过程中顶部内容
   */
  ['loading']?: (params: { distance }) => any
  /**
   * 刷新成功提示内容
   */
  ['success']?: () => any
}

declare interface _PullRefresh
  extends BaseComponent<
    PullRefreshProps & PullRefreshTemplateEvents,
    PullRefreshSlots
  > {}

export declare const PullRefresh: _PullRefresh
