import { BaseComponent, TemplateEvents } from './_common'

declare interface Day {
  /**
   * 日期对应的 Date 对象
   */
  date: Date
  /**
   * 日期类型
   */
  type: 'selected' | 'start' | 'middle' | 'end' | 'disabled'
  /**
   * 中间显示的文字
   */
  text: string
  /**
   * 上方的提示信息
   */
  topInfo: string
  /**
   * 下方的提示信息
   */
  bottomInfo: string
  /**
   * 额外类名
   */
  className: string
}

declare interface CalendarProps {
  /**
   * 选择类型
   * @default "single"
   */
  type?: 'single' | 'multiple' | 'range'
  /**
   * 日历标题
   * @default "日期选择"
   */
  title?: string
  /**
   * 主题色，对底部按钮和选中日期生效
   * @default "#ee0a24"
   */
  color?: string
  /**
   * 可选择的最小日期
   * @default 当前日期
   */
  minDate?: Date
  /**
   * 可选择的最大日期
   * @default 当前日期的六个月后
   */
  maxDate?: Date
  /**
   * 默认选中的日期
   * @default 今日
   */
  defaultDate?: Date | Date[] | null
  /**
   * 日期行高
   * @default 64
   */
  rowHeight?: string | number
  /**
   * 日期格式化函数
   */
  formatter?: (day: Day) => Day
  /**
   * 是否以弹层的形式展示日历
   * @default true
   */
  poppable?: boolean
  /**
   * 是否只渲染可视区域的内容
   * @default true
   */
  lazyRender?: boolean
  /**
   * 是否显示月份背景水印
   * @default true
   */
  showMark?: boolean
  /**
   * 是否展示日历标题
   * @default true
   */
  showTitle?: boolean
  /**
   * 是否展示日历副标题（年月）
   * @default true
   */
  showSubTitle?: boolean
  /**
   * 是否展示确认按钮
   * @default true
   */
  showConfirm?: boolean
  /**
   * 是否为只读状态，只读状态下不能选择日期
   * @default false
   */
  readonly?: boolean
  /**
   * 确认按钮的文字
   * @default "确定"
   */
  confirmText?: string
  /**
   * 确认按钮处于禁用状态时的文字
   * @default "确定"
   */
  confirmDisabledText?: string
  /**
   * 设置周起始日
   * @default 0
   */
  firstDayOfWeek?: 0 | 1 | 2 | 3 | 4 | 5 | 6
  /**
   * 弹出位置
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   * @default "bottom"
   */
  position?: 'top' | 'left' | 'bottom' | 'right'
  /**
   * 是否显示圆角弹窗
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   * @default true
   */
  round?: boolean
  /**
   * 是否在页面回退时自动关闭
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   * @default true
   */
  closeOnPopstate?: boolean
  /**
   * 是否在点击遮罩层后关闭
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 是否开启底部安全区适配
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * 指定挂载的节点
   * @description 当 `Calendar` 的 `poppable` 为 `true` 时，支持
   */
  getContainer?: string | (() => any)
  /**
   * 日期区间最多可选天数
   * @description 当 `Calendar` 的 `type` 为 `range` 和 `multiple` 时，支持
   * @default 无限制
   */
  maxRange?: string | number
  /**
   * 范围选择超过最多可选天数时的提示文案
   * @description 当 `Calendar` 的 `type` 为 `range` 和 `multiple` 时，支持
   * @default "选择天数不能超过 xx 天"
   */
  rangePrompt?: string
  /**
   * 是否允许日期范围的起止时间为同一天
   * @description 当 `Calendar` 的 `type` 为 `range` 时，支持
   * @default false
   */
  allowSameDay?: boolean
  /**
   * Events
   */
  on?: CalendarEvents
  /**
   * Slots
   */
  scopedSlots?: CalendarSlots
}

declare interface CalendarEvents {
  /**
   * 点击并选中任意日期时触发
   */
  ['select']?: (value: Date | Date[]) => any
  /**
   * 日期选择完成后触发，若`show-confirm`为`true`，则点击确认按钮后触发
   */
  ['confirm']?: (value: Date | Date[]) => any
  /**
   * 打开弹出层时触发
   */
  ['open']?: () => any
  /**
   * 关闭弹出层时触发
   */
  ['close']?: () => any
  /**
   * 打开弹出层且动画结束后触发
   */
  ['opened']?: () => any
  /**
   * 关闭弹出层且动画结束后触发
   */
  ['closed']?: () => any
  /**
   * 当日历组件的 `type` 为 `multiple` 时，取消选中日期时触发
   */
  ['unselect']?: (value: Date) => any
  /**
   * 当某个月份进入可视区域时触发
   */
  ['month-show']?: (params: { date: Date; title: string }) => any
}

declare type CalendarTemplateEvents = TemplateEvents<CalendarEvents>

declare interface CalendarSlots {
  /**
   * 自定义标题
   */
  ['title']?: () => any
  /**
   * 自定义底部区域内容
   */
  ['footer']?: () => any
  /**
   * 自定义日期上方的提示信息
   */
  ['top-info']?: (day: Day) => any
  /**
   * 自定义日期下方的提示信息
   */
  ['bottom-info']?: (day: Day) => any
}

declare interface _Calendar
  extends BaseComponent<
    CalendarProps & CalendarTemplateEvents,
    CalendarSlots
  > {}

declare interface _CalendarRef {
  /**
   * 将选中的日期重置到指定日期，未传参时会重置到默认日期
   */
  reset?: (date?: Date | Date[]) => any
  /**
   * 滚动到某个日期
   */
  scrollToDate?: (date: Date) => any
}

export declare const Calendar: _Calendar

export declare const CalendarRef: _CalendarRef
