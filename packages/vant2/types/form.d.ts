import { BaseComponent, TemplateEvents } from './_common'

export declare interface Rule {
  /**
   * 是否为必选字段，当值为空字符串、空数组、`undefined`、`null` 时，校验不通过
   */
  required?: boolean
  /**
   * 错误提示文案
   */
  message?: string | ((value, rule) => string)
  /**
   * 通过函数进行校验
   */
  validator?: (value, rule) => boolean | Promise<boolean>
  /**
   * 通过正则表达式进行校验
   */
  pattern?: RegExp
  /**
   * 本项规则的触发时机
   */
  trigger?: 'onChange' | 'onBlur'
  /**
   * 格式化函数，将表单项的值转换后进行校验
   */
  formatter?: (value, rule) => any
}

declare interface FormProps {
  /**
   * 表单项 label 宽度，默认单位为`px`
   * @default "6.2em"
   */
  labelWidth?: string | number
  /**
   * 表单项 label 对齐方式
   * @default "left"
   */
  labelAlign?: 'left' | 'center' | 'right'
  /**
   * 输入框对齐方式
   * @default "left"
   */
  inputAlign?: 'left' | 'center' | 'right'
  /**
   * 错误提示文案对齐方式
   * @default "left"
   */
  errorMessageAlign?: 'left' | 'center' | 'right'
  /**
   * 表单校验触发时机
   * @default "onBlur"
   */
  validateTrigger?: 'onChange' | 'onBlur' | 'onSubmit'
  /**
   * 是否在 label 后面添加冒号
   * @default false
   */
  colon?: boolean
  /**
   * 是否禁用表单中的所有输入框
   * @default false
   */
  disabled?: boolean
  /**
   * 是否将表单中的所有输入框设置为只读
   * @default false
   */
  readonly?: boolean
  /**
   * 否在某一项校验不通过时停止校验
   * @default false
   */
  validateFirst?: boolean
  /**
   * 是否在提交表单且校验不通过时滚动至错误的表单项
   * @default false
   */
  scrollToError?: boolean
  /**
   * 是否在校验不通过时标红输入框
   * @default true
   */
  showError?: boolean
  /**
   * 否在校验不通过时在输入框下方展示错误提示
   * @default true
   */
  showErrorMessage?: boolean
  /**
   * 是否在按下回车键时提交表单
   * @default true
   */
  submitOnEnter?: boolean
  /**
   * Events
   */
  on?: FormEvents
  /**
   * Slots
   */
  scopedSlots?: FormSlots
}

declare interface FormEvents {
  /**
   * 提交表单且验证通过后触发
   */
  ['submit']?: (values: Record<string, any>) => any
  /**
   * 提交表单且验证不通过后触发
   */
  ['failed']?: (errorInfo: {
    values: Record<string, any>
    errors: Record<string, any>[]
  }) => any
}

declare type FormTemplateEvents = TemplateEvents<FormEvents>

declare interface FormSlots {
  /**
   * 表单内容
   */
  ['default']?: () => any
}

declare interface _Form
  extends BaseComponent<FormProps & FormTemplateEvents, FormSlots> {}

declare interface _FormRef {
  /**
   * 提交表单，与点击提交按钮的效果等价
   */
  submit: () => any
  /**
   * validate	验证表单，支持传入 `name` 来验证单个或部分表单项
   */
  validate: (name?: string | string[]) => Promise<any>
  /**
   * 重置表单项的验证提示，支持传入 `name` 来重置单个或部分表单项
   */
  resetValidation: (name?: string | string[]) => any
  /**
   * 滚动到对应表单项的位置，默认滚动到顶部，第二个参数传 `false` 可滚动至底部
   */
  scrollToField: (name: string, alignToTop: boolean) => any
}

export declare const Form: _Form

export declare const FormRef: _FormRef
