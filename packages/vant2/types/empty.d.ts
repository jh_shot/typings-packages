import { BaseComponent } from './_common'

declare interface EmptyProps {
  /**
   * 图片类型
   * @description 可选值为 `error` `network` `search`，支持传入图片 URL
   * @default "default"
   */
  image?: string
  /**
   * 图片大小，默认单位为 `px`
   */
  imageSize?: string | number
  /**
   * 图片下方的描述文字
   */
  description?: string
  /**
   * Slots
   */
  scopedSlots?: EmptySlots
}

declare interface EmptySlots {
  /**
   * 自定义底部内容
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['image']?: () => any
  /**
   * 自定义描述文字
   */
  ['description']?: () => any
}

declare interface _Empty extends BaseComponent<EmptyProps, EmptySlots> {}

export declare const Empty: _Empty
