import { BaseComponent, TemplateEvents } from './_common'

declare interface RateProps {
  /**
   * 当前分值
   */
  value?: number
  /**
   * 图标总数
   * @default 5
   */
  count?: string | number
  /**
   * 图标大小，默认单位为`px`
   * @default "20px"
   */
  size?: string | number
  /**
   * 图标间距，默认单位为`px`
   * @default "4px"
   */
  gutter?: string | number
  /**
   * 选中时的颜色
   * @default "#ee0a24"
   */
  color?: string
  /**
   * 未选中时的颜色
   * @default "#c8c9cc"
   */
  voidColor?: string
  /**
   * 禁用时的颜色
   * @default "#c8c9cc"
   */
  disabledColor?: string
  /**
   * 选中时的图标名称或图片链接
   * @default "star"
   */
  icon?: string
  /**
   * 未选中时的图标名称或图片链接
   * @default "star-o"
   */
  voidIcon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 是否允许半选
   * @default false
   */
  allowHalf?: boolean
  /**
   * 是否为只读状态
   * @default false
   */
  readonly?: boolean
  /**
   * 是否禁用评分
   * @default false
   */
  disabled?: boolean
  /**
   * 是否可以通过滑动手势选择评分
   * @default true
   */
  touchable?: boolean
  /**
   * Events
   */
  on?: RateEvents
}

declare interface RateEvents {
  /**
   * 当前分值变化时触发的事件
   */
  ['change']?: (value: number) => any
}

declare type RateTemplateEvents = TemplateEvents<RateEvents>

declare interface _Rate extends BaseComponent<RateProps & RateTemplateEvents> {}

export declare const Rate: _Rate
