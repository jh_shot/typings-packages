import { BaseComponent } from './_common'

declare interface TabbarItemProps {
  /**
   * 标签名称，作为匹配的标识符
   * @default 当前标签的索引值
   */
  name?: string | number
  /**
   * 图标名称或图片链接
   */
  icon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 是否显示图标右上角小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Slots
   */
  scopedSlots?: TabbarItemSlots
}

declare interface TabbarItemSlots {
  /**
   * 自定义图标
   */
  ['icon']?: (active) => any
}

declare interface _TabbarItem
  extends BaseComponent<TabbarItemProps, TabbarItemSlots> {}

export declare const TabbarItem: _TabbarItem
