import { BaseComponent } from './_common'

declare interface GoodsActionIconProps {
  /**
   * 按钮文字
   */
  text?: string
  /**
   * 图标
   */
  icon?: string
  /**
   * 图标颜色
   * @default "#323233"
   */
  color?: string
  /**
   * 图标额外类名
   */
  iconClass?: any
  /**
   * 是否显示图标右上角小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Slots
   */
  scopedSlots?: GoodsActionIconSlots
}

declare interface GoodsActionIconSlots {
  /**
   * 文本内容
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['icon']?: () => any
}

declare interface _GoodsActionIcon
  extends BaseComponent<GoodsActionIconProps, GoodsActionIconSlots> {}

export declare const GoodsActionIcon: _GoodsActionIcon
