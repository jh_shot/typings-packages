import {
  ImagePreviewOptions,
  VanImagePreview,
  ImagePreview as _ImagePreview
} from 'vant/types/image-preview'

import { BaseComponent } from './_common'

declare type ImagePreviewComponentProps = ImagePreviewOptions

declare interface _ImagePreviewComponent
  extends BaseComponent<ImagePreviewComponentProps> {}

export declare const ImagePreview: Omit<_ImagePreview, 'Component'> & {
  (options: ImagePreviewOptions, startPosition?: number): VanImagePreview
  Component: _ImagePreviewComponent
}
