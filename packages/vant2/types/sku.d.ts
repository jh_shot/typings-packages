import { BaseComponent, TemplateEvents } from './_common'

declare interface SkuObj {
  tree?: Array<{
    /**
     * skuKeyName：规格类目名称
     */
    k?: string
    /**
     * skuKeyStr：sku 组合列表（下方 list）中当前类目对应的 key 值，value 值会是从属于当前类目的一个规格值 id
     */
    k_s?: string | number
    v?: Array<{
      /**
       *  skuValueId：规格值 id
       */
      id?: string | number
      /**
       * skuValueName：规格值名称
       */
      name?: string
      /**
       * 规格类目图片，只有第一个规格类目可以定义图片
       */
      imgUrl?: string
      /**
       * 用于预览显示的规格类目图片
       */
      previewImgUrl?: string
    }>
    /**
     * 是否展示大图模式
     */
    largeImageMode?: boolean
  }>
  list?: Array<{
    /**
     * skuId
     */
    id?: string | number
    /**
     * 规格类目 k_s 为 s1 的对应规格值 id
     */
    s1?: string | number
    /**
     * 规格类目 k_s 为 s2 的对应规格值 id
     */
    s2?: string | number
    /**
     * 价格（单位分）
     */
    price?: number
    /**
     * 当前 sku 组合对应的库存
     */
    stock_num?: number
  }>
  /**
   * 默认价格（单位元）
   */
  price?: string | number
  /**
   * 商品总库存
   */
  stock_num?: number
  /**
   * 无规格商品 skuId 取 collection_id，否则取所选 sku 组合对应的 id
   */
  collection_id?: string | number
  /**
   * 是否无规格商品
   */
  none_sku?: boolean
  messages?: Array<{
    /**
     * 留言类型为 time 时，是否含日期
     * - `0` 不包含
     * - `1` 包含
     */
    datetime?: '0' | '1'
    /**
     * 留言类型为 text 时，是否多行文本
     * - `0` 单行
     * - `1` 多行
     */
    multiple?: '0' | '1'
    /**
     * 留言名称
     */
    name?: string
    /**
     * 留言类型
     */
    type?: 'text' | 'id_no' | 'tel' | 'date' | 'time' | 'email'
    /**
     * 是否必填
     * - `1` 必填
     */
    required?: '1'
    /**
     * 占位文本
     */
    placeholder?: string
    /**
     * 附加描述文案
     */
    extraDesc?: string
  }>
  /**
   * 是否隐藏剩余库存
   */
  hide_stock?: boolean
}

declare interface PropertyObj {
  /**
   * 属性id
   */
  k_id?: string | number
  /**
   * 属性名
   */
  k?: string
  /**
   * 是否可多选
   */
  is_multiple?: boolean
  v?: Array<{
    /**
     * 属性值id
     */
    id?: string | number
    /**
     * 属性值名
     */
    name?: string
    /**
     * 属性值加价
     */
    price?: number
  }>
}

declare interface InitialSkuObj {
  /**
   * 键：skuKeyStr（sku 组合列表中当前类目对应的 key 值）
   */
  s1?: string | number
  /**
   * 值：skuValueId（规格值 id）
   */
  s2?: string | number
  /**
   * 初始选中数量
   */
  selectedNum?: number
  /**
   * 初始选中的商品属性
   * @description 键：属性id
   * @description 值：属性值id列表
   */
  selectedProp?: Record<any, any[]>
}

declare interface Goods {
  /**
   * 默认商品 sku 缩略图
   */
  picture?: string
}

declare interface CustomStepperConfig {
  /**
   * 自定义限购文案
   */
  quotaText?: string
  /**
   * 自定义步进器超过限制时的回调
   */
  handleOverLimit?: (data) => any
  /**
   * 步进器变化的回调
   */
  handleStepperChange?: (currentValue) => any
  /**
   * 库存
   */
  stockNum?: number
  /**
   * 格式化库存
   */
  stockFormatter?: (stockNum: number) => any
}

declare interface MessageConfig {
  /**
   * 图片上传回调，需要返回一个promise，promise正确执行的结果需要是一个图片url
   */
  uploadImg?: () => Promise<string>
  /**
   * 自定义图片上传逻辑，使用此选项时，会禁用原生图片选择
   */
  customUpload?: () => Promise<string>
  /**
   * 最大上传体积 (MB)
   */
  uploadMaxSize?: number
  /**
   * placeholder 配置
   */
  placeholderMap?: Record<string, any>
  /**
   * 初始留言信息
   * @description 键：留言 name
   * @description 值：留言内容
   */
  initialMessages?: Record<string, any>
}

declare interface SkuData {
  /**
   * 商品 id
   */
  goodsId?: string | number
  /**
   * 留言信息
   */
  messages?: Record<string, any>
  /**
   * 另一种格式的留言，key 不同
   */
  cartMessages?: Record<string, any>
  /**
   * 选择的商品数量
   */
  selectedNum?: number
  /**
   * 选择的 sku 组合
   */
  selectedSkuComb?: {
    id?: string | number
    price?: number
    s1?: string | number
    s2?: string | number
    s3?: string | number
    stock_num?: number
    properties?: PropertyObj[]
    property_price?: number
  }
}

declare interface SkuProps {
  /**
   * 是否显示商品规格弹窗
   * @default false
   */
  value?: boolean
  /**
   * 商品 sku 数据
   */
  sku?: SkuObj
  /**
   * 商品信息
   */
  goods?: Goods
  /**
   * 商品 id
   */
  goodsId?: string | number
  /**
   * 显示在价格后面的标签
   */
  priceTag?: string
  /**
   * 是否显示商品剩余库存
   * @default false
   */
  hideStock?: boolean
  /**
   * 是否显示限购提示
   * @default false
   */
  hideQuotaText?: boolean
  /**
   * 是否隐藏已选提示
   * @default false
   */
  hideSelectedText?: boolean
  /**
   * 库存阈值。低于这个值会把库存数高亮显示
   * @default 50
   */
  stockThreshold?: number
  /**
   * 是否显示加入购物车按钮
   * @default true
   */
  showAddCartBtn?: boolean
  /**
   * 购买按钮文字
   * @default "立即购买"
   */
  buyText?: string
  /**
   * 加入购物车按钮文字
   * @default "加入购物车"
   */
  addCartText?: string
  /**
   * 限购数，0 表示不限购
   * @default 0
   */
  quota?: number
  /**
   * 已经购买过的数量
   * @default 0
   */
  quotaUsed?: number
  /**
   * 隐藏时重置选择的商品数量
   * @default false
   */
  resetStepperOnHide?: boolean
  /**
   * 隐藏时重置已选择的 sku
   * @default false
   */
  resetSelectedSkuOnHide?: boolean
  /**
   * 是否禁用步进器输入
   * @default false
   */
  disableStepperInput?: boolean
  /**
   * 是否在点击遮罩层后关闭
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 数量选择组件左侧文案
   * @default "购买数量"
   */
  stepperTitle?: string
  /**
   * 步进器相关自定义配置
   * @default {}
   */
  customStepperConfig?: CustomStepperConfig
  /**
   * 留言相关配置
   * @default {}
   */
  messageConfig?: MessageConfig
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * 默认选中的 sku
   * @default {}
   */
  initialSku?: InitialSkuObj
  /**
   * 是否展示售罄的 sku，默认展示并置灰
   * @default true
   */
  showSoldoutSku?: boolean
  /**
   * 是否禁用售罄的 sku
   * @default true
   */
  disableSoldoutSku?: boolean
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * 起售数量
   * @default 1
   */
  startSaleNum?: number
  /**
   * 商品属性
   */
  properties?: PropertyObj[]
  /**
   * 是否在点击商品图片时自动预览
   * @default true
   */
  previewOnClickImage?: boolean
  /**
   * 是否展示头部图片
   * @default true
   */
  showHeaderImage?: boolean
  /**
   * 是否开启图片懒加载，须配合 Lazyload 组件使用
   * @default false
   */
  lazyLoad?: boolean
  /**
   * Events
   */
  on?: SkuEvents
}

declare interface SkuEvents {
  /**
   * 点击添加购物车回调
   */
  ['add-cart']?: (skuData: SkuData) => any
  /**
   * 点击购买回调
   */
  ['buy-clicked']?: (skuData: SkuData) => any
  /**
   * 购买数量变化时触发
   */
  ['stepper-change']?: (value: number) => any
  /**
   * 切换规格类目时触发
   */
  ['sku-selected']?: (params: { skuValue; selectedSku; selectedSkuComb }) => any
  /**
   * 切换商品属性时触发
   */
  ['sku-prop-selected']?: (params: {
    propValue
    selectedProp
    selectedSkuComb
  }) => any
  /**
   * 打开商品图片预览时触发
   */
  ['open-preview']?: (data) => any
  /**
   * 关闭商品图片预览时触发
   */
  ['close-preview']?: (data) => any
  /**
   * 规格和属性被重置时触发
   */
  ['sku-reset']?: (params: {
    selectedSku
    selectedProp
    selectedSkuComb
  }) => any
}

declare type SkuTemplateEvents = TemplateEvents<SkuEvents>

declare interface SkuSlots {
  /**
   * 商品信息展示区，包含商品图片、名称、价格等信息
   */
  ['sku-header']?: () => any
  /**
   * 自定义 sku 头部价格展示
   */
  ['sku-header-price']?: () => any
  /**
   * 自定义 sku 头部原价展示
   */
  ['sku-header-origin-price']?: () => any
  /**
   * 额外 sku 头部区域
   */
  ['sku-header-extra']?: () => any
  /**
   * 自定义 sku 头部图片额外的展示
   */
  ['sku-header-image-extra']?: () => any
  /**
   * 自定义 sku 头部图片额外的展示
   */
  ['sku-header-image-extra']?: () => any
  /**
   * sku 展示区上方的内容，无默认展示内容，按需使用
   */
  ['sku-body-top']?: () => any
  /**
   * 商品 sku 展示区
   */
  ['sku-group']?: () => any
  /**
   * 额外商品 sku 展示区，一般用不到
   */
  ['extra-sku-group']?: () => any
  /**
   * 商品数量选择区
   */
  ['sku-stepper']?: () => any
  /**
   * 商品留言区
   */
  ['sku-messages']?: () => any
  /**
   * 商品留言之前的区域
   */
  ['before-sku-messages']?: () => any
  /**
   * 商品留言之后的区域
   */
  ['after-sku-messages']?: () => any
  /**
   * 操作按钮区顶部内容，无默认展示内容，按需使用
   */
  ['sku-actions-top']?: () => any
  /**
   * 操作按钮区
   */
  ['sku-actions']?: () => any
}

declare interface _Sku
  extends BaseComponent<SkuProps & SkuTemplateEvents, SkuSlots> {}

declare interface _SkuRef {
  /**
   * 获取当前 skuData
   */
  getSkuData: () => SkuData
  /**
   * 重置选中规格到初始状态
   */
  resetSelectedSku: () => any
}

export declare const Sku: _Sku

export declare const SkuRef: _SkuRef
