import { BaseComponent, TemplateEvents } from './_common'

declare interface SwipeCellProps {
  /**
   * 标识符，可以在事件参数中获取到
   */
  name?: string | number
  /**
   * 指定左侧滑动区域宽度，单位为`px`
   * @default "auto"
   */
  leftWidth?: string | number
  /**
   * 指定右侧滑动区域宽度，单位为`px`
   * @default "auto"
   */
  rightWidth?: string | number
  /**
   * 关闭前的回调函数
   */
  beforeClose?: (
    name: string,
    position: 'left' | 'right' | 'cell' | 'outside',
    instance: _SwipeCellRef
  ) => any
  /**
   * 是否禁用滑动
   * @default false
   */
  disabled?: boolean
  /**
   * 是否阻止滑动事件冒泡
   * @default false
   */
  stopPropagation?: boolean
  /**
   * Events
   */
  on?: SwipeCellEvents
  /**
   * Slots
   */
  scopedSlots?: SwipeCellSlots
}

declare interface SwipeCellEvents {
  /**
   * 点击时触发
   */
  ['click']?: (position: 'left' | 'right' | 'cell' | 'outside') => any
  /**
   * 打开时触发
   */
  ['open']?: (params: { position: 'left' | 'right'; name: string }) => any
  /**
   * 关闭时触发
   */
  ['close']?: (params: { position: 'left' | 'right'; name: string }) => any
}

declare type SwipeCellTemplateEvents = TemplateEvents<SwipeCellEvents>

declare interface SwipeCellSlots {
  /**
   * 自定义显示内容
   */
  ['default']?: () => any
  /**
   * 左侧滑动内容
   */
  ['left']?: () => any
  /**
   * 右侧滑动内容
   */
  ['right']?: () => any
}

declare interface _SwipeCell
  extends BaseComponent<
    SwipeCellProps & SwipeCellTemplateEvents,
    SwipeCellSlots
  > {}

declare interface _SwipeCellRef {
  /**
   * 打开单元格侧边栏
   */
  open: (position: 'left' | 'right') => any
  /**
   * 收起单元格侧边栏
   */
  close: () => any
}

export declare const SwipeCell: _SwipeCell

export declare const SwipeCellRef: _SwipeCellRef
