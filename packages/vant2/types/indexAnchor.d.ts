import { BaseComponent } from './_common'

declare interface IndexAnchorProps {
  /**
   * 索引字符
   */
  index?: string | number
  /**
   * Slots
   */
  scopedSlots?: IndexAnchorSlots
}

declare interface IndexAnchorSlots {
  /**
   * 锚点位置显示内容，默认为索引字符
   */
  ['default']?: () => any
}

declare interface _IndexAnchor
  extends BaseComponent<IndexAnchorProps, IndexAnchorSlots> {}

export declare const IndexAnchor: _IndexAnchor
