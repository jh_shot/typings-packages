import { BaseComponent, TemplateEvents } from './_common'

declare interface Contact {
  /**
   * 每位联系人的唯一标识
   */
  id?: string | number
  /**
   * 联系人姓名
   */
  name?: string
  /**
   * 联系人手机号
   */
  tel?: string | number
  /**
   * 是否为默认联系人
   */
  isDefault?: boolean
}

declare interface ContactListProps {
  /**
   * 当前选中联系人的 id
   */
  value?: string | number
  /**
   * 联系人列表
   * @default []
   */
  list?: Contact[]
  /**
   * 新建按钮文案
   * @default "新建联系人"
   */
  addText?: string
  /**
   * 默认联系人标签文案
   */
  defaultTagText?: string
  /**
   * Events
   */
  on?: ContactEvents
}

declare interface ContactEvents {
  /**
   * 点击新增按钮时触发
   */
  ['add']?: () => any
  /**
   * 点击编辑按钮时触发
   */
  ['edit']?: (contact: Contact, index: number) => any
  /**
   * 切换选中的联系人时触发
   */
  ['select']?: (contact: Contact, index: number) => any
}

declare type ContactTemplateEvents = TemplateEvents<ContactEvents>

declare interface _ContactList
  extends BaseComponent<ContactListProps & ContactTemplateEvents> {}

export declare const ContactList: _ContactList
