import { BaseComponent } from './_common'

declare interface LoadingProps {
  /**
   * 颜色
   * @default "#c9c9c9"
   */
  color?: string
  /**
   * 类型
   * @default "circular"
   */
  type?: 'spinner' | 'circular'
  /**
   * 加载图标大小，默认单位为 `px`
   * @default "30px"
   */
  size?: string | number
  /**
   * 文字大小，默认单位为 `px`
   * @default "14px"
   */
  textSize?: string | number
  /**
   * 文字颜色
   * @default "#c9c9c9"
   */
  textColor?: string
  /**
   * 是否垂直排列图标和文字内容
   * @default false
   */
  vertical?: boolean
  /**
   * Slots
   */
  scopedSlots?: LoadingSlots
}

declare interface LoadingSlots {
  /**
   * 加载文案
   */
  ['default']?: () => any
}

declare interface _Loading extends BaseComponent<LoadingProps, LoadingSlots> {}

export declare const Loading: _Loading
