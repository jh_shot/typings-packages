import { BaseComponent, TemplateEvents } from './_common'

declare interface TabbarProps {
  /**
   * 是否固定在底部
   * @default true
   */
  fixed?: boolean
  /**
   * 是否显示外边框
   * @default true
   */
  border?: boolean
  /**
   * 元素 z-index
   * @default 1
   */
  zIndex?: string | number
  /**
   * 选中标签的颜色
   * @default "#1989fa"
   */
  activeColor?: string
  /**
   * 未选中标签的颜色
   * @default "#7d7e80"
   */
  inactiveColor?: string
  /**
   * 是否开启路由模式
   * @default false
   */
  route?: boolean
  /**
   * 固定在底部时，是否在标签位置生成一个等高的占位元素
   * @default false
   */
  placeholder?: boolean
  /**
   * 是否开启底部安全区适配，设置 `fixed` 时默认开启
   * @default false
   */
  safeAreaInsetBottom?: boolean
  /**
   * 切换标签前的回调函数，返回 `false` 可阻止切换，支持返回 Promise
   */
  beforeChange?: (name: any) => boolean | Promise<any>
  /**
   * Events
   */
  on?: TabbarEvents
}

declare interface TabbarEvents {
  /**
   * 切换标签时触发
   */
  ['change']?: (active) => any
}

declare type TabbarTemplateEvents = TemplateEvents<TabbarEvents>

declare interface _Tabbar
  extends BaseComponent<TabbarProps & TabbarTemplateEvents> {}

export declare const Tabbar: _Tabbar
