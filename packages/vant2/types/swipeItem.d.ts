import { BaseComponent, TemplateEvents } from './_common'

declare interface SwipeItemProps {
  /**
   * Events
   */
  on?: SwipeItemEvents
}

declare interface SwipeItemEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
}

declare type SwipeItemTemplateEvents = TemplateEvents<SwipeItemEvents>

declare interface _SwipeItem
  extends BaseComponent<SwipeItemProps & SwipeItemTemplateEvents> {}

export declare const SwipeItem: _SwipeItem
