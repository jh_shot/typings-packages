import { BaseComponent } from './_common'

declare interface BadgeProps {
  /**
   * 徽标内容
   */
  content?: string | number
  /**
   * 徽标背景颜色
   * @default "#ee0a24"
   */
  color?: string
  /**
   * 是否展示为小红点
   * @default false
   */
  dot?: boolean
  /**
   * 最大值，超过最大值会显示 `{max}+`，仅当 content 为数字时有效
   */
  max?: string | number
  /**
   * Slots
   */
  scopedSlots?: BadgeSlots
}

declare interface BadgeSlots {
  /**
   * 徽标包裹的子元素
   */
  ['default']?: () => any
  /**
   * 自定义徽标内容
   */
  ['content']?: () => any
}

declare interface _Badge extends BaseComponent<BadgeProps, BadgeSlots> {}

export declare const Badge: _Badge
