import { BaseComponent, TemplateEvents } from './_common'
import { Rule } from './form'

declare interface FieldProps {
  /**
   * 当前输入的值
   */
  value?: string | number
  /**
   * 输入框左侧文本
   */
  label?: string
  /**
   * 名称，提交表单的标识符
   */
  name?: string
  /**
   * 输入框类型,
   * @default "text"
   */
  type?: 'text' | 'tel' | 'digit' | 'number' | 'textarea' | 'password'
  /**
   * 大小
   */
  size?: 'large'
  /**
   * 输入的最大字符数
   */
  maxlength?: string | number
  /**
   * 输入框占位提示文字
   */
  placeholder?: string
  /**
   * 是否显示内边框
   * @default true
   */
  border?: boolean
  /**
   * 是否禁用输入框
   * @default false
   */
  disabled?: boolean
  /**
   * 是否只读
   * @default false
   */
  readonly?: boolean
  /**
   * 是否在 label 后面添加冒号
   * @default false
   */
  colon?: boolean
  /**
   * 是否显示表单必填星号
   * @default false
   */
  required?: boolean
  /**
   * 是否使内容垂直居中
   * @default false
   */
  center?: boolean
  /**
   * 是否启用清除图标，点击清除图标后会清空输入框
   * @default false
   */
  clearable?: boolean
  /**
   * 显示清除图标的时机
   * @default "focus"
   */
  clearTrigger?: 'always' | 'focus'
  /**
   * 是否开启点击反馈
   * @default false
   */
  clickable?: boolean
  /**
   * 是否展示右侧箭头并开启点击反馈
   * @default false
   */
  isLink?: boolean
  /**
   * 是否自动聚焦，iOS 系统不支持该属性
   * @default false
   */
  autofocus?: boolean
  /**
   * 是否显示字数统计，需要设置`maxlength`属性
   * @default false
   */
  showWordLimit?: boolean
  /**
   * 是否将输入内容标红
   * @default false
   */
  error?: boolean
  /**
   * 底部错误提示文案，为空时不展示
   */
  errorMessage?: string
  /**
   * 输入内容格式化函数
   */
  formatter?: (...args: any[]) => any
  /**
   * 格式化函数触发的时机
   * @default "onChange"
   */
  formatTrigger?: 'onBlur' | 'onChange'
  /**
   * 箭头方向
   * @default "right"
   */
  arrowDirection?: 'left' | 'up' | 'down' | 'right'
  /**
   * 左侧文本额外类名
   */
  labelClass?: any
  /**
   * 左侧文本宽度，默认单位为`px`
   * @default "6.2em"
   */
  labelWidth?: string | number
  /**
   * 左侧文本对齐方式
   * @default "left"
   */
  labelAlign?: 'left' | 'center' | 'right'
  /**
   * 输入框对齐方式
   * @default "left"
   */
  inputAlign?: 'left' | 'center' | 'right'
  /**
   * 错误提示文案对齐方式
   * @default "left"
   */
  errorMessageAlign?: 'left' | 'center' | 'right'
  /**
   * 是否自适应内容高度，只对 textarea 有效，可传入对象,如 { maxHeight: 100, minHeight: 50 }，单位为`px`
   * @default false
   */
  autosize?: boolean | Record<string, any>
  /**
   * 左侧图标名称或图片链接
   */
  leftIcon?: string
  /**
   * 右侧图标名称或图片链接
   */
  rightIcon?: string
  /**
   * 图标类名前缀，同 Icon 组件的 class-prefix 属性
   * @default "van-icon"
   */
  iconPrefix?: string
  /**
   * 表单校验规则
   */
  rules?: Rule[]
  /**
   * input 标签原生的自动完成属性
   */
  autocomplete?: string
  /**
   * Events
   */
  on?: FieldEvents
  /**
   * Slots
   */
  scopedSlots?: FieldProps
}

declare interface FieldEvents {
  /**
   * 输入框内容变化时触发
   */
  ['input']?: (value: string) => any | Promise<void>
  /**
   * 输入框获得焦点时触发
   */
  ['focus']?: (event: Event) => any | Promise<void>
  /**
   * 输入框失去焦点时触发
   */
  ['blur']?: (event: Event) => any | Promise<void>
  /**
   * 点击清除按钮时触发
   */
  ['clear']?: (event: Event) => any | Promise<void>
  /**
   * 	点击 Field 时触发
   */
  ['click']?: (event: Event) => any | Promise<void>
  /**
   * 点击输入区域时触发
   */
  ['click-input']?: (event: Event) => any | Promise<void>
  /**
   * 点击左侧图标时触发
   */
  ['click-left-icon']?: (event: Event) => any | Promise<void>
  /**
   * 点击右侧图标时触发
   */
  ['click-right-icon']?: (event: Event) => any | Promise<void>
}

declare type FieldTemplateEvents = TemplateEvents<FieldEvents>

declare interface FieldSlots {
  /**
   * 自定义输入框 label 标签
   */
  ['label']?: () => any
  /**
   * 自定义输入框，使用此插槽后，与输入框相关的属性和事件将失效。
   * 在 Form 组件进行表单校验时，会使用 input 插槽中子组件的 value，而不是 Field 组件的 value。
   */
  ['input']?: () => any
  /**
   * 自定义输入框头部图标
   */
  ['left-icon']?: () => any
  /**
   * 自定义输入框尾部图标
   */
  ['right-icon']?: () => any
  /**
   * 自定义输入框尾部按钮
   */
  ['button']?: () => any
  /**
   * 自定义输入框最右侧的额外内容
   */
  ['extra']?: () => any
}

declare interface _Field
  extends BaseComponent<FieldProps & FieldTemplateEvents, FieldSlots> {}

declare interface _FieldRef {
  /**
   * 获取输入框焦点
   */
  focus: () => any
  /**
   * 取消输入框焦点
   */
  blur: () => any
}

export declare const Field: _Field

export declare const FieldRef: _FieldRef
