import { BaseComponent, TemplateEvents } from './_common'
import { AreaList } from './area'

declare interface AddressInfo {
  /**
   * 每条地址的唯一标识
   */
  id?: string | number
  /**
   * 收货人姓名
   */
  name?: string
  /**
   * 收货人手机号
   */
  tel?: string
  /**
   * 省份
   */
  province?: string
  /**
   * 城市
   */
  city?: string
  /**
   * 区县
   */
  county?: string
  /**
   * 详细地址
   */
  addressDetail?: string
  /**
   * 地区编码，通过省市区选择获取（必填）
   */
  areaCode?: string
  /**
   * 邮政编码
   */
  postalCode?: string
  /**
   * 是否为默认地址
   */
  isDefault?: boolean
}

declare interface SearchResult {
  /**
   * 地名
   */
  name?: string
  /**
   * 详细地址
   */
  address?: string
}

declare interface AddressEditProps {
  /**
   * 地区列表
   */
  areaList?: AreaList
  /**
   * 地区选择列占位提示文字
   * @default []
   */
  areaColumnsPlaceholder?: string[]
  /**
   * 地区输入框占位提示文字
   * @default "选择省 / 市 / 区"
   */
  areaPlaceholder?: string
  /**
   * 收货人信息初始值
   * @default {}
   */
  addressInfo?: AddressInfo
  /**
   * 详细地址搜索结果
   * @default []
   */
  searchResult?: SearchResult[]
  /**
   * 是否显示邮政编码
   * @default false
   */
  showPostal?: boolean
  /**
   * 是否显示删除按钮
   * @default false
   */
  showDelete?: boolean
  /**
   * 是否显示默认地址栏
   * @default false
   */
  showSetDefault?: boolean
  /**
   * 是否显示搜索结果
   * @default false
   */
  showSearchResult?: boolean
  /**
   * 是否显示地区
   * @default true
   */
  showArea?: boolean
  /**
   * 是否显示详细地址
   * @default true
   */
  showDetail?: boolean
  /**
   * 是否禁用地区选择
   * @default false
   */
  disableArea?: boolean
  /**
   * 保存按钮文字
   * @default "保存"
   */
  saveButtonText?: string
  /**
   * 删除按钮文字
   * @default "删除"
   */
  deleteButtonText?: string
  /**
   * 详细地址输入框行数
   * @default 1
   */
  detailRows?: string | number
  /**
   * 详细地址最大长度
   * @default 200
   */
  detailMaxlength?: string | number
  /**
   * 是否显示保存按钮加载动画
   * @default false
   */
  isSaving?: boolean
  /**
   * 是否显示删除按钮加载动画
   * @default false
   */
  isDeleting?: boolean
  /**
   * 手机号格式校验函数
   */
  telValidator?: (value: string) => boolean
  /**
   * 手机号最大长度
   */
  telMaxlength?: string | number
  /**
   * 邮政编码格式校验函数
   */
  postalValidator?: (value: string) => boolean
  /**
   * 自定义校验函数
   */
  validator?: (key, val) => string
  /**
   * Events
   */
  on?: AddressEditEvents
  /**
   * Slots
   */
  scopedSlots?: AddressEditSlots
}

declare interface AddressEditEvents {
  /**
   * 点击保存按钮时触发
   * @param content 表单内容
   */
  ['save']?: (content) => any
  /**
   * 输入框聚焦时触发
   * @param key 聚焦的输入框对应的 key
   */
  ['focus']?: (key) => any
  /**
   * 确认删除地址时触发
   * @param content 表单内容
   */
  ['delete']?: (content) => any
  /**
   * 取消删除地址时触发
   * @param content 表单内容
   */
  ['cancel-delete']?: (content) => any
  /**
   * 选中搜索结果时触发
   * @param value 搜索结果
   */
  ['select-search']?: (value) => any
  /**
   * 点击收件地区时触发
   */
  ['click-area']?: () => any
  /**
   * 修改收件地区时触发
   * @param values 地区信息
   */
  ['change-area']?: (values) => any
  /**
   * 修改详细地址时触发
   * @param value 详细地址内容
   */
  ['change-detail']?: (value) => any
  /**
   * 切换是否使用默认地址时触发
   * @param value 是否选中
   */
  ['change-default']?: (value) => any
}

declare type AddressEditTemplateEvents = TemplateEvents<AddressEditEvents>

declare interface AddressEditSlots {
  /**
   * 在邮政编码下方插入内容
   */
  ['default']?: () => any
}

declare interface _AddressEdit
  extends BaseComponent<
    AddressEditProps & AddressEditTemplateEvents,
    AddressEditSlots
  > {}

declare interface _AddressEditRef {
  /**
   * 设置详细地址
   */
  setAddressDetail: (addressDetail: string) => any
}

export declare const AddressEdit: _AddressEdit

export declare const AddressEditRef: _AddressEditRef
