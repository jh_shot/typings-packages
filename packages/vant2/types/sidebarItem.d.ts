import { BaseComponent, TemplateEvents } from './_common'

declare interface SidebarItemProps {
  /**
   * 内容
   * @default ""
   */
  title?: string
  /**
   * 是否显示右上角小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 是否禁用该项
   * @default false
   */
  disabled?: boolean
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Events
   */
  on?: SidebarItemEvents
  /**
   * Slots
   */
  scopedSlots?: SidebarItemSlots
}

declare interface SidebarItemEvents {
  /**
   * 点击时触发
   */
  ['click']?: (index: number) => any
}

declare type SidebarItemTemplateEvents = TemplateEvents<SidebarItemEvents>

declare interface SidebarItemSlots {
  /**
   * 自定义标题
   */
  ['title']?: () => any
}

declare interface _SidebarItem
  extends BaseComponent<
    SidebarItemProps & SidebarItemTemplateEvents,
    SidebarItemSlots
  > {}

export declare const SidebarItem: _SidebarItem
