import { BaseComponent, TemplateEvents } from './_common'

declare interface CheckboxProps {
  /**
   * 是否为选中状态
   * @default false
   */
  value?: boolean
  /**
   * 标识符
   */
  name?: any
  /**
   * 形状
   * @default "round"
   */
  shape?: 'round' | 'square'
  /**
   * 是否禁用复选框
   * @default false
   */
  disabled?: boolean
  /**
   * 是否禁用复选框文本点击
   * @default false
   */
  labelDisabled?: boolean
  /**
   * 文本位置
   * @default "right"
   */
  labelPosition?: 'left' | 'right'
  /**
   * 图标大小，默认单位为 `px`
   * @default "20px"
   */
  iconSize?: string | number
  /**
   * 选中状态颜色
   * @default "#1989fa"
   */
  checkedColor?: string
  /**
   * 是否与复选框组绑定
   * @default true
   */
  bindGroup?: boolean
  /**
   * Events
   */
  on?: CheckboxEvents
  /**
   * Slots
   */
  scopedSlots?: CheckboxSlots
}

declare interface CheckboxEvents {
  /**
   * 当绑定值变化时触发的事件
   */
  ['change']?: (checked: boolean) => any
  /**
   * 点击复选框时触发
   */
  ['click']?: (event: Event) => any
}

declare type CheckboxTemplateEvents = TemplateEvents<CheckboxEvents>

declare interface CheckboxSlots {
  /**
   * 自定义文本
   */
  ['default']?: () => any
  /**
   * 自定义图标
   */
  ['icon']?: (checked: boolean) => any
}

declare interface _Checkbox
  extends BaseComponent<
    CheckboxProps & CheckboxTemplateEvents,
    CheckboxSlots
  > {}

declare interface _CheckboxRef {
  /**
   * 切换选中状态，传 `true` 为选中，`false` 为取消选中，不传参为取反
   */
  toggle: (checked?: boolean) => any
}

export declare const Checkbox: _Checkbox

export declare const CheckboxRef: _CheckboxRef
