import { BaseComponent, TemplateEvents } from './_common'
import { ButtonType } from './button'

declare interface SubmitBarProps {
  /**
   * 价格（单位分）
   */
  price?: number
  /**
   * 价格小数点位数
   * @default 2
   */
  decimalLength?: string | number
  /**
   * 价格左侧文案
   * @default "合计："
   */
  label?: string
  /**
   * 价格右侧文案
   */
  suffixLabel?: string
  /**
   * 价格文案对齐方向
   */
  textAlign?: 'left' | 'right'
  /**
   * 按钮文字
   */
  buttonText?: string
  /**
   * 按钮类型
   * @default "danger"
   */
  buttonType?: ButtonType
  /**
   * 自定义按钮颜色
   */
  buttonColor?: string
  /**
   * 在订单栏上方的提示文案
   */
  tip?: string
  /**
   * 提示文案左侧的图标名称或图片链接
   */
  tipIcon?: string
  /**
   * 货币符号
   * @default "¥"
   */
  currency?: string
  /**
   * 是否禁用按钮
   * @default false
   */
  disabled?: boolean
  /**
   * 是否显示将按钮显示为加载中状态
   * @default false
   */
  loading?: boolean
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
  /**
   * Events
   */
  on?: SubmitBarEvents
  /**
   * Slots
   */
  scopedSlots?: SubmitBarSlots
}

declare interface SubmitBarEvents {
  /**
   * 按钮点击事件回调
   */
  ['submit']?: () => any
}

declare type SubmitBarTemplateEvents = TemplateEvents<SubmitBarEvents>

declare interface SubmitBarSlots {
  /**
   * 自定义订单栏左侧内容
   */
  ['default']?: () => any
  /**
   * 自定义按钮
   */
  ['button']?: () => any
  /**
   * 自定义订单栏上方内容
   */
  ['top']?: () => any
  /**
   * 提示文案中的额外内容
   */
  ['tip']?: () => any
}

declare interface _SubmitBar
  extends BaseComponent<
    SubmitBarProps & SubmitBarTemplateEvents,
    SubmitBarSlots
  > {}

export declare const SubmitBar: _SubmitBar
