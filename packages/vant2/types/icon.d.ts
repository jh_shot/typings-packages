import { BaseComponent, TemplateEvents } from './_common'

declare interface IconProps {
  /**
   * 图标名称或图片链接
   */
  name?: string
  /**
   * 是否显示图标右上角小红点
   * @default false
   */
  dot?: boolean
  /**
   * 图标右上角徽标的内容
   */
  badge?: string | number
  /**
   * 图标右上角徽标的内容（已废弃，请使用 badge 属性）
   * @deprecated
   */
  info?: string | number
  /**
   * 图标颜色
   * @default "inherit"
   */
  color?: string
  /**
   * 图标大小，如 `20px` `2em`，默认单位为`px`
   * @default "inherit"
   */
  size?: string | number
  /**
   * 类名前缀，用于使用自定义图标
   * @default "van-icon"
   */
  classPrefix?: string
  /**
   * HTML 标签
   * @default "i"
   */
  tag?: string
  /**
   * Events
   */
  on?: IconEvents
}

declare interface IconEvents {
  /**
   * 点击图标时触发
   */
  ['click']?: (event: Event) => any
}

declare type IconTemplateEvents = TemplateEvents<IconEvents>

declare interface _Icon extends BaseComponent<IconProps & IconTemplateEvents> {}

export declare const Icon: _Icon
