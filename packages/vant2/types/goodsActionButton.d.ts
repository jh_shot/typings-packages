import { BaseComponent } from './_common'
import { ButtonType } from './button'

declare interface GoodsActionButtonProps {
  /**
   * 按钮文字
   */
  text?: string
  /**
   * 按钮类型
   * @default "default"
   */
  type?: ButtonType
  /**
   * 按钮颜色，支持传入`linear-gradient`渐变色
   */
  color?: string
  /**
   * 左侧图标名称或图片链接
   */
  icon?: string
  /**
   * 是否禁用按钮
   * @default false
   */
  disabled?: boolean
  /**
   * 是否显示为加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 点击后跳转的链接地址
   */
  url?: string
  /**
   * 点击后跳转的目标路由对象，同 vue-router 的 to 属性
   */
  to?: string | Record<string, any>
  /**
   * 是否在跳转时替换当前页面历史
   * @default false
   */
  replace?: boolean
  /**
   * Slots
   */
  scopedSlots?: GoodsActionButtonSlots
}

declare interface GoodsActionButtonSlots {
  /**
   * 按钮显示内容
   */
  ['default']?: () => any
}

declare interface _GoodsActionButton
  extends BaseComponent<GoodsActionButtonProps, GoodsActionButtonSlots> {}

export declare const GoodsActionButton: _GoodsActionButton
