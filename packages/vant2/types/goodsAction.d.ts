import { BaseComponent } from './_common'

declare interface GoodsActionProps {
  /**
   * 是否开启底部安全区适配
   * @default true
   */
  safeAreaInsetBottom?: boolean
}

declare interface _GoodsAction extends BaseComponent<GoodsActionProps> {}

export declare const GoodsAction: _GoodsAction
