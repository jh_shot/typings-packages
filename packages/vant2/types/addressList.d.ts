import { BaseComponent, TemplateEvents } from './_common'

declare interface Address {
  /**
   * 每条地址的唯一标识
   */
  id?: string | number
  /**
   * 收货人姓名
   */
  name?: string
  /**
   * 收货人手机号
   */
  tel?: string | number
  /**
   * 收货地址
   */
  address?: string
  /**
   * 是否为默认地址
   */
  isDefault?: boolean
}

declare interface AddressListProps {
  /**
   * 当前选中地址的 id
   */
  value?: string | number
  /**
   * 地址列表
   * @default []
   */
  list?: Address[]
  /**
   * 不可配送地址列表
   * @default []
   */
  disabledList?: Address[]
  /**
   * 不可配送提示文案
   */
  disabledText?: string
  /**
   * 是否允许切换地址
   * @default true
   */
  switchable?: boolean
  /**
   * 底部按钮文字
   * @default "新增地址"
   */
  addButtonText?: string
  /**
   * 默认地址标签文字
   */
  defaultTagText?: string
  /**
   * Events
   */
  on?: AddressListEvents
  /**
   * Slots
   */
  scopedSlots?: AddressListSlots
}

declare interface AddressListEvents {
  /**
   * 点击新增按钮时触发
   */
  ['add']?: () => any
  /**
   * 点击编辑按钮时触发
   */
  ['edit']?: (item: Address, index: number) => any
  /**
   * 切换选中的地址时触发
   */
  ['select']?: (item: Address, index: number) => any
  /**
   * 编辑不可配送的地址时触发
   */
  ['edit-disabled']?: (item: Address, index: number) => any
  /**
   * 选中不可配送的地址时触发
   */
  ['select-disabled']?: (item: Address, index: number) => any
  /**
   * 点击任意地址时触发
   */
  ['click-item']?: (item: Address, index: number) => any
}

declare type AddressListTemplateEvents = TemplateEvents<AddressListEvents>

declare interface AddressListSlots {
  /**
   * 在列表下方插入内容
   */
  ['default']?: () => any
  /**
   * 在顶部插入内容
   */
  ['top']?: () => any
  /**
   * 在列表项底部插入内容
   */
  ['item-bottom']?: (...args: any[]) => any
  /**
   * 列表项标签内容自定义
   */
  ['tag']?: (...args: any[]) => any
}

declare interface _AddressList
  extends BaseComponent<
    AddressListProps & AddressListTemplateEvents,
    AddressListSlots
  > {}

export declare const AddressList: _AddressList
