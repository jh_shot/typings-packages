import { BaseComponent, TemplateEvents } from './_common'

declare interface SwipeProps {
  /**
   * 自动轮播间隔，单位为 ms
   */
  autoplay?: boolean
  /**
   * 动画时长，单位为 ms
   * @default 500
   */
  duration?: string | number
  /**
   * 初始位置索引值
   * @default 0
   */
  initialSwipe?: string | number
  /**
   * 滑块宽度，单位为`px`
   * @default "auto"
   */
  width?: string | number
  /**
   * 滑块高度，单位为`px`
   * @default "auto"
   */
  height?: string | number
  /**
   * 是否开启循环播放
   * @default true
   */
  loop?: boolean
  /**
   * 是否显示指示器
   * @default true
   */
  showIndicators?: boolean
  /**
   * 是否为纵向滚动
   * @default false
   */
  vertical?: boolean
  /**
   * 是否可以通过手势滑动
   * @default true
   */
  touchable?: boolean
  /**
   * 是否阻止滑动事件冒泡
   * @default true
   */
  stopPropagation?: boolean
  /**
   * 是否延迟渲染未展示的轮播
   * @default false
   */
  lazyRender?: boolean
  /**
   * 指示器颜色
   * @default "#1989fa"
   */
  indicatorColor?: string
  /**
   * Events
   */
  on?: SwipeEvents
  /**
   * Slots
   */
  scopedSlots?: SwipeSlots
}

declare interface SwipeEvents {
  /**
   * 每一页轮播结束后触发
   */
  ['change']?: (index: number) => any
}

declare type SwipeTemplateEvents = TemplateEvents<SwipeEvents>

declare interface SwipeSlots {
  /**
   * 轮播内容
   */
  ['default']?: () => any
  /**
   * 自定义指示器
   */
  ['indicator']?: () => any
}

declare interface _Swipe
  extends BaseComponent<SwipeProps & SwipeTemplateEvents, SwipeSlots> {}

declare interface _SwipeRef {
  /**
   * 切换到上一轮播
   */
  prev: () => any
  /**
   * 切换到下一轮播
   */
  next: () => any
  /**
   * 切换到指定位置
   */
  swipeTo: (index: number, options?: { immediate: boolean }) => any
  /**
   * 外层元素大小或组件显示状态变化时，可以调用此方法来触发重绘
   */
  resize: () => any
}

export declare const Swipe: _Swipe

export declare const SwipeRef: _SwipeRef
