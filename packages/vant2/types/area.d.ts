import { BaseComponent, TemplateEvents } from './_common'

export declare interface AreaList {
  /**
   * 省
   */
  province_list: Record<any, any>
  /**
   * 市
   */
  city_list: Record<any, any>
  /**
   * 区
   */
  county_list: Record<any, any>
}

declare interface AreaProps {
  /**
   * 当前选中的省市区`code`
   */
  value?: string
  /**
   * 顶部栏标题
   */
  title?: string
  /**
   * 确认按钮文字
   * @default "确定"
   */
  confirmButtonText?: string
  /**
   * 取消按钮文字
   * @default "取消"
   */
  cancelButtonText?: string
  /**
   * 省市区数据
   */
  areaList?: AreaList
  /**
   * 列占位提示文字
   */
  columnsPlaceholder?: string[]
  /**
   * 是否显示加载状态
   * @default false
   */
  loading?: boolean
  /**
   * 是否为只读状态，只读状态下无法切换选项
   * @default false
   */
  readonly?: boolean
  /**
   * 选项高度，支持 `px` `vw` `vh` `rem` 单位，默认 `px`
   * @default 44
   */
  itemHeight?: string | number
  /**
   * 显示列数
   * - `1` 省
   * - `2` 省市
   * - `3` 省市区
   */
  columnsNum?: 1 | 2 | 3
  /**
   * 可见的选项个数
   * @default 6
   */
  visibleItemCount?: string | number
  /**
   * 快速滑动时惯性滚动的时长，单位`ms`
   * @default 1000
   */
  swipeDuration?: string | number
  /**
   * 根据`code`校验海外地址，海外地址会划分至单独的分类
   */
  isOverseaCode?: () => boolean
  /**
   * Events
   */
  on?: AreaEvents
  /**
   * Slots
   */
  scopedSlots?: AreaSlots
}

declare interface AreaEvents {
  /**
   * 点击右上方完成按钮
   */
  ['confirm']?: (data: Array<{ code; name }>) => any
  /**
   * 点击取消按钮时
   */
  ['cancel']?: () => any
  /**
   * 选项改变时触发
   */
  ['change']?: (picker, data, index) => any
}

declare type AreaTemplateEvents = TemplateEvents<AreaEvents>

declare interface AreaSlots {
  /**
   * 自定义标题内容
   */
  ['title']?: () => any
  /**
   * 自定义选项上方内容
   */
  ['columns-top']?: () => any
  /**
   * 自定义选项下方内容
   */
  ['columns-bottom']?: () => any
}

declare interface _Area
  extends BaseComponent<AreaProps & AreaTemplateEvents, AreaSlots> {}

declare interface _AreaRef {
  /**
   * 根据 code 重置所有选项，若不传 code，则重置到第一项
   */
  reset: (code?: string) => any
}

export declare const Area: _Area

export declare const AreaRef: _AreaRef
