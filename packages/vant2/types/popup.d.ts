import { BaseComponent, TemplateEvents } from './_common'

declare interface PopupProps {
  /**
   * 是否显示弹出层
   * @default false
   */
  value?: boolean
  /**
   * 是否显示遮罩层
   * @default true
   */
  overlay?: boolean
  /**
   * 弹出位置
   * @default "center"
   */
  position?: 'top' | 'center' | 'bottom' | 'right' | 'left'
  /**
   * 自定义遮罩层类名
   */
  overlayClass?: string
  /**
   * 自定义遮罩层样式
   */
  overlayStyle?: any
  /**
   * 动画时长，单位秒
   * @default 0.3
   */
  duration?: string | number
  /**
   * 是否显示圆角
   * @default false
   */
  round?: boolean
  /**
   * 是否锁定背景滚动
   * @default true
   */
  lockScroll?: boolean
  /**
   * 是否在显示弹层时才渲染节点
   * @default true
   */
  lazyRender?: boolean
  /**
   * 是否在页面回退时自动关闭
   * @default false
   */
  closeOnPopstate?: boolean
  /**
   * 是否在点击遮罩层后关闭
   * @default true
   */
  closeOnClickOverlay?: boolean
  /**
   * 是否显示关闭图标
   */
  closeable?: boolean
  /**
   * 关闭图标名称或图片链接
   * @default "cross"
   */
  closeIcon?: string
  /**
   * 关闭图标位置
   * @default "top-right"
   */
  closeIconPosition?: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right'
  /**
   * 动画类名，等价于 transition 的`name`属性
   */
  transition?: string
  /**
   * 是否在初始渲染时启用过渡动画
   * @default false
   */
  transitionAppear?: boolean
  /**
   * 指定挂载的节点
   */
  getContainer?: string | (() => any)
  /**
   * 是否开启底部安全区适配
   * @default false
   */
  safeAreaInsetBottom?: boolean
  /**
   * Events
   */
  on?: PopupEvents
  /**
   * Slots
   */
  scopedSlots?: PopupSlots
}

declare interface PopupEvents {
  /**
   * 点击弹出层时触发
   */
  ['click']?: (event: Event) => any
  /**
   * 点击遮罩层时触发
   */
  ['click-overlay']?: () => any
  /**
   * 点击关闭图标时触发
   */
  ['click-close-icon']?: (event: Event) => any
  /**
   * 打开弹出层时触发
   */
  ['open']?: () => any
  /**
   * 关闭弹出层时触发
   */
  ['close']?: () => any
  /**
   * 开弹出层且动画结束后触发
   */
  ['opened']?: () => any
  /**
   * 关闭弹出层且动画结束后触发
   */
  ['closed']?: () => any
}

declare type PopupTemplateEvents = TemplateEvents<PopupEvents>

declare interface PopupSlots {
  ['default']?: () => any
}

declare interface _Popup
  extends BaseComponent<PopupProps & PopupTemplateEvents, PopupSlots> {}

export declare const Popup: _Popup
