import { BaseComponent, TemplateEvents } from './_common'

declare interface StepperProps {
  /**
   * 当前输入的值
   */
  value?: string | number
  /**
   * 最小值
   * @default 1
   */
  min?: string | number
  /**
   * 最大值
   */
  max?: string | number
  /**
   * 初始值，当 v-model 为空时生效
   * @default 1
   */
  defaultValue?: string | number
  /**
   * 步长，每次点击时改变的值
   * @default 1
   */
  step?: string | number
  /**
   * 标识符，可以在`change`事件回调参数中获取
   */
  name?: string | number
  /**
   * 输入框宽度，默认单位为`px`
   * @default "32px"
   */
  inputWidth?: string | number
  /**
   * 按钮大小以及输入框高度，默认单位为`px`
   * @default "28px"
   */
  buttonSize?: string | number
  /**
   * 固定显示的小数位数
   */
  decimalLength?: string | number
  /**
   * 样式风格
   */
  theme?: 'round'
  /**
   * 输入框占位提示文字
   */
  placeholder?: string
  /**
   * 是否只允许输入整数
   * @default false
   */
  integer?: boolean
  /**
   * 是否禁用步进器
   * @default false
   */
  disabled?: boolean
  /**
   * 是否禁用增加按钮
   * @default false
   */
  disablePlus?: boolean
  /**
   * 是否禁用减少按钮
   * @default false
   */
  disableMinus?: boolean
  /**
   * 是否禁用输入框
   * @default false
   */
  disableInput?: boolean
  /**
   * 是否开启异步变更，开启后需要手动控制输入值
   * @default false
   */
  asyncChange?: boolean
  /**
   * 是否显示增加按钮
   * @default true
   */
  showPlus?: boolean
  /**
   * 是否显示减少按钮
   * @default true
   */
  showMinus?: boolean
  /**
   * 是否显示输入框
   * @default true
   */
  showInput?: boolean
  /**
   * 是否开启长按手势
   * @default true
   */
  longPress?: boolean
  /**
   * 是否允许输入的值为空
   * @default false
   */
  allowEmpty?: boolean
  /**
   * Events
   */
  on?: StepperEvents
}

declare interface StepperEvents {
  /**
   * 当绑定值变化时触发的事件
   */
  ['change']?: (value: string, detail: { name: string }) => any
  /**
   * 点击不可用的按钮时触发
   */
  ['overlimit']?: () => any
  /**
   * 点击增加按钮时触发
   */
  ['plus']?: () => any
  /**
   * 点击减少按钮时触发
   */
  ['minus']?: () => any
  /**
   * 输入框聚焦时触发
   */
  ['focus']?: (event: Event) => any
  /**
   * 输入框失焦时触发
   */
  ['blur']?: (event: Event) => any
}

declare type StepperTemplateEvents = TemplateEvents<StepperEvents>

declare interface _Stepper
  extends BaseComponent<StepperProps & StepperTemplateEvents> {}

export declare const Stepper: _Stepper
