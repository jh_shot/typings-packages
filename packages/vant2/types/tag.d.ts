import { BaseComponent, TemplateEvents } from './_common'

declare interface TagProps {
  /**
   * 类型
   * @default "default"
   */
  type?: 'default' | 'primary' | 'success' | 'danger' | 'warning'
  /**
   * 大小
   */
  size?: 'large' | 'medium'
  /**
   * 标签颜色
   */
  color?: string
  /**
   * 是否为空心样式
   * @default false
   */
  plain?: boolean
  /**
   * 是否为圆角样式
   * @default false
   */
  round?: boolean
  /**
   * 是否为标记样式
   * @default false
   */
  mark?: boolean
  /**
   * 文本颜色，优先级高于`color`属性
   * @default "white"
   */
  textColor?: string
  /**
   * 是否为可关闭标签
   * @default false
   */
  closeable?: boolean
  /**
   * Events
   */
  on?: TagEvents
  /**
   * Slots
   */
  scopedSlots?: TagSlots
}

declare interface TagEvents {
  /**
   * 点击时触发
   */
  ['click']?: (event: Event) => any
  /**
   * 关闭标签时触发
   */
  ['close']?: () => any
}

declare type TagTemplateEvents = TemplateEvents<TagEvents>

declare interface TagSlots {
  /**
   * 标签显示内容
   */
  ['default']?: () => any
}

declare interface _Tag
  extends BaseComponent<TagProps & TagTemplateEvents, TagSlots> {}

export declare const Tag: _Tag
