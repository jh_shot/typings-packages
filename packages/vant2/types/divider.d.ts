import { BaseComponent } from './_common'

declare interface DividerProps {
  /**
   * 是否使用虚线
   * @default false
   */
  dashed?: boolean
  /**
   * 是否使用 0.5px 线
   * @default true
   */
  hairline?: boolean
  /**
   * 内容位置
   * @default "center"
   */
  contentPosition?: 'left' | 'center' | 'right'
  /**
   * Slots
   */
  scopedSlots?: DividerSlots
}

declare interface DividerSlots {
  /**
   * 内容
   */
  ['default']?: () => any
}

declare interface _Divider extends BaseComponent<DividerProps, DividerSlots> {}

export declare const Divider: _Divider
