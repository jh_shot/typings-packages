import {
  NotifyMessage,
  NotifyOptions,
  VanNotify,
  Notify as _Notify
} from 'vant/types/notify'

import { BaseComponent } from './_common'

declare type NotifyComponentProps = NotifyOptions

declare interface _NotifyComponent
  extends BaseComponent<NotifyComponentProps> {}

export declare const Notify: Omit<_Notify, 'Component'> & {
  (message: NotifyOptions | NotifyMessage): VanNotify
  Component: _NotifyComponent
}
