import { BaseComponent } from './_common'

declare interface CellGroupProps {
  /**
   * 分组标题
   */
  title?: string
  /**
   * 是否显示外边框
   * @default true
   */
  border?: boolean
  /**
   * Slots
   */
  scopedSlots?: CellGroupSlots
}

declare interface CellGroupSlots {
  /**
   * 默认插槽
   */
  ['default']?: () => any
  /**
   * 自定义分组标题
   */
  ['title']?: () => any
}

declare interface _CellGroup
  extends BaseComponent<CellGroupProps, CellGroupSlots> {}

export declare const CellGroup: _CellGroup
