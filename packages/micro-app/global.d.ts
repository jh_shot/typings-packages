type EventCenterForMicroApp = InstanceType<
  typeof import('@micro-zoe/micro-app/interact')['EventCenterForMicroApp']
>

declare global {
  interface Window {
    /**
     * 判断应用是否在微前端环境中
     */
    __MICRO_APP_ENVIRONMENT__?: boolean
    /**
     * 应用名称
     */
    __MICRO_APP_NAME__?: string
    /**
     * 子应用的静态资源前缀
     */
    __MICRO_APP_PUBLIC_PATH__?: string
    /**
     * 子应用的基础路由
     */
    __MICRO_APP_BASE_ROUTE__?: string
    /**
     * 判断应用是否是主应用
     */
    __MICRO_APP_BASE_APPLICATION__?: boolean
    /**
     * 获取真实的window
     */
    rawWindow?: Window
    /**
     * 获取真实的document
     */
    rawDocument?: Document
    /**
     * 子应用mount函数
     */
    mount: () => void
    /**
     * 子应用unmount函数
     */
    unmount: () => void
    /**
     * 注入对象
     */
    microApp?: {
      /**
       * 创建无绑定的纯净元素，该元素可以逃离元素隔离的边界，不受子应用沙箱的控制
       */
      pureCreateElement: typeof import('@micro-zoe/micro-app')['pureCreateElement']
      /**
       * 解除元素绑定，通常用于受子应用元素绑定影响，导致主应用元素错误绑定到子应用的情况
       */
      removeDomScope: typeof import('@micro-zoe/micro-app')['removeDomScope']
      /**
       * 虚拟路由系统
       */
      router: typeof import('@micro-zoe/micro-app')['default']['router']
    } & EventCenterForMicroApp
  }
}

export type {}
