# MicroApp类型定义

基于 MicroApp 1.0.0.rc.5 版本编写

- 全局变量引用 `global.d.ts`

`tsconfig.json` 示例：

```json
{
  "compilerOptions": {
    "types": [
      "@ttou/micro-app-typings/global"
    ]
  }
}
```
